#!/usr/bin/python

###############################################################################
##
## Author        : Jevgenij Zubovskij
## Company       : University of Edinburgh
##
## Module Name   : main.py
## Project       : Microprocessor
## Tools Version : Python 2.7
##
## Description   : This Python module generates the ROM and RAM files and 
## contains all the fucntions (combinations of instructions) and how they are 
## called. I.E. this is the main execution file, same as one would write a C
## program, instantiate variables, write functions, call them etc.
##
###############################################################################


import sys
from generator_constants import Rom_Constants 
from generator_constants import Ram_Constants 
from generator_constants import Bus_Constants 
from ram_generator       import RAM
from rom_generator       import *


rom_constants = Rom_Constants()
ram_constants = Ram_Constants()
bus_constants = Bus_Constants()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################     GENERATE RAM
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


debug = 0

if len(sys.argv) > 1 and int(sys.argv[1]) == 1:
         debug = 1
         print 'DEBUG MODE RAM GENERATION'


ram_constants = Ram_Constants()
ram = RAM(ram_constants.MAX_SIZE, debug)


#x current, max and reset and even
ram.add(ram_constants.X_CURR_ADDR, 0,   "x_curr")
ram.add(ram_constants.X_DIM_ADDR,  160, "x_max")
ram.add(ram_constants.X_RESET_ADDR,0,   "x_reset")
ram.add(ram_constants.X_LINE_ADDR, 0,   "x_line")

#y current, max and reset and even
ram.add(ram_constants.Y_CURR_ADDR, 0,   "y_curr")
ram.add(ram_constants.Y_DIM_ADDR,  120, "y_max")
ram.add(ram_constants.Y_RESET_ADDR,0,   "y_reset")
ram.add(ram_constants.Y_LINE_ADDR, 0,   "y_line")

#pixel 
ram.add(ram_constants.PIXEL_ADDR,  0,   "pixel")

#colour config FG, BG
ram.add(ram_constants.COLOUR_FG_ADDR, 0xF0, "foreground_colour")
ram.add(ram_constants.COLOUR_BG_ADDR, 0x0D, "background_colour")

#timer (1s) counter max value and value
ram.add(ram_constants.TIMER_COUNTER_CURR_ADDR, 0,  "timer_counter_curr")
ram.add(ram_constants.TIMER_COUNTER_DIM_ADDR,  20, "timer_counter_max")
ram.add(ram_constants.TIMER_COUNTER_RESET_ADDR, 0, "timer_counter_reset")

#mouse coordinates
ram.add(ram_constants.MOUSE_X_PREV_ADDR, 20, "previous mouse pointer x")
ram.add(ram_constants.MOUSE_Y_PREV_ADDR, 20, "previous mouse pointer y")

#line constants
ram.add(ram_constants.LINE_X_1_ADDR, 50,  "line 1 along x")
ram.add(ram_constants.LINE_X_2_ADDR, 110, "line 2 along x")
                
ram.add(ram_constants.LINE_Y_1_ADDR, 40,  "line 1 along y")
ram.add(ram_constants.LINE_Y_2_ADDR, 80,  "line 2 along y")

#region selection
ram.add(ram_constants.REGION_SELECTED_CURR_ADDR, 0, "the current region of the screen (and IR command)")
ram.add(ram_constants.REGION_SELECTED_PREV_ADDR, 0, "the previous region of the screen (and IR command)")

#car selected
ram.add(ram_constants.CAR_SELECTED_ADDR, 0, "car selected number")

#record previous mouse address
ram.add(ram_constants.MOUSE_STATUS_PREV_ADDR, 0, "mouse_prev_addr")


#constants
ram.add(ram_constants.ZERO_ADDR,  0, "0 constant")
ram.add(ram_constants.ONE_ADDR,   1, "1 constant")
ram.add(ram_constants.TWO_ADDR,   2, "2 constant")
ram.add(ram_constants.THREE_ADDR, 3, "3 constant")
ram.add(ram_constants.FOUR_ADDR,  4, "4 constant")
ram.add(ram_constants.FIVE_ADDR,  5, "5 constant")
ram.add(ram_constants.SIX_ADDR,   6, "6 constant")
ram.add(ram_constants.SEVEN_ADDR, 7, "7 constant")
ram.add(ram_constants.EIGHT_ADDR, 8, "8 constant")
ram.add(ram_constants.NINE_ADDR,  9, "9 constant")
ram.add(ram_constants.TEN_ADDR,   10,"10 constant")
ram.add(ram_constants.MAX_ADDR,   255, "255 constant")



                
ram.add(ram_constants.GREEN_COLOUR_ADDR,  28,  "green colour rgb")
ram.add(ram_constants.RED_COLOUR_ADDR,    224, "red colour rgb")
ram.add(ram_constants.BLUE_COLOUR_ADDR,   3,   "blue colour rgb")
ram.add(ram_constants.YELLOW_COLOUR_ADDR, 252, "yellow colour rgb")





#write the RAM out
ram_file = "../Memory Initialisation/Main_RAM.txt"
if debug:
        ram_file = "./Intermediate Representations/debug_ram.txt"

ram.write(ram_file)     
        

output_file = "./Intermediate Representations/program_instructions.txt"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################     PROGRAM PARAMETERS AND FUNCTIONS (MODULAR BLOCKS)
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


##########################################################
#################     VGA COUNTER UPDATES 
##########################################################

#update the horizontal counter
def update_x():

        mod_n_counter_update(ram_constants.X_CURR_ADDR, ram_constants.X_DIM_ADDR, ram_constants.X_RESET_ADDR,  "x")

#update the vertical counter
def update_y():

        load_a(ram_constants.X_RESET_ADDR, "a = x_reset")                
        load_b(ram_constants.ZERO_ADDR, "b = 0")               

        branch_pc = instructions.program_counter
        branch_a_eq_b(0, "if x_reset == 0 i.e. not reset, don't update y")          

        mod_n_counter_update(ram_constants.Y_CURR_ADDR, ram_constants.Y_DIM_ADDR, ram_constants.Y_RESET_ADDR,  "y")      

        instructions.entries[branch_pc].address =  instructions.program_counter


 

##########################################################
#################     GET X, Y, PIXEL TO VGA
##########################################################

#get pixel value accoring to pattern beign drawn on screen
def get_pixel():

        instructions.add_comment("BEGIN : Get Pixel")
                
        load_a(ram_constants.X_CURR_ADDR, "a = x_curr") 

        load_b(ram_constants.LINE_X_1_ADDR, "b = line_x_1")  
        #save first branch pc   
        branch_line_1_pc = instructions.program_counter
        branch_a_eq_b(0, "jump if line_x_1")


        load_b(ram_constants.LINE_X_2_ADDR, "b = line_x_2")  
        #save first branch pc   
        branch_line_2_pc = instructions.program_counter
        branch_a_eq_b(0, "jump if line_x_2")

                
        load_a(ram_constants.Y_CURR_ADDR, "a = x_curr") 

        load_b(ram_constants.LINE_Y_1_ADDR, "b = line_y_1")  
        #save first branch pc   
        branch_line_3_pc = instructions.program_counter
        branch_a_eq_b(0, "jump if line_y_1")


        load_b(ram_constants.LINE_Y_2_ADDR, "b = line_y_1")  
        #save first branch pc   
        branch_line_4_pc = instructions.program_counter
        branch_a_eq_b(0, "jump if line_y_2")


        load_a(ram_constants.ZERO_ADDR, "a = 0")
        #save branch pc
        branch_line_5_pc = instructions.program_counter
        goto(0, "pixel = a, jump to assign")

        instructions.entries[branch_line_1_pc].address =  instructions.program_counter  
        instructions.entries[branch_line_2_pc].address =  instructions.program_counter   
        instructions.entries[branch_line_3_pc].address =  instructions.program_counter   
        instructions.entries[branch_line_4_pc].address =  instructions.program_counter     
        load_a(ram_constants.ONE_ADDR, "a = 1")
        
        instructions.entries[branch_line_5_pc].address =  instructions.program_counter    
        store_a(ram_constants.PIXEL_ADDR, "pixel = (is_horizontal_or_vertical_line) ? 1 : 0")

        instructions.add_comment("END : Get Pixel")


#write current pixel at current X,Y to the VGA
def write_to_vga():
              
        instructions.add_comment("BEGIN : Write to VGA")

        
        load_a(ram_constants.X_CURR_ADDR, "a = x") 
        store_a(bus_constants.VGA_ADDR0, "0xB0 = x")  

        load_a(ram_constants.Y_CURR_ADDR, "a = y") 
        store_a(bus_constants.VGA_ADDR1, "0xB1 = y")  

        load_a(ram_constants.PIXEL_ADDR, "a = pixel")  
        store_a(bus_constants.VGA_ADDR2, "0xB2 =  pixel")  
                
        instructions.add_comment("END : Write to VGA")
                
##########################################################
#################     VGA PATTERN GENERATION 
##########################################################


#conclude if pattern is done being written
def is_vga_pattern_done():

        instructions.add_comment("BEGIN : Is VGA PAttern Done")


        load_a(ram_constants.Y_RESET_ADDR, "a = vga_pattern_done")           
        load_b(ram_constants.ONE_ADDR, "b ==1")             

        branch_pc = instructions.program_counter
        branch_a_eq_b(0, "jump over return to base address if a == b")        
 
        goto(rom_constants.WRITE_VGA_PATTERN_PC, "start the vga pattern loop over")            

        instructions.entries[branch_pc].address =  instructions.program_counter

        instructions.add_comment("END : Is VGA PAttern Done")

#function to write current (x,y) pixel value
def write_current_pixel():

        instructions.add_comment("BEGIN : Write Current Pixel")

        rom_constants.PIXEL_WRITE_PC = instructions.program_counter
        
        get_pixel()     
        write_to_vga()            
        function_return("pixel written")

        instructions.add_comment("END : Write Current Pixel")


#write the vga pattern
def write_vga_pattern():


        instructions.add_comment("BEGIN : Write VGA Pattern")    

        rom_constants.WRITE_VGA_PATTERN_PC = instructions.program_counter

        function_call(rom_constants.PIXEL_WRITE_PC, "write current pixel value")            
           
        update_x() 
        update_y()  

        is_vga_pattern_done()    

        instructions.add_comment("END : Write VGA Pattern")      



##########################################################
#################      DETERMINE SCREEN REGION
##########################################################


#determine the X of the region on screen
def determine_region_x():

        instructions.add_comment("BEGIN : Determine Region X") 

        load_a(bus_constants.MOUSE_ADDR1, "a = mouse_x")

        ###################################################################  

        load_b(ram_constants.FOUR_ADDR, "b = 4")
        store_b(ram_constants.REGION_SELECTED_CURR_ADDR, "region = b")

        load_b(ram_constants.LINE_X_1_ADDR, "b = line_x_1")
        if_in_leftmost_regions_pc = instructions.program_counter
        branch_a_lesser_b(0, "do not continue of region found")

        ###################################################################  

        load_b(ram_constants.EIGHT_ADDR, "b = 8")
        store_b(ram_constants.REGION_SELECTED_CURR_ADDR, "region = b")

        load_b(ram_constants.LINE_X_2_ADDR, "b = line_x_2")
        if_in_rightmost_regions_pc = instructions.program_counter
        branch_a_greater_b(0, "do not continue of region found")

        ################################################################### 

        load_b(ram_constants.ZERO_ADDR, "b = 0")
        store_b(ram_constants.REGION_SELECTED_CURR_ADDR, "region = b")

        ###################################################################   
        
        instructions.entries[if_in_leftmost_regions_pc].address = instructions.program_counter
        instructions.entries[if_in_rightmost_regions_pc].address = instructions.program_counter

        instructions.add_comment("END : Determine Region X") 

#determine the Y of the region on screen
def determine_region_y():

        instructions.add_comment("BEGIN : Determine Region Y") 

        load_a(bus_constants.MOUSE_ADDR2, "a = mouse_y")


        ###################################################################  

        load_b(ram_constants.LINE_Y_1_ADDR, "b = line_y_1")
        if_in_topmost_regions_pc = instructions.program_counter
        branch_a_lesser_b(0, "if mouse_y_curr < line_y_1")

        ###################################################################  


        load_b(ram_constants.LINE_Y_2_ADDR, "b = line_y_2")
        if_in_bottommost_regions_pc = instructions.program_counter
        branch_a_greater_b(0, "if mouse_y_curr > line_y_2")

        ###################################################################  

        region_is_center_pc = instructions.program_counter
        goto(0, "region detected")
        

        ###################################################################  
        
        instructions.entries[if_in_topmost_regions_pc].address = instructions.program_counter
        load_a(ram_constants.REGION_SELECTED_CURR_ADDR, "a = region")
        load_b(ram_constants.ONE_ADDR, "b = 1")
        alu_a_add_b('A', "a = region + 1")
        need_region_increment_pc = instructions.program_counter
        goto(0, "region detected")

        ###################################################################     

        instructions.entries[if_in_bottommost_regions_pc].address = instructions.program_counter
        load_a(ram_constants.REGION_SELECTED_CURR_ADDR, "a = region")
        load_b(ram_constants.TWO_ADDR, "b = 2")
        alu_a_add_b('A', "a = region + 2")
        #no need to jump as simply can store

        ###################################################################  
        
        instructions.entries[need_region_increment_pc].address = instructions.program_counter
        store_a(ram_constants.REGION_SELECTED_CURR_ADDR, "region = a")

        ###################################################################  

        instructions.entries[region_is_center_pc].address = instructions.program_counter


        ###################################################################






        instructions.add_comment("END : Determine Region Y") 

#determine full number that is to be the screen region
def determine_screen_region():

#--------------
#| 5 | 1 | 9  |
#--------------
#| 4 | 0 | 8  |
#--------------
#| 6 | 2 | 10 |
#--------------


        instructions.add_comment("BEGIN : Determine Screen Region") 

        determine_region_x()
        determine_region_y()

        instructions.add_comment("END : Determine Screen Region") 

        
##########################################################
#################      COLOUR OPERATIONS
##########################################################

#function inverts colours
def invert_colours():


        instructions.add_comment("BEGIN : Invert Colours") 


        load_a(ram_constants.COLOUR_FG_ADDR, "a = config_fg")           
        load_b(ram_constants.COLOUR_BG_ADDR, "b = config_bg")           
      
        store_b(ram_constants.COLOUR_FG_ADDR, "config_fg = b")        
        store_a(ram_constants.COLOUR_BG_ADDR, "config_bg = a")         

        store_b(bus_constants.VGA_ADDR2, "0xB0 =  b ie.  fg_new -> bg_old")                
        store_a(bus_constants.VGA_ADDR1, "0xB1 =  a i.e. bg_new -> fg_old")                

        store_a(bus_constants.LED_ADDR0, "0xC0 =  bg_new")                      
        store_b(bus_constants.LED_ADDR1, "0xC0 =  fg_new")


        instructions.add_comment("END : Invert Colours") 

        
#function reads colour values from RAM and updates it, according to
#the car selected, in the VGA interface
def write_colours():


        instructions.add_comment("BEGIN : Write Colours") 
    

        rom_constants.WRITE_COLOURS_PC = instructions.program_counter

        ###################################################################

        load_a(ram_constants.CAR_SELECTED_ADDR, "a = car_selected")
        load_b(ram_constants.ZERO_ADDR, "b = 0") 
        is_car_green_pc = instructions.program_counter
        branch_a_greater_b(0, "if car > 0, branch away")

        load_a(ram_constants.GREEN_COLOUR_ADDR, "a = green")
        car_is_green_pc = instructions.program_counter  
        goto(0, "car = 0")

        ###################################################################

        instructions.entries[is_car_green_pc].address = instructions.program_counter


        #load_a(ram_constants.CAR_SELECTED_ADDR, "a = car_selected")
        load_b(ram_constants.ONE_ADDR, "b = 1") 
        is_car_blue_pc = instructions.program_counter
        branch_a_greater_b(0, "if car > 1, branch away")

        load_a(ram_constants.BLUE_COLOUR_ADDR, "a = blue")
        car_is_blue_pc = instructions.program_counter   
        goto(0, "car = 1")


        ###################################################################

        instructions.entries[is_car_blue_pc].address = instructions.program_counter     


        #load_a(ram_constants.CAR_SELECTED_ADDR, "a = car_selected")
        load_b(ram_constants.TWO_ADDR, "b = 2") 
        is_car_red_pc = instructions.program_counter
        branch_a_greater_b(0, "if car > 2, branch away")

        load_a(ram_constants.RED_COLOUR_ADDR, "a = red")
        car_is_red_pc = instructions.program_counter    
        goto(0, "car = 2")

        ###################################################################

        instructions.entries[is_car_red_pc].address = instructions.program_counter      

        load_a(ram_constants.YELLOW_COLOUR_ADDR, "toherwise, a = yellow")

        ###################################################################

        instructions.entries[car_is_green_pc].address = instructions.program_counter
        instructions.entries[car_is_blue_pc].address = instructions.program_counter
        instructions.entries[car_is_red_pc].address = instructions.program_counter


        load_b(ram_constants.COLOUR_BG_ADDR, "b = config_bg")                 

        store_b(bus_constants.VGA_ADDR2, "0xB0 =  b ie.  fg_new -> config_fg")                
        store_a(bus_constants.VGA_ADDR1, "0xB1 =  a i.e. bg_new -> config_bg")  

        function_return("colours updated")            

        instructions.add_comment("END : Write Colours") 


##########################################################
#################      IR TRANSMITTER FUNCTIONS
##########################################################

#function to flash leds based on timer value

def timer_visualize():


        instructions.add_comment("BEGIN : Visualize Timer Value")


        load_a(bus_constants.TIMER_ADDR0, "a = timer_curr (in seconds)")
        store_a(bus_constants.SEVEN_SEG_ADDR0, "seven_seg[0] = a")

        ###################################################################

        load_b(ram_constants.ZERO_ADDR, "b = 0")

        if_timer_zero_pc = instructions.program_counter
        branch_a_greater_b(0, "branch away if timer is > 0")
        load_a(ram_constants.MAX_ADDR, "a = 255")
        store_a(bus_constants.LED_ADDR0, "led[0] = 255")
        store_a(bus_constants.LED_ADDR1, "led[1] = 255")

        timer_zero_done_pc = instructions.program_counter
        goto(0, "flash leds done")

        instructions.entries[if_timer_zero_pc].address = instructions.program_counter

        
        store_b(bus_constants.LED_ADDR0, "led[0] = 0")
        store_b(bus_constants.LED_ADDR1, "led[1] = 0") 

        ###################################################################

        instructions.entries[timer_zero_done_pc].address = instructions.program_counter

        instructions.add_comment("END : Visualize Timer Value")


#write the command, determined by mouse position, selected
#car and speeds to the IR peripheral
def write_to_ir():

        instructions.add_comment("BEGIN : Write To IR")     

        load_a(bus_constants.MOUSE_ADDR3, "a = mouse_scroll_value")
        store_a(ram_constants.CAR_SELECTED_ADDR, "car_selected = a")
        store_a(bus_constants.IR_ADDR2, "ir_car_selected = car_select")
        
        load_a(ram_constants.REGION_SELECTED_CURR_ADDR, "a = region_selected")
        store_a(bus_constants.IR_ADDR0, "ir_command = region_select")

        ###################################################################

        load_b(ram_constants.REGION_SELECTED_PREV_ADDR, "b = region_prev")
        if_same_region_pc = instructions.program_counter
        branch_a_eq_b(0, "do not reset if same region")

        
        store_a(bus_constants.TIMER_ADDR2, "reset timer")
        store_a(ram_constants.REGION_SELECTED_PREV_ADDR, "region_prev = region_curr")
        
        
        ###################################################################

        instructions.entries[if_same_region_pc].address = instructions.program_counter

        ###################################################################


        timer_visualize()





        ###################################################################


        instructions.add_comment("END : Write To IR")         





##########################################################
#################      DRAW THE MOUSE POINTER
##########################################################


#function returns original pattern value to pixel
def clear_prev_mouse_pointer():

        instructions.add_comment("BEGIN : Clear Previous Mouse Pointer")     

        load_a(ram_constants.MOUSE_X_PREV_ADDR, "a = prev_mouse_x")
        store_a(ram_constants.X_CURR_ADDR, "x_curr = prev_mouse_x")

        load_a(ram_constants.MOUSE_Y_PREV_ADDR, "a = prev_mouse_y")
        store_a(ram_constants.Y_CURR_ADDR, "y_curr = prev_mouse_y")

        function_call(rom_constants.PIXEL_WRITE_PC, "restore old pixel value")
        
        instructions.add_comment("END : Clear Previous Mouse Pointer")   


#functions records new mouse position as new previous one
def draw_new_mouse_pointer():

        instructions.add_comment("BEGIN : Draw New Mouse Pointer")   
        
        load_a(bus_constants.MOUSE_ADDR1, "a = mouse_x")
        store_a(ram_constants.MOUSE_X_PREV_ADDR, "prev_mouse_x = mouse_x")
        store_a(bus_constants.VGA_ADDR0, "0xB0 = x")
        

        load_a(bus_constants.MOUSE_ADDR2, "b = mouse_y")
        store_a(ram_constants.MOUSE_Y_PREV_ADDR, "prev_mouse_y = mouse_y")
        store_a(bus_constants.VGA_ADDR1, "0xB1 = y")
        
                
        load_a(ram_constants.ONE_ADDR, "pixel = 1")  
        store_a(bus_constants.VGA_ADDR2, "0xB2 =  pixel")  
               
        instructions.add_comment("END : Draw New Mouse Pointer") 

#function executes all the mouse pointer drawing exectuion
def update_mouse_pointer():

        instructions.add_comment("BEGIN : Update Mouse Pointer") 
    

        clear_prev_mouse_pointer()
        draw_new_mouse_pointer()

        instructions.add_comment("END : Update Mouse Pointer")         


#function gets the speed setting from the switches for the mouse
def update_mouse_speeds():

        instructions.add_comment("BEGIN : Update Mouse Speeds")     

        load_a(bus_constants.SWITCH_ADDR0, "a = switches[7:0]")
        store_a(bus_constants.MOUSE_ADDR4, "mouse_speeds = a")

        instructions.add_comment("END : Update Mouse Speeds")  

#function obtains the action from the mouse
def get_mouse_action():
        
        instructions.add_comment("BEGIN : Get Mouse Action")   

        load_a(bus_constants.MOUSE_ADDR0, "a = mouse_status")
        load_b(ram_constants.MOUSE_STATUS_PREV_ADDR, "b = mouse_status_prev")
        if_same_status_pc = instructions.program_counter
        branch_a_eq_b(0, "if same status, jump away")

        ###################################################################

        store_a(ram_constants.MOUSE_STATUS_PREV_ADDR, "mouse_status_prev = mouse_status")
        store_a(bus_constants.IR_ADDR1, "ir[1]  = a")

        ###################################################################

        instructions.entries[if_same_status_pc].address = instructions.program_counter
        instructions.add_comment("END : Get Mouse Action")      


##########################################################
#################      TIMER INTERRUPT HANDLER
##########################################################

#timer interrupt handler
def handle_timer_interrupt():


        instructions.add_comment("BEGIN : Handle Timer Interrupt") 

        rom_constants.TIMER_INTERRUPT_PC = instructions.program_counter

        get_mouse_action()  
      
        write_to_ir()

        function_call(rom_constants.WRITE_COLOURS_PC, "rewrite_colours")



        instructions.add_comment("END : Handle Timer Interrupt") 

##########################################################
#################      TIMER INTERRUPT HANDLER
##########################################################

#timer interrupt handler
def handle_mouse_interrupt():


        instructions.add_comment("BEGIN : Handle Mouse Interrupt")     

        rom_constants.MOUSE_INTERRUPT_PC = instructions.program_counter

        update_mouse_pointer()
        determine_screen_region()
        update_mouse_speeds()
        
        instructions.add_comment("END : Handle Mouse Interrupt")   


##########################################################
#################      INTERRUPT ADDRESS INSERTER
##########################################################
                
#insert interrupt addresses into corresponding positions in the ROM
def insert_interrupt_addresses():

        instructions.insert_value(rom_constants.TIMER_INTERRUPT_LOC, rom_constants.TIMER_INTERRUPT_PC, "Timer interrupt address")
        instructions.insert_value(rom_constants.MOUSE_INTERRUPT_LOC, rom_constants.MOUSE_INTERRUPT_PC , "Mouse interrup address")
        

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################     GENERATE INSTRUCTIONS
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#initialize the functions in memory
branch_pc = instructions.program_counter
goto(0, "write pattern")
write_current_pixel()
write_colours()
instructions.entries[branch_pc].address =  instructions.program_counter


#begin execution
function_call(rom_constants.WRITE_COLOURS_PC, "write current pixel value")          
write_vga_pattern()
goto_idle("Wait for interrupts")
handle_timer_interrupt()
goto_idle("Wait for interrupts")
handle_mouse_interrupt()
goto_idle("Wait for interrupts")


#after functions define, take the required PCs and insert into ROM
insert_interrupt_addresses()
instructions.write(output_file)

#testing instruction set

#address = 0
#register = 'B'
#load_a(0)
#load_b(0)
#store_a(0)
#store_b(0)
#alu_a_add_b(register)
#alu_a_sub_b(register)
#alu_a_mult_b(register)
#alu_shift_a_l(register)
#alu_shift_a_r(register)
#alu_incr_a(register)
#alu_incr_b(register)
#alu_decr_a(register)
#alu_decr_b(register)
#alu_a_eq_b(register)
#alu_a_greater_b(register)
#alu_a_lesser_b(register)
#alu_a_mod_b(register)
#alu_a_and_b(register)
#alu_not_a(register)
#alu_a_or_b(register)
#branch_a_eq_b(address)
#branch_a_greater_b(address)
#branch_a_lesser_b(address)
#goto(address)
#goto_idle()
#function_call(address)
#function_return()
#deref_a()
#deref_b()
#nop()














                
