#!/usr/bin/python

###############################################################################
##
## Author        : Jevgenij Zubovskij
## Company       : University of Edinburgh
##
## Module Name   : ram_generator.py
## Project       : Microprocessor
## Tools Version : Python 2.7
##
## Description   : This Python module generates the RAM initialisation file.
## Entries can be inserted at any specified address in the ram size range. 
## Moreover, it allows (debug) generating the RAM file in a manner that 
## allows it to be directly copy-pasted into the RAM Verilog module, allowing 
## it to be used for simulation purposes.
##
## All entries have a comment associated with them to indicate their 
## meaning, which can be left empty if user chooses.
##
## Also allows printing overall statistics for the RAM and writing contents to
## a speficifed file
##
###############################################################################

import sys
from generator_constants import Ram_Constants 

ram_constants = Ram_Constants()


#RAM entry object
class RAM_Entry:

        def __init__(self, address, value, meaning, debug):
                self.value   = value
                self.address = address
                self.meaning = meaning
                self.debug   = debug

        def __repr__(self):
                return self. __str__()

	#get the string value of the entry
        def __str__(self):
                if self.debug:
                        return "Mem[{}] = 8\'b{};".format(self.address, str(format(self.value, '08b')))
                else:
                        return "{}".format(str(format(self.value, '08b')))


        #custom comparison functions to use standard sorting
        def __eq__(self, other):        
                return self.value == other.value and self.address == self.address

        def __ne__(self, other):
                return not self.__eq__(other)

        def __lt__(self, other):
                return self.address < self.address

        def __gt__(self, other):
                return self.address > self.address

        def __le__(self, other):
                return (self < other) or (self == other)

        def __ge__(self, other):
                return (self > other) or (self == other)

#RAM array object, containing all entries and relatied information
class RAM:
        
        def __init__(self, max_size, debug):

                #initialize array
                self.debug = debug
                self.entries = [RAM_Entry(index, 0, ram_constants.UNUSED_STRING , self.debug) for index in range(max_size)]
                self.max_size = max_size
                self.entries_assigned = 0
                self.max_address_used = 0

	#add entry to the ram at specified address
        def add(self, address, value, meaning):

                #create a new entry
                new_entry = RAM_Entry(address, value, meaning, self.debug)


                #record assigning an entry
                if self.entries[address % self.max_size].meaning == ram_constants.UNUSED_STRING:
                        self.entries_assigned += 1

                #replace entry  
                self.entries[address % self.max_size] = new_entry;
                #sort the array
                self.entries.sort()
                
                #update max address
                self.max_address_used =  self.max_address_used if (self.max_address_used > address) else address        

                #make sure RAM 
                if address >= self.max_size:
                        print "***{} is more than maximum RAM size {}\nAssigning to RAM[{}] instead".format(address, self.max_size, address % self.max_size)


        #print ram contents
        def print_contents(self):
                
                print "\n"
                print "-------------------------------------------------------------------"
                print "------------Processor RAM Contents---------------------------------"
                print "-------------------------------------------------------------------"
				
                print "{:>7}{:<12}{:<30}".format("|ADDR|", "|Value|", "|Comment|")
                print "-------------------------------------------------------------------"
                for index in range(self.max_size):

                        meaning = self.entries[index].meaning
                        address = self.entries[index].address
                        value = self.entries[index].value

                        

                        #only print used entries
                        if meaning != ram_constants.UNUSED_STRING:
                                print "@:{:>3} : 0x{:>2} ({:>3}) //{}".format \
								(address, str(format(value, '02x')),str(int(value)), meaning)


                print "-------------------------------------------------------------------"
                print "------------Processor RAM Contents End-----------------------------"
                print "-------------------------------------------------------------------"
                print "\n"

        #print general statistics stats
        def print_stats(self):

                print "\n"
                print "-------------------------------------------------------------------"
                print "------------Processor RAM Overview---------------------------------"
                print "-------------------------------------------------------------------"
                
                print "RAM : {:<20} : {}".format("Entries Assigned ", self.entries_assigned)
                print "RAM : {:<20} : {}".format("Max Address Used", self.max_address_used)
                print "RAM : {:<20} : {}".format("Mamixum Size", self.max_size)
		print "RAM : {:<20} : {}".format("Remaining Space", (self.max_size - self.entries_assigned))
                
                print "-------------------------------------------------------------------"
                print "------------Processor RAM Overview End-----------------------------"
                print "-------------------------------------------------------------------"
                print "\n"

	#write entries to specified file
        def write(self, output_file):
                
                #print statistics of use
                self.print_stats()

                #print ram contents
                self.print_contents()


                #write the entries
                output = open(output_file, 'w')

                for index in range(self.max_size):
                        output.write("{}\n".format(str(self.entries[index])))

                output.close()



                
