#!/usr/bin/python

###############################################################################
##
## Author        : Jevgenij Zubovskij
## Company       : University of Edinburgh
##
## Module Name   : rom_generator.py
## Project       : Microprocessor
## Tools Version : Python 2.7
##
## Description   : This Python module generates the file containing all of the
## supported instructions. It also inserts the interrupt addresses into said 
## instructions file. The generations happens via printing the assembly 
## instructions with specified parameters. The Program counter (PC) is 
## incremented on each instruction insertion (generation / printing). 
## 
##
## Insertion of lietrals at a specified program counter also supported.
## 
## Allows setting the jump address for a insturction at any program counter,
## removing the need for knowing jump address at the time it is called (which
## needs manual calculation). This allows automatic calculation of it.
##
## All entries have a comment associated with them to indicate their 
## meaning, which can be left empty if user chooses.
##
## Also allows printing overall statistics for the ROM and writing contents to
## a speficifed file
##
## Every specific instruction (assembly one) is added to the rom via a function
## defined in this file, which creates said instruction entry with the 
## corresponding byte width, instruction string, comment specified by user etc.
##
## Hence, allowing much easier coding
##
###############################################################################


import sys
from generator_constants import Rom_Constants 
from generator_constants import Ram_Constants 
from generator_constants import Bus_Constants 

debug = 0


output_file = "./Intermediate Representations/program_instructions.txt"

rom_constants = Rom_Constants()
ram_constants = Ram_Constants()
bus_constants = Bus_Constants()



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################     ROM OBJECT DEFINITIONS
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




##########################################################
#################     SINGLUAR INSTRUCTION OBJECT
##########################################################

class Instruction():
        
        
        def __init__(self, program_counter, command_strings, address, width_bytes, meaning):

                self.program_counter = program_counter
                self.command_strings = command_strings
                self.address         = address
                self.width_bytes     = width_bytes
                self.meaning         = meaning

        def __repr__(self):
                return self. __str__()

              
	#function to get entry to string                  
        def __str__(self):


                result = ""
                

                #do not print nonexistent entries
                if self.command_strings[0] == rom_constants.TRASH_STRING:
                        return rom_constants.TRASH_STRING

                #if it is an interrupt address
                if self.command_strings[0] == rom_constants.LITERAL_STRING:
                        return "{}".format(str(format(int(self.command_strings[1]), '08b')))

                #combine all elements into one array
                command_string = ""
                for index in range(len(self.command_strings)):
                        command_string += "{} ".format(self.command_strings[index])

                
                #convert to int for formatting
                address = self.get_address()

                #make it into a copy-pasteable format
                if debug:
                        result += "ROM[{}] = 8\'b{:<20};".format(self.program_counter, command_string)
                        if self.width_bytes > 1:
                                result += "ROM[{}] = 8\'b{:4};".format(self.program_counter+1,str(format(address, '08b')))                              

                else:
                #otherwise generate actual bits
                        result += "{:<20}".format(command_string)
                        if self.width_bytes > 1:
                                result += "0x{:4}".format(str(format(address, '02x')))
                return result

        #custom comparison functions to use standard sorting
        def __eq__(self, other):        
                return self.program_counter == other.program_counter and self.command_strings == self.command_strings

        def __ne__(self, other):
                return not self.__eq__(other)

        def __lt__(self, other):
                return self.program_counter < self.program_counter

        def __gt__(self, other):
                return self.program_counter > self.program_counter

        def __le__(self, other):
                return (self < other) or (self == other)

        def __ge__(self, other):
                return (self > other) or (self == other)

        def get_address(self):
                return self.address if isinstance( (self.address), int ) else int(self.address,16)



##########################################################
#################     ROM COMMENT OBJECT
##########################################################

class Instruction_Comment():
        
        
        def __init__(self, program_counter, value):

                self.program_counter = program_counter
                self.value = value

        def __repr__(self):
                return self. __str__()

                       
	#to string function         
        def __str__(self):

                return str(self.value)

        #custom comparison functions to use standard sorting
        def __eq__(self, other):        
                return self.program_counter == other.program_counter and self.command_strings == self.command_strings

        def __ne__(self, other):
                return not self.__eq__(other)

        def __lt__(self, other):
                return self.program_counter < self.program_counter

        def __gt__(self, other):
                return self.program_counter > self.program_counter

        def __le__(self, other):
                return (self < other) or (self == other)

        def __ge__(self, other):
                return (self > other) or (self == other)

        def get_address(self):
                return self.address if isinstance( (self.address), int ) else int(self.address,16)        

##########################################################
#################     ROM OBJECT
##########################################################


class Instruction_Set:

        #max size in bytes
        def __init__(self, max_size):

                #initialize array
                self.entries =  [Instruction(index, [rom_constants.NOP_STRING], 0, rom_constants.NOP_WIDTH, \
                                                                rom_constants.NOP_STRING) for index in range(max_size)]
                                                                
                self.max_size = max_size
                self.program_counter = 0
                self.instruction_count = 0
                self.entries_assigned = 0
                self.comments = []

        def add(self, strings, address, width, meaning):

                #create a new entry
                new_entry = Instruction(self.program_counter, strings, address, width, meaning)

                #replace entry  
                self.entries[self.program_counter % self.max_size] = new_entry;
                

                #remove instructions that are displaced
                if width > 1:
                        self.entries[self.program_counter+1] = \
                                                Instruction(0, [rom_constants.TRASH_STRING], 0, 0, rom_constants.TRASH_STRING)

                #increment number of actual instructions
                self.instruction_count += 1
                #increment number of entries
                self.entries_assigned += width

                #make sure RAM 
                if self.program_counter >= self.max_size:
                        print "***{} is more than maximum ROM size {}\nAssigning to ROM[{}] instead".format \
                                                (self.program_counter, self.max_size, self.program_counter % self.max_size)

                #increment program counter
                self.program_counter += width


        #insert literal at specified program counter
        def insert_value(self, index, value, meaning):
                #increment number of entries
                self.entries_assigned += 1
                #add literal into entries
                self.entries[index] = Instruction(index, [rom_constants.LITERAL_STRING, str(value)], 0, rom_constants.LITERAL_WIDTH, meaning)

        #insert comment between instructions at specified program counter
        def insert_comment(self, program_counter, value):

                self.comments.append(Instruction_Comment(program_counter, value))

        #add comment at currrent program counter
        def add_comment(self, value):

                self.comments.append(Instruction_Comment(self.program_counter, value))


        #print the entries
        def print_entries(self):

                print "\n"
                print "-------------------------------------------------------------------"
                print "------------Processor ROM Contents---------------------------------"
                print "-------------------------------------------------------------------"

                print "{:>6}{:<34}{:>11}".format("|PC|", "|Instruction|", "|Comment|")
                print "-------------------------------------------------------------------"
                for index in range(self.max_size):


                        #always print preceeding comments
                        for comment in self.comments:
                            if comment.program_counter == index:
                                print str(comment)
                        
                        
                        program_counter = self.entries[index].program_counter
                        value = "{}".format(str(self.entries[index]))
                                                #only print address if such exists
                                                
                        #make sure to print address for the literal
                        if self.entries[index].command_strings[0] == rom_constants.LITERAL_STRING:
                                address = "({:>3})".format(int(self.entries[index].command_strings[1]))
                                                           
                        #do not necessarily do that for the rest                                   
                        else:        
                                address = "({:>3})".format("N/A") if self.entries[index].width_bytes < 2 \
                                                           else "({:>3})".format(int(self.entries[index].get_address()))
                                                
                                                
                                
                        meaning = self.entries[index].meaning
        


                        entry_string = str(self.entries[index])
                        #only print valid entries
                        if meaning != rom_constants.TRASH_STRING and meaning != rom_constants.NOP_STRING:
                                print "@:{:>3} : {:<28}{} //{}".format(program_counter, value, address, meaning)

                print "-------------------------------------------------------------------"
                print "------------Processor ROM Contents End-----------------------------"
                print "-------------------------------------------------------------------"
                print "\n"

        #print ROM statistics
        def print_stats(self):

                print "\n"
                print "-------------------------------------------------------------------"
                print "------------Processor ROM Overview---------------------------------"
                print "-------------------------------------------------------------------"

                print "ROM : {:<20} : {}".format("Program Counter", self.program_counter)
                print "ROM : {:<20} : {}".format("Instruction Count", self.instruction_count)
                print "ROM : {:<20} : {}".format("Entry Count", self.entries_assigned)
                print "ROM : {:<20} : {}".format("Maximum Size", self.max_size)
                print "RAM : {:<20} : {}".format("Remaining Space", (self.max_size - self.entries_assigned))


                print "-------------------------------------------------------------------"
                print "------------Processor ROM Overview End-----------------------------"
                print "-------------------------------------------------------------------"
                print "\n"

        def write(self, output_file):


                #write the entries
                output = open(output_file, 'w')

                #print overall statistics
                self.print_stats()

                #check rom contents
                self.print_entries()

                for index in range(self.max_size):
                        entry_string = str(self.entries[index])

                        #only print valid entries
                        if entry_string != rom_constants.TRASH_STRING:
                                output.write("{}\n".format(str(self.entries[index])))

                output.close()


instructions = Instruction_Set(rom_constants.MAX_SIZE)



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################     MODULAR OPERATIONS
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



##########################################################
#################     MEMORY OPERATIONS
##########################################################

def load_a(address, meaning):

        register = 'A'
        strings = []

        strings.append(rom_constants.LOAD_STRING)
        strings.append(register)
        address = hex(address)
        width   = rom_constants.LOAD_WIDTH

        instructions.add(strings, address, width, meaning)


def load_b(address, meaning):

        register = 'B'
        strings = []

        strings.append(rom_constants.LOAD_STRING)
        strings.append(register)
        address = hex(address)
        width   = rom_constants.LOAD_WIDTH

        instructions.add(strings, address, width, meaning)

##########################################################

def store_a(address, meaning):

        register = 'A'
        strings = []

        strings.append(rom_constants.STORE_STRING)
        strings.append(register)
        address = hex(address)
        width   = rom_constants.STORE_WIDTH

        instructions.add(strings, address, width, meaning)


def store_b(address, meaning):

        register = 'B'
        strings = []

        strings.append(rom_constants.STORE_STRING)
        strings.append(register)
        address = hex(address)
        width   = rom_constants.STORE_WIDTH


        instructions.add(strings, address, width, meaning)


##########################################################
#################     ALU OPERATIONS
##########################################################

def alu_a_add_b(register, meaning):

        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_A_ADD_B_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)


def alu_a_sub_b(register, meaning):

        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_A_SUB_B_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)


def alu_a_mult_b(register, meaning):

        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_A_MULT_B_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)



##########################################################

def alu_shift_a_l(register, meaning):

        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_SHIFT_A_L_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)


def alu_shift_a_r(register, meaning):

        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_SHIFT_A_R_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)
         
##########################################################

def alu_incr_a(register, meaning):



        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_INCR_A_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)


def alu_incr_b(register, meaning):

        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_INCR_B_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)

##########################################################

def alu_decr_a(register, meaning):
   

        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_DECR_A_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)


def alu_decr_b(register, meaning):

        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_DECR_B_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)

##########################################################

def alu_a_eq_b(register, meaning):


        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_A_EQ_B_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)


def alu_a_greater_b(register, meaning):
 
        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_A_GREATER_B_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)

def alu_a_lesser_b(register, meaning):

        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_A_LESSER_B_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)

def alu_a_mod_b(register, meaning):


        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_A_MOD_B_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)

def alu_a_and_b(register, meaning):

        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_A_AND_B_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)

def alu_not_a(register, meaning):

        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_NOT_A_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)

def alu_a_or_b(register, meaning):

        strings = []

        strings.append(rom_constants.ALU_STRING)
        strings.append(register)
        strings.append(rom_constants.ALU_A_OR_B_STRING)
        width  = rom_constants.ALU_WIDTH

        instructions.add(strings, 0, width, meaning)


##########################################################
#################     BRANCHING
##########################################################
         
def branch_a_eq_b(address, meaning):


        strings = []

        strings.append(rom_constants.BRANCH_STRING)
        strings.append(rom_constants.ALU_A_EQ_B_STRING)
        address = hex(address)
        width  = rom_constants.BRANCH_WIDTH

        instructions.add(strings, address , width, meaning)


def branch_a_greater_b(address, meaning):

        strings = []

        strings.append(rom_constants.BRANCH_STRING)
        strings.append(rom_constants.ALU_A_GREATER_B_STRING)
        address = hex(address)
        width  = rom_constants.BRANCH_WIDTH

        instructions.add(strings, address , width, meaning)
 

def branch_a_lesser_b(address, meaning):

        strings = []

        strings.append(rom_constants.BRANCH_STRING)
        strings.append(rom_constants.ALU_A_LESSER_B_STRING)
        address = hex(address)
        width  = rom_constants.BRANCH_WIDTH

        instructions.add(strings, address , width, meaning)

##########################################################

def goto(address, meaning): 

        strings = []

        strings.append(rom_constants.GOTO_STRING)
        address = hex(address)
        width  = rom_constants.GOTO_WIDTH

        instructions.add(strings, address , width, meaning)

##########################################################

def goto_idle(meaning):

        strings = []

        strings.append(rom_constants.GOTO_IDLE_STRING)
        width  = rom_constants.GOTO_IDLE_WIDTH

        instructions.add(strings, 0, width, meaning)


##########################################################
#################     FUNCTIONS
##########################################################

def function_call(address, meaning):
        
        strings = []

        strings.append(rom_constants.FUNCTION_CALL_STRING)
        address = hex(address)
        width   = rom_constants.FUNCTION_CALL_WIDTH

        instructions.add(strings, address , width, meaning)

def function_return(meaning):

        strings = []

        strings.append(rom_constants.RETURN_STRING)
        width   = rom_constants.RETURN_WIDTH

        instructions.add(strings, 0, width, meaning)

##########################################################
#################     DEREFERENCING 
##########################################################

def deref_a(meaning):


        strings = []

        strings.append(rom_constants.DEREF_STRING)
        strings.append(rom_constants.DEREF_A_INTO_A_STRING)
        width   = rom_constants.DEREF_WIDTH

        instructions.add(strings, address , width, meaning)

def deref_b(meaning):


        strings = []

        strings.append(rom_constants.DEREF_STRING)
        strings.append(rom_constants.DEREF_B_INTO_B_STRING)
        width   = rom_constants.DEREF_WIDTH

        instructions.add(strings, address , width, meaning)


##########################################################


##########################################################
#################     NOP 
##########################################################

def nop(meaning):


        strings = []

        strings.append(rom_constants.NOP_STRING)
        width   = rom_constants.NOP_WIDTH

        instructions.add(strings, address , width, meaning)

##########################################################




#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################     GENERIC FUNCTIONS
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



##########################################################
#################     GENERIC COUNTER
##########################################################

def mod_n_counter_update(current_value_address, n_address, value_reset_address, variable_name):

        instructions.add_comment("BEGIN : Generic Counter for {}".format(variable_name))

        
        load_a(current_value_address, "a = {}_curr".format(variable_name, variable_name))
        load_b(n_address, "b = {}_max".format(variable_name))    
        alu_incr_a('A', "a = {}_curr + 1".format(variable_name))  
  
        alu_a_mod_b('A', "a = ({}_curr + 1) % {}_dim".format(variable_name,variable_name))       


        #if need to record its reset
        if(value_reset_address != "NONE" ):

                store_a(current_value_address, "{}_curr = {}_curr + 1) % {}_dim".format(variable_name, variable_name, variable_name))           

                load_b(ram_constants.ZERO_ADDR, "b = 0")     
                alu_a_eq_b('B', "b = {}_reset".format(variable_name))       
                store_b(value_reset_address, "{}_reset = ({}_curr + 1) % {}_max == 0".format \
                                                              (variable_name, variable_name, variable_name)) 

        else:
                
                    store_a(current_value_address, "{}_curr = {}_curr + 1) % {}_dim".format \
                            (variable_name, variable_name, variable_name))

        instructions.add_comment("END : Generic Counter for {}".format(variable_name))

##########################################################
#################     GENERIC IS-MULTIPLE-OF CHECKER
##########################################################

def is_value_multiple_of_n(current_value_address, n_address, value_is_multiple_address, value_name, n_name, multiple_name):

        instructions.add_comment("BEGIN : Is Multiple of N for {}".format(variable_name))
        
        load_a(current_value_address, "a = {}".format(value_name)) 
        load_b(n_address, "b = {}".format(n_name))      
        alu_a_mod_b('A', "a = {} % {}".format(value_name, n_name))

        load_b(ram_constants.ZERO_ADDR, "b = 0")
        alu_a_eq_b('A', "a =  {} % {} == 0".format(value_name, n_name))
  
        store_a(value_is_multiple_address, "{} = {} % {} == 0".format(multiple_name, value_name, n_name))  

        instructions.add_comment("END : Is Multiple of N for {}".format(variable_name))




                
