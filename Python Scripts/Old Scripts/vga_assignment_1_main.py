#!/usr/bin/python

###############################################################################
##
## Author        : Jevgenij Zubovskij
## Company       : University of Edinburgh
##
## Module Name   : program_generator.py
## Project       : Microprocessor
## Tools Version : Python 2.7
##
## Description   : This Python module generates the ROM and RAM files
##
###############################################################################


import sys
from generator_constants import Rom_Constants 
from generator_constants import Ram_Constants 
from generator_constants import Bus_Constants 
from ram_generator       import RAM
from rom_generator       import *


rom_constants = Rom_Constants()
ram_constants = Ram_Constants()
bus_constants = Bus_Constants()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################     GENERATE RAM
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


debug = 0

if len(sys.argv) > 1 and int(sys.argv[1]) == 1:
         debug = 1
         print 'DEBUG MODE RAM GENERATION'


ram_constants = Ram_Constants()
ram = RAM(ram_constants.MAX_SIZE, debug)


ram.add(ram_constants.ZERO_ADDR, 0, "0 constant")
ram.add(ram_constants.ONE_ADDR,  1, "1 constant")
ram.add(ram_constants.TWO_ADDR,  2, "2 constant")
ram.add(ram_constants.THREE_ADDR,3, "3 constant")
ram.add(ram_constants.FOUR_ADDR, 4, "4 constant")

#x current, max and reset and even
ram.add(ram_constants.X_CURR_ADDR, 0,   "x_curr")
ram.add(ram_constants.X_DIM_ADDR,  160, "x_max")
ram.add(ram_constants.X_RESET_ADDR,0,   "x_reset")
ram.add(ram_constants.X_ODD_ADDR,  0,   "x_odd")

#y current, max and reset and even
ram.add(ram_constants.Y_CURR_ADDR, 0,   "y_curr")
ram.add(ram_constants.Y_DIM_ADDR,  120, "y_max")
ram.add(ram_constants.Y_RESET_ADDR,0,   "y_reset")
ram.add(ram_constants.Y_ODD_ADDR,  0,   "y_odd")

#pixel 
ram.add(ram_constants.PIXEL_ADDR,  0,   "pixel")

#colour config FG, BG
ram.add(ram_constants.COLOUR_FG_ADDR, 0xF0, "foreground_colour")
ram.add(ram_constants.COLOUR_BG_ADDR, 0x0D, "background_colour")

#timer (1s) counter max value and value
ram.add(ram_constants.TIMER_COUNTER_CURR_ADDR, 0, "timer_counter_curr")
ram.add(ram_constants.TIMER_COUNTER_DIM_ADDR,  20,"timer_counter_max")

#write the RAM out
ram_file = "../Memory Initialisation/Main_RAM.txt"
if debug:
	ram_file = "./Intermediate Representations/debug_ram.txt"

ram.write(ram_file)	
	

output_file = "./Intermediate Representations/program_instructions.txt"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################     PROGRAM PARAMETERS AND FUNCTIONS (MODULAR BLOCKS)
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



##########################################################
#################     X COUNTER UPDATE 
##########################################################



def update_x():


        load_a(ram_constants.X_CURR_ADDR, "a = x_curr")
        load_b(ram_constants.X_DIM_ADDR, "b = x_max")    
        alu_incr_a('A', "a = x_curr + 1")    
        alu_a_mod_b('A', "a = x % x_dim")       
        store_a(ram_constants.X_CURR_ADDR, "x_curr = x % x_dim ")  

        load_b(ram_constants.ZERO_ADDR, "b = 0")     
        alu_a_eq_b('B', "b = x_reset")       
	store_b(ram_constants.X_RESET_ADDR, "x_reset = (x_curr + 1) % x_max == 0") 

##########################################################
#################     Y COUNTER UPDATE 
##########################################################


def update_y():

	load_a(ram_constants.X_RESET_ADDR, "a = x_reset")		 
        load_b(ram_constants.ZERO_ADDR, "b = 0")               

	#save pc of the instruction that should do the jump
	branch_pc = instructions.program_counter
        branch_a_eq_b(0, "if x_reset == 0 i.e. not reset, don;t update y")          

        load_a(ram_constants.Y_CURR_ADDR, "a = y_curr")             
        load_b(ram_constants.Y_DIM_ADDR, "b = y_max")              
        alu_incr_a('A', "a = y_curr + 1")                 
        alu_a_mod_b('A', "a = y % y_dim")                
        store_a(ram_constants.Y_CURR_ADDR, "y_curr = y % y_dim ")            

        load_b(ram_constants.ZERO_ADDR, "b = 0")               
        alu_a_eq_b('B', "a = y_reset")                 
        store_b(ram_constants.Y_RESET_ADDR, "store y_reset")		 

	#make it the branch jump here
	instructions.entries[branch_pc].address =  instructions.program_counter

##########################################################
#################     CALCULATE IF X IS EVEN 
##########################################################


def is_x_odd():
       
	load_a(ram_constants.X_CURR_ADDR, "a = x") 
	load_b(ram_constants.TWO_ADDR, "b = 2")      
	alu_a_mod_b('A', "a = x % 2")     
        store_a(ram_constants.X_ODD_ADDR, "x_odd = x % 2")  
        

##########################################################
#################     CALCULATE IF Y IS EVEN 
##########################################################
        

def is_y_odd():
         
        load_a(ram_constants.Y_CURR_ADDR, "a = y") 
	load_b(ram_constants.TWO_ADDR, "b = 2")   
	alu_a_mod_b('A', "a = y % 2")   
        store_a(ram_constants.Y_ODD_ADDR, "y_odd = y % 2") 




##########################################################
#################     GET PIXEL VALUE BASED ON X and Y
##########################################################

def get_pixel():
       	        
        load_a(ram_constants.X_ODD_ADDR, "a = x_odd") 
        load_b(ram_constants.Y_ODD_ADDR, "b = y_odd ")                
        alu_a_or_b('A', "a = a | b")     
	
	load_b(ram_constants.ZERO_ADDR, "b = 0")    
	alu_a_eq_b('A', "a = x_even && y_even")      	

        store_a(ram_constants.PIXEL_ADDR, "pixel = ~y_odd && ~x_odd") 

##########################################################
#################     GET X, Y, PIXEL TO VGA
##########################################################

def write_to_vga():
              

        load_a(ram_constants.X_CURR_ADDR, "a = x") 
        store_a(bus_constants.VGA_ADDR0, "0xB0 = x")  

        load_a(ram_constants.Y_CURR_ADDR, "a = y") 
        store_a(bus_constants.VGA_ADDR1, "0xB1 = y")  

        load_a(ram_constants.PIXEL_ADDR, "a = pixel")  
        store_a(bus_constants.VGA_ADDR2, "0xB2 =  pixel")  
                
                
##########################################################
#################     FULL PATTERN GENERATOR 
##########################################################

def write_vga_pattern():


        
        is_x_odd()      # check if x % 2 = 0                   
        is_y_odd()      # check if y % 2 = 0
        get_pixel()     # pixel = (x % 2 == 0) && (y % 2 == 0)
        write_to_vga()  # x_addr = x, y_addr = y
        update_x()      # x = (x+1) % x_max
        update_y()      # y = (y+1) % y_max

        load_a(ram_constants.Y_RESET_ADDR, "a = vga_pattern_done") 	     
        load_b(ram_constants.ONE_ADDR, "b ==1")             

	#save pc of the instruction that should do the jump
	branch_pc = instructions.program_counter
        branch_a_eq_b(0, "jump over return to base address if a == b")        
 
        goto(rom_constants.BASE_PC, "start the loop over")            

	#make it the branch jump here
	instructions.entries[branch_pc].address =  instructions.program_counter

        rom_constants.PATTERN_DONE_PC = instructions.program_counter

##########################################################
#################      COUNTER PROCEDURE
##########################################################

def increment_counter():


        rom_constants.INCREMENT_COUNTER_PC = instructions.program_counter

        
        load_a(ram_constants.TIMER_COUNTER_CURR_ADDR, "a = timer_counter_curr")   
        load_b(ram_constants.TIMER_COUNTER_DIM_ADDR, "b = timer_counter_max")   
        alu_incr_a('A', "a = timer_counter_curr + 1")                  
        alu_a_mod_b('A', "a = timer_counter_curr % timer_counter_max")                 
        store_a(ram_constants.TIMER_COUNTER_CURR_ADDR, "timer_counter_curr = a") 

        load_b(ram_constants.ZERO_ADDR, "b = 0")                

	#save pc of the instruction that should do the jump
	branch_pc = instructions.program_counter
        branch_a_greater_b(0, "skip inversion unless reset the counter to 0")     

        load_a(ram_constants.COLOUR_FG_ADDR, "a = config_fg")           
        load_b(ram_constants.COLOUR_BG_ADDR, "b = config_bg")           
      
        store_b(ram_constants.COLOUR_FG_ADDR, "config_fg = b")        
        store_a(ram_constants.COLOUR_BG_ADDR, "config_bg = a")         

        store_b(bus_constants.VGA_ADDR2, "0xB0 =  b ie.  fg_new -> bg_old")                
        store_a(bus_constants.VGA_ADDR1, "0xB1 =  a i.e. bg_new -> fg_old")                

        store_a(bus_constants.LED_ADDR, "0xC0 =  bg_new")                      
        store_b(bus_constants.LED_ADDR, "0xC0 =  fg_new")                 
       
        store_b(bus_constants.SODD_SEG_ADDR_0, "0xD0 =  bg_new")                
        store_a(bus_constants.SODD_SEG_ADDR_1, "0xD1 =  fg_new")         


	#make it the branch jump here
	instructions.entries[branch_pc].address =  instructions.program_counter



##########################################################
#################      INTERRUPT ADDRESS INSERTER
##########################################################
                

def insert_interrupt_addresses():

	instructions.insert_value(rom_constants.TIMER_INTERRUPT_LOC, rom_constants.INCREMENT_COUNTER_PC, "Timer interrupt address")
	instructions.insert_value(rom_constants.MOUSE_INTERRUPT_LOC, rom_constants.INCREMENT_COUNTER_PC, "Mouse interrup address")
        

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################     GENERATE INSTRUCTIONS
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

write_vga_pattern()
increment_counter()
goto_idle("Wait for interrupts")
insert_interrupt_addresses()
instructions.write(output_file)

#testing instruction set

#address = 0
#register = 'B'
#load_a(0)
#load_b(0)
#store_a(0)
#store_b(0)
#alu_a_add_b(register)
#alu_a_sub_b(register)
#alu_a_mult_b(register)
#alu_shift_a_l(register)
#alu_shift_a_r(register)
#alu_incr_a(register)
#alu_incr_b(register)
#alu_decr_a(register)
#alu_decr_b(register)
#alu_a_eq_b(register)
#alu_a_greater_b(register)
#alu_a_lesser_b(register)
#alu_a_mod_b(register)
#alu_a_and_b(register)
#alu_not_a(register)
#alu_a_or_b(register)
#branch_a_eq_b(address)
#branch_a_greater_b(address)
#branch_a_lesser_b(address)
#goto(address)
#goto_idle()
#function_call(address)
#function_return()
#deref_a()
#deref_b()
#nop()














                
