#!/usr/bin/python

###############################################################################
##
## Author        : Jevgenij Zubovskij
## Company       : University of Edinburgh
##
## Module Name   : program_generator.py
## Project       : Microprocessor
## Tools Version : Python 2.7
##
## Description   : This Python module generates the ROM and RAM files
##
###############################################################################



#TODO maybe do pattern better, like only 2 lines, or add a new one.....
#TODO jump to already existing pixel calculation code

import sys
from generator_constants import Rom_Constants 
from generator_constants import Ram_Constants 
from generator_constants import Bus_Constants 
from ram_generator       import RAM
from rom_generator       import *


rom_constants = Rom_Constants()
ram_constants = Ram_Constants()
bus_constants = Bus_Constants()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################     GENERATE RAM
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


debug = 0

if len(sys.argv) > 1 and int(sys.argv[1]) == 1:
         debug = 1
         print 'DEBUG MODE RAM GENERATION'


ram_constants = Ram_Constants()
ram = RAM(ram_constants.MAX_SIZE, debug)


ram.add(ram_constants.ZERO_ADDR, 0, "0 constant")
ram.add(ram_constants.ONE_ADDR,  1, "1 constant")
ram.add(ram_constants.TWO_ADDR,  2, "2 constant")
ram.add(ram_constants.THREE_ADDR,3, "3 constant")
ram.add(ram_constants.FOUR_ADDR, 4, "4 constant")

#x current, max and reset and even
ram.add(ram_constants.X_CURR_ADDR, 0,   "x_curr")
ram.add(ram_constants.X_DIM_ADDR,  160, "x_max")
ram.add(ram_constants.X_RESET_ADDR,0,   "x_reset")
ram.add(ram_constants.X_LINE_ADDR, 0,   "x_line")

#y current, max and reset and even
ram.add(ram_constants.Y_CURR_ADDR, 0,   "y_curr")
ram.add(ram_constants.Y_DIM_ADDR,  120, "y_max")
ram.add(ram_constants.Y_RESET_ADDR,0,   "y_reset")
ram.add(ram_constants.Y_LINE_ADDR, 0,   "y_line")

#pixel 
ram.add(ram_constants.PIXEL_ADDR,  0,   "pixel")

#colour config FG, BG
ram.add(ram_constants.COLOUR_FG_ADDR, 0xF0, "foreground_colour")
ram.add(ram_constants.COLOUR_BG_ADDR, 0x0D, "background_colour")

#timer (1s) counter max value and value
ram.add(ram_constants.TIMER_COUNTER_CURR_ADDR, 0, "timer_counter_curr")
ram.add(ram_constants.TIMER_COUNTER_DIM_ADDR,  20,"timer_counter_max")

#mouse coordinates
ram.add(ram_constants.MOUSE_X_CURR_ADDR, 20, "current mouse pointer x")
ram.add(ram_constants.MOUSE_Y_CURR_ADDR, 20, "current mouse pointer y")


#mouse coordinates
ram.add(ram_constants.MOUSE_X_PREV_ADDR, 20, "previous mouse pointer x")
ram.add(ram_constants.MOUSE_Y_PREV_ADDR, 20, "previous mouse pointer y")

#line constants
ram.add(ram_constants.LINE_X_1_ADDR, 60,  "line 1 along x")
ram.add(ram_constants.LINE_X_2_ADDR, 110, "line 2 along x")
		
ram.add(ram_constants.LINE_Y_1_ADDR, 40,  "line 1 along y")
ram.add(ram_constants.LINE_Y_2_ADDR, 80,  "line 2 along y")




#write the RAM out
ram_file = "../Memory Initialisation/Main_RAM.txt"
if debug:
	ram_file = "./Intermediate Representations/debug_ram.txt"

ram.write(ram_file)	
	

output_file = "./Intermediate Representations/program_instructions.txt"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################     PROGRAM PARAMETERS AND FUNCTIONS (MODULAR BLOCKS)
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


##########################################################
#################     X COUNTER UPDATE 
##########################################################


def update_x():

	mod_n_counter_update(ram_constants.X_CURR_ADDR, ram_constants.X_DIM_ADDR, ram_constants.X_RESET_ADDR,  "x")

##########################################################
#################     Y COUNTER UPDATE 
##########################################################


def update_y():

	load_a(ram_constants.X_RESET_ADDR, "a = x_reset")		 
        load_b(ram_constants.ZERO_ADDR, "b = 0")               

	#save pc of the instruction that should do the jump
	branch_pc = instructions.program_counter
        branch_a_eq_b(0, "if x_reset == 0 i.e. not reset, don;t update y")          

        mod_n_counter_update(ram_constants.Y_CURR_ADDR, ram_constants.Y_DIM_ADDR, ram_constants.Y_RESET_ADDR,  "y")	 

	#make it the branch jump here
	instructions.entries[branch_pc].address =  instructions.program_counter


def get_pixel():
       	        
        load_a(ram_constants.X_CURR_ADDR, "a = x_curr") 

        load_b(ram_constants.LINE_X_1_ADDR, "b = line_x_1")  
	#save first branch pc 	
	branch_line_1_pc = instructions.program_counter
	branch_a_eq_b(0, "jump if line_x_1")


        load_b(ram_constants.LINE_X_2_ADDR, "b = line_x_2")  
	#save first branch pc 	
	branch_line_2_pc = instructions.program_counter
	branch_a_eq_b(0, "jump if line_x_2")

###################################################################################################
	        
        load_a(ram_constants.Y_CURR_ADDR, "a = x_curr") 

        load_b(ram_constants.LINE_Y_1_ADDR, "b = line_y_1")  
	#save first branch pc 	
	branch_line_3_pc = instructions.program_counter
	branch_a_eq_b(0, "jump if line_y_1")


        load_b(ram_constants.LINE_Y_2_ADDR, "b = line_y_1")  
	#save first branch pc 	
	branch_line_4_pc = instructions.program_counter
	branch_a_eq_b(0, "jump if line_y_2")


        load_a(ram_constants.ZERO_ADDR, "")
	#save branch pc
	branch_line_5_pc = instructions.program_counter
	goto(0, "")

	instructions.entries[branch_line_1_pc].address =  instructions.program_counter  
	instructions.entries[branch_line_2_pc].address =  instructions.program_counter   
	instructions.entries[branch_line_3_pc].address =  instructions.program_counter   
	instructions.entries[branch_line_4_pc].address =  instructions.program_counter     
        load_a(ram_constants.ONE_ADDR, "a = 1")
	
  	instructions.entries[branch_line_5_pc].address =  instructions.program_counter    
        store_a(ram_constants.PIXEL_ADDR, "pixel = (is_horizontal_or_vertical_line) ? 1 : 0") 

##########################################################
#################     GET X, Y, PIXEL TO VGA
##########################################################

def write_to_vga():
              

        load_a(ram_constants.X_CURR_ADDR, "a = x") 
        store_a(bus_constants.VGA_ADDR0, "0xB0 = x")  

        load_a(ram_constants.Y_CURR_ADDR, "a = y") 
        store_a(bus_constants.VGA_ADDR1, "0xB1 = y")  

        load_a(ram_constants.PIXEL_ADDR, "a = pixel")  
        store_a(bus_constants.VGA_ADDR2, "0xB2 =  pixel")  
                
                
##########################################################
#################     FULL PATTERN GENERATOR 
##########################################################


#conclude if pattern is done
def is_vga_pattern_done():

	load_a(ram_constants.Y_RESET_ADDR, "a = vga_pattern_done") 	     
        load_b(ram_constants.ONE_ADDR, "b ==1")             

	#save pc of the instruction that should do the jump
	branch_pc = instructions.program_counter
        branch_a_eq_b(0, "jump over return to base address if a == b")        
 
        goto(rom_constants.BASE_PC, "start the loop over")            

	#make it the branch jump here
	instructions.entries[branch_pc].address =  instructions.program_counter

        rom_constants.PATTERN_DONE_PC = instructions.program_counter

#function to write current (x,y) pixel value
def write_current_pixel():

	rom_constants.PIXEL_WRITE_PC = instructions.program_counter

        get_pixel()     
        write_to_vga()  

        load_a(ram_constants.Y_RESET_ADDR, "a = vga_pattern_done") 	     
        load_b(ram_constants.ZERO_ADDR, "b == 0")             

	#save pc of the instruction that should do the jump
	branch_pc = instructions.program_counter
        branch_a_eq_b(0, "return if pattern done")           

	function_return("pixel written")

	#make it the branch jump here
	instructions.entries[branch_pc].address =  instructions.program_counter


def write_vga_pattern():
       
	write_current_pixel()
	
        update_x() 
        update_y()  

	#check if pattern is done
	is_vga_pattern_done()    


##########################################################
#################      INVERT COLOURS
##########################################################

def invert_colours():


        load_a(ram_constants.COLOUR_FG_ADDR, "a = config_fg")           
        load_b(ram_constants.COLOUR_BG_ADDR, "b = config_bg")           
      
        store_b(ram_constants.COLOUR_FG_ADDR, "config_fg = b")        
        store_a(ram_constants.COLOUR_BG_ADDR, "config_bg = a")         

        store_b(bus_constants.VGA_ADDR2, "0xB0 =  b ie.  fg_new -> bg_old")                
        store_a(bus_constants.VGA_ADDR1, "0xB1 =  a i.e. bg_new -> fg_old")                

        store_a(bus_constants.LED_ADDR, "0xC0 =  bg_new")                      
        store_b(bus_constants.LED_ADDR, "0xC0 =  fg_new")                 
       
        store_b(bus_constants.SODD_SEG_ADDR_0, "0xD0 =  bg_new")                
        store_a(bus_constants.SODD_SEG_ADDR_1, "0xD1 =  fg_new")   



##########################################################
#################      DRAW THE MOUSE POINTER
##########################################################


#function returns original pattern value to pixel
def clear_prev_mouse_pointer():

	load_a(ram_constants.MOUSE_X_PREV_ADDR, "a = prev_mouse_x")
	store_a(ram_constants.X_CURR_ADDR, "x_curr = prev_mouse_x")

	load_a(ram_constants.MOUSE_Y_PREV_ADDR, "a = prev_mouse_y")
	store_a(ram_constants.Y_CURR_ADDR, "y_curr = prev_mouse_y")

	function_call(rom_constants.PIXEL_WRITE_PC, "restore old pixel value")  	


#functions records new mouse position as new previous one, A = mouse X, B = mouse Y on exiting
def record_new_mouse_position():

	load_a(ram_constants.MOUSE_X_CURR_ADDR, "a = mouse_x")
	store_a(ram_constants.MOUSE_X_PREV_ADDR, "prev_mouse_x = mouse_x")

	load_b(ram_constants.MOUSE_Y_CURR_ADDR, "b = mouse_y")
	store_b(ram_constants.MOUSE_Y_PREV_ADDR, "prev_mouse_y = mouse_y")
	

#function draws the new mouse position on screen
def draw_new_mouse_position():

	load_a(ram_constants.MOUSE_X_CURR_ADDR, "a = mouse_x")
	store_a(bus_constants.VGA_ADDR0, "0xB0 = x") 
	alu_incr_a('A', "a = mouse_x + 1")
	
	load_b(ram_constants.X_DIM_ADDR, "b = x_dim")
	alu_a_mod_b('A', "a = mouse_x + 1 % x_dim")
	store_a(ram_constants.MOUSE_X_CURR_ADDR, "mouse_x = mouse_x + 1 % x_dim")
	

	load_a(ram_constants.MOUSE_Y_CURR_ADDR, "a = mouse_y")
        store_a(bus_constants.VGA_ADDR1, "0xB1 = y")
	alu_incr_a('A', "a = mouse_y + 1")

	load_b(ram_constants.Y_DIM_ADDR, "b = y_dim")
	alu_a_mod_b('A', "a = mouse_x + 1 % x_dim")
	store_a(ram_constants.MOUSE_Y_CURR_ADDR, "mouse_y = mouse_y + 1")




        load_a(ram_constants.ONE_ADDR, "pixel = 1")  
        store_a(bus_constants.VGA_ADDR2, "0xB2 =  pixel")  


#function fully redraws pattern
def redraw_pattern():
	
	load_a(ram_constants.ZERO_ADDR, "a = 0")
	store_a(ram_constants.Y_RESET_ADDR, "y_reset = 0")
	goto(rom_constants.BASE_PC, "return to start of pattern drawer")	

	

#function executes all the mouse pointer drawing exectuion
def update_mouse_pointer():


	clear_prev_mouse_pointer()
	record_new_mouse_position()
	draw_new_mouse_position()


##########################################################
#################      TIMER INTERRUPT HANDLER
##########################################################

#timer interrupt handler
def handle_timer_interrupt():

        rom_constants.INCREMENT_COUNTER_PC = instructions.program_counter
	mod_n_counter_update(ram_constants.TIMER_COUNTER_CURR_ADDR, ram_constants.TIMER_COUNTER_DIM_ADDR, ram_constants.TIMER_COUNTER_RESET_ADDR,  "timer_counter")

	load_a(ram_constants.TIMER_COUNTER_RESET_ADDR, "a = timer_counter_reset")
        load_b(ram_constants.ZERO_ADDR, "b = 0")                

	#save pc of the instruction that should do the jump
	branch_pc = instructions.program_counter
        branch_a_eq_b(0, "skip inversion unless reset the counter to 0")     

	#inverse colours
	invert_colours()      

	#make it the branch jump here
	instructions.entries[branch_pc].address =  instructions.program_counter

	#draw the mouse
	update_mouse_pointer()



##########################################################
#################      INTERRUPT ADDRESS INSERTER
##########################################################
                

def insert_interrupt_addresses():

	instructions.insert_value(rom_constants.TIMER_INTERRUPT_LOC, rom_constants.INCREMENT_COUNTER_PC, "Timer interrupt address")
	instructions.insert_value(rom_constants.MOUSE_INTERRUPT_LOC, rom_constants.INCREMENT_COUNTER_PC, "Mouse interrup address")
        

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################     GENERATE INSTRUCTIONS
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

write_vga_pattern()
goto_idle("Wait for interrupts")
handle_timer_interrupt()
goto_idle("Wait for interrupts")




insert_interrupt_addresses()
instructions.write(output_file)

#testing instruction set

#address = 0
#register = 'B'
#load_a(0)
#load_b(0)
#store_a(0)
#store_b(0)
#alu_a_add_b(register)
#alu_a_sub_b(register)
#alu_a_mult_b(register)
#alu_shift_a_l(register)
#alu_shift_a_r(register)
#alu_incr_a(register)
#alu_incr_b(register)
#alu_decr_a(register)
#alu_decr_b(register)
#alu_a_eq_b(register)
#alu_a_greater_b(register)
#alu_a_lesser_b(register)
#alu_a_mod_b(register)
#alu_a_and_b(register)
#alu_not_a(register)
#alu_a_or_b(register)
#branch_a_eq_b(address)
#branch_a_greater_b(address)
#branch_a_lesser_b(address)
#goto(address)
#goto_idle()
#function_call(address)
#function_return()
#deref_a()
#deref_b()
#nop()














                
