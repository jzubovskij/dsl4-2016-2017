#!/usr/bin/python

###############################################################################
##
## Author        : Jevgenij Zubovskij
## Company       : University of Edinburgh
##
## Module Name   : program_generator.py
## Project       : Microprocessor
## Tools Version : Python 2.7
##
## Description   : This Python module contains all the constants
##
###############################################################################



##########################################################
#################     RAM CONSTANTS
##########################################################

class Ram_Constants:

	def __init__(self):

		self.MAX_SIZE    = 128 #max size of RAM
		self.UNUSED_STRING = "UNUSED" #contant to signify unasssigned entries

		##########################################################

		self.ZERO_ADDR   = 0x00 # should hold a 0
		self.ONE_ADDR    = 0x01 # should hold a 1
		self.TWO_ADDR    = 0x02 # should hold a 2
		self.THREE_ADDR  = 0x03 # should hold a 3
		self.FOUR_ADDR   = 0x04 # should hold a 4

		##########################################################


		self.X_CURR_ADDR  = 0x05 # current X counter value
		self.X_DIM_ADDR   = 0x06 # max X value (160)
		self.X_RESET_ADDR = 0x07 # was X reset to 0
		self.X_ODD_ADDR   = 0x08 # is X % 2 == 0

		##########################################################

		self.Y_CURR_ADDR  = 0x09 # current Y counter value
		self.Y_DIM_ADDR   = 0x0A # max Y value (120)
		self.Y_RESET_ADDR = 0x0B # was Y reset to 0
		self.Y_ODD_ADDR   = 0x0C # is Y % 2 == 0

		##########################################################
		 
		self.PIXEL_ADDR        = 0x0D # holds current pixel value
		self.COLOUR_FG_ADDR    = 0x0E # holds current BG colour
		self.COLOUR_BG_ADDR    = 0x0F # holds current BG colour

		##########################################################

		self.TIMER_COUNTER_CURR_ADDR = 0x10 # current timer counter value
		self.TIMER_COUNTER_DIM_ADDR  = 0x11 # max timer counter value

		##########################################################



##########################################################
#################     ROM ADDRESSES 
##########################################################

class Rom_Constants:

	def __init__(self):


		#maximum rom size
		self.MAX_SIZE = 256



		self.BASE_PC              = 0x00
		self.PATTERN_DONE_PC      = 0x00
		self.INCREMENT_COUNTER_PC = 0x00



		#Memory
        	self.LOAD_STRING = 'LOAD' 
		self.LOAD_WIDTH  = 2
		

		self.STORE_STRING = 'STORE'
		self.STORE_WIDTH  = 2



		#ALU
		self.ALU_STRING = 'ALU'
		self.ALU_WIDTH = 1

		
		self.ALU_A_ADD_B_STRING = 'A_ADD_B'
		self.ALU_A_SUB_B_STRING = 'A_SUB_B'
		self.ALU_A_MULT_B_STRING = 'A_MULT_B'
		self.ALU_A_MOD_B_STRING = 'A_MOD_B'
		self.ALU_SHIFT_A_L_STRING = 'SHIFT_A_L'
		self.ALU_SHIFT_A_R_STRING = 'SHIFT_A_R'
		self.ALU_INCR_A_STRING = 'INCR_A'
		self.ALU_INCR_B_STRING = 'INCR_B'
		self.ALU_DECR_A_STRING = 'DECR_A'
		self.ALU_DECR_B_STRING = 'DECR_B'
		self.ALU_A_EQ_B_STRING = 'A_EQ_B'
		self.ALU_A_GREATER_B_STRING = 'A_GREATER_B'
		self.ALU_A_LESSER_B_STRING  = 'A_LESSER_B'
		self.ALU_NOT_A_STRING  = 'NOT_A'
		self.ALU_A_OR_B_STRING = 'A_OR_B'
		self.ALU_A_AND_B_STRING = 'A_AND_B'



		#Branching
		self.BRANCH_STRING = 'BRANCH'
		self.BRANCH_WIDTH  = 2

		self.GOTO_STRING = 'GOTO'
		self.GOTO_WIDTH = 2

		self.GOTO_IDLE_STRING = 'GOTO_IDLE'
		self.GOTO_IDLE_WIDTH = 1


		#Functions
		self.FUNCTION_CALL_STRING = 'FUNCTION_CALL'
		self.FUNCTION_CALL_WIDTH = 2

		self.RETURN_STRING = 'RETURN'
		self.RETURN_WIDTH = 1
		


		#Dereference
		
		self.DEREF_STRING = 'DEREF'
		self.DEREF_WIDTH = 1

		self.DEREF_A_INTO_A_STRING = 'A_INTO_A'
		self.DEREF_B_INTO_B_STRING = 'B_INTO_B'
		

		#NOP

		self.NOP_STRING = 'NOP'
		self.NOP_WIDTH = 1

		#Literals
		self.LITERAL_STRING = "LITERAL"
		self.LITERAL_WIDTH = 1

		self.TIMER_INTERRUPT_LOC = 254
		self.MOUSE_INTERRUPT_LOC = 255


		#Non-printable
		self.TRASH_STRING = "TRASH"





##########################################################
#################     BUS ADDRESSES 
##########################################################

class Bus_Constants:
	def __init__(self):



		self.VGA_ADDR0  	 = 0xB0
		self.VGA_ADDR1  	 = 0xB1
		self.VGA_ADDR2  	 = 0xB2

		##########################################################

		self.LED_ADDR   	 = 0xC0

		##########################################################

		self.SODD_SEG_ADDR_0 = 0xD0
		self.SODD_SEG_ADDR_1 = 0xD1

		##########################################################

		self.TIMER_ADDR_0 	 = 0xF0
		self.TIMER_ADDR_1 	 = 0xF1
		self.TIMER_ADDR_2 	 = 0xF2
		self.TIMER_ADDR_3 	 = 0xF3


		##########################################################








                
