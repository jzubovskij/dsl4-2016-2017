#!/usr/bin/python

###############################################################################
##
## Author        : Jevgenij Zubovskij
## Company       : University of Edinburgh
##
## Module Name   : assembler.py
## Project       : Microprocessor
## Tools Version : Python 2.7
##
## Description   : This Python module reads instructions from a file. It then
## said assembly instructions into their binary representations for the
## processor to use for execution. It also stores the interrupt addresses in
## the ROM. It also has a debug mode that allows creation of the above
## binary representations but in a copy-pasteable way directly into the
## ROM verilog file. Hence, allowing running the programs in sumulations
## as well.
##
###############################################################################


import math
import sys

from generator_constants import Rom_Constants


print "BEGINNING ASSEMBLY"


rom_constants = Rom_Constants()

debug = 0

output_file = open( "../Memory Initialisation/Instruction_ROM.txt", 'w')


#if no input argument
if len(sys.argv) > 1 and int(sys.argv[1]) == 1:
         debug = 1
         output_file = open("./Intermediate Representations/debug_rom.txt", 'w')
         print 'DEBUG MODE ROM GENERATION'



input_file = open('./Intermediate Representations/program_instructions.txt', 'r')


line_number = 0
lines = [line.rstrip('\n') for line in input_file]

for line in lines:
        tokens = line.split()

        #skip empty lines
        if len(tokens) < 1:
                continue

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################     LOAD INSTRUCTION (REG <- MEM)
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        if tokens[0] == rom_constants.LOAD_STRING:


                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1

                #print the LOAD PART AND TARGET REGISTER
                if   tokens[1] == 'A':
                        output_file.write(str(format(0, '08b')))
                elif tokens[1] == 'B':
                        output_file.write(str(format(1, '08b')))

                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")



                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1

                #print SOURCE ADDRESS
                address = int(tokens[2], 0)
                output_file.write(str(format(address, '08b')))

                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################     STORE INSTRUCTION (REG -> MEM)
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        elif tokens[0] == rom_constants.STORE_STRING:

                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1

                #print the LOAD PART AND TARGET REGISTER
                if   tokens[1] == 'A':
                        output_file.write(str(format(2, '08b')))
                elif tokens[1] == 'B':
                        output_file.write(str(format(3, '08b')))

                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")


                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1

                #print TARGET ADDRESS
                address = int(tokens[2], 0)
                output_file.write(str(format(address, '08b')))

                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################     ALU INSTRUCTION (REG <- ALU)
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        elif tokens[0] == rom_constants.ALU_STRING:

                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1

                #print ALU OPERATION

                #ARITHMETICS
                if   tokens[2] == rom_constants.ALU_A_ADD_B_STRING:
                        output_file.write(str(format(0, '04b')))
                elif tokens[2] == rom_constants.ALU_A_SUB_B_STRING:
                        output_file.write(str(format(1, '04b')))
                elif tokens[2] == rom_constants.ALU_A_MULT_B_STRING:
                        output_file.write(str(format(2, '04b')))
                elif tokens[2] == rom_constants.ALU_SHIFT_A_L_STRING:
                        output_file.write(str(format(3, '04b')))
                elif tokens[2] == rom_constants.ALU_SHIFT_A_R_STRING:
                        output_file.write(str(format(4, '04b')))



                #INCREMENTS
                elif tokens[2] == rom_constants.ALU_INCR_A_STRING:
                        output_file.write(str(format(5, '04b')))
                elif tokens[2] == rom_constants.ALU_INCR_B_STRING:
                        output_file.write(str(format(6, '04b')))
                elif tokens[2] == rom_constants.ALU_DECR_A_STRING:
                        output_file.write(str(format(7, '04b')))
                elif tokens[2] == rom_constants.ALU_DECR_B_STRING:
                        output_file.write(str(format(8, '04b')))

                #LOGIC
                elif tokens[2] == rom_constants.ALU_A_EQ_B_STRING:
                        output_file.write(str(format(9, '04b')))
                elif tokens[2] == rom_constants.ALU_A_GREATER_B_STRING:
                        output_file.write(str(format(10, '04b')))
                elif tokens[2] == rom_constants.ALU_A_LESSER_B_STRING:
                        output_file.write(str(format(11, '04b')))
                elif tokens[2] == rom_constants.ALU_A_MOD_B_STRING:
                        output_file.write(str(format(12, '04b')))
                elif tokens[2] == rom_constants.ALU_A_AND_B_STRING:
                        output_file.write(str(format(13, '04b')))
                elif tokens[2] == rom_constants.ALU_NOT_A_STRING:
                        output_file.write(str(format(14, '04b')))
                elif tokens[2] == rom_constants.ALU_A_OR_B_STRING:
                        output_file.write(str(format(15, '04b')))


                #print OPCODE
                if   tokens[1] == 'A':
                        output_file.write(str(format(4, '04b')))
                elif tokens[1] == 'B':
                        output_file.write(str(format(5, '04b')))

                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#############   CONDITIONAL BRANCHING INSTRUCTION (IF(CONDITION) PC <- PC_NEW)
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        elif tokens[0] == rom_constants.BRANCH_STRING:

                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1

                if   tokens[1] == rom_constants.ALU_A_EQ_B_STRING:
                        #print the A_EQ_B OPCODE
                        output_file.write(str(format(9, '04b')))

                elif tokens[1] == rom_constants.ALU_A_GREATER_B_STRING:
                        #print the A_GREATER_B OPCODE
                        output_file.write(str(format(10, '04b')))

                elif tokens[1] == rom_constants.ALU_A_LESSER_B_STRING:
                        #print the A_GREATER_B OPCODE
                        output_file.write(str(format(11, '04b')))

                #print BRANCH OPCODE
                output_file.write(str(format(6, '04b')))

                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")


                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1


                #print the BRANCH ADDRESS
                address = int(tokens[2], 0)
                output_file.write(str(format(address, '08b')))


                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################    GOTO INSTRUCTION (PC <- PC_NEW)
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        elif tokens[0] == rom_constants.GOTO_STRING:

                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1

                #print GOTO OPCODE
                output_file.write(str(format(7, '08b')))

                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")

                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1


                #print the GOTO ADDRESS
                address = int(tokens[1], 0)
                output_file.write(str(format(address, '08b')))

                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################    GOTO IDLE INSTRUCTION (STATE -> IDLE)
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        elif tokens[0] == rom_constants.GOTO_IDLE_STRING:

                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1

                #print GOTO_IDLE OPCODE
                output_file.write(str(format(8, '08b')))

                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################    FUNCTION CALL INSTRUCTION (PC_SAVED <- PC , PC-> PC_NEW)
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        elif tokens[0] == rom_constants.FUNCTION_CALL_STRING:

                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1

                #print FUNCTION_CALL OPCODE
                output_file.write(str(format(9, '08b')))



                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")

                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1


                #print the FUNCTION ADDRESS
                address = int(tokens[1], 0)
                output_file.write(str(format(address, '08b')))

                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################    RETURN INSTRUCTION (PC <- PC_SAVED)
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        elif tokens[0] == rom_constants.RETURN_STRING:

                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1

                #print RETURN OPCODE
                output_file.write(str(format(10, '08b')))

                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################    DEREFERENCE INSTRUCTION (REG <- RAM[REG])
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        elif tokens[0] == rom_constants.DEREF_STRING:

                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1



                if tokens[1] == rom_constants.DEREF_A_INTO_A_STRING:
                    #print OPCODE
                    output_file.write(str(format(11, '08b')))
                elif tokens[1] == rom_constants.DEREF_B_INTO_B_STRING:
                    output_file.write(str(format(12, '08b')))

                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################    NO-OP INSTRUCTION (PC <- PC + 1)
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        elif tokens[0] == rom_constants.NOP_STRING:

                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1

                #print NOP OPCODE
                output_file.write(str(format(0xff, '08b')))

                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
################################################################################
#################    INTERRUPT ADDRESSES
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        #otherwise interrupt addresses, siply re-print
        else:           

                #if direct input into ROM "manually"
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))
                line_number += 1

                output_file.write(tokens[0])

                #if direct input into ROM "manually"
                if debug:
                        output_file.write(';')
                output_file.write("\n")

        #check that still have space in the ROM
        if line_number > rom_constants.MAX_SIZE:
                print "ROM FULL, STOPPING ASSEMBLER"
                break


#close the files
input_file.close()
output_file.close()

print "ASSEMBLY COMPLETED"
