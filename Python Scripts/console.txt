

-------------------------------------------------------------------
------------Processor RAM Overview---------------------------------
-------------------------------------------------------------------
RAM : Entries Assigned     : 40
RAM : Max Address Used     : 91
RAM : Mamixum Size         : 128
RAM : Remaining Space      : 88
-------------------------------------------------------------------
------------Processor RAM Overview End-----------------------------
-------------------------------------------------------------------




-------------------------------------------------------------------
------------Processor RAM Contents---------------------------------
-------------------------------------------------------------------
 |ADDR||Value|     |Comment|                     
-------------------------------------------------------------------
@:  5 : 0x00 (  0) //x_curr
@:  6 : 0xa0 (160) //x_max
@:  7 : 0x00 (  0) //x_reset
@:  8 : 0x00 (  0) //x_line
@:  9 : 0x00 (  0) //y_curr
@: 10 : 0x78 (120) //y_max
@: 11 : 0x00 (  0) //y_reset
@: 12 : 0x00 (  0) //y_line
@: 13 : 0x00 (  0) //pixel
@: 14 : 0xf0 (240) //foreground_colour
@: 15 : 0x0d ( 13) //background_colour
@: 16 : 0x00 (  0) //timer_counter_curr
@: 17 : 0x14 ( 20) //timer_counter_max
@: 18 : 0x00 (  0) //timer_counter_reset
@: 21 : 0x14 ( 20) //previous mouse pointer x
@: 22 : 0x14 ( 20) //previous mouse pointer y
@: 23 : 0x32 ( 50) //line 1 along x
@: 24 : 0x6e (110) //line 2 along x
@: 25 : 0x28 ( 40) //line 1 along y
@: 26 : 0x50 ( 80) //line 2 along y
@: 27 : 0x00 (  0) //the current region of the screen (and IR command)
@: 28 : 0x00 (  0) //the previous region of the screen (and IR command)
@: 29 : 0x00 (  0) //car selected number
@: 30 : 0x00 (  0) //mouse_prev_addr
@: 48 : 0x1c ( 28) //green colour rgb
@: 49 : 0xe0 (224) //red colour rgb
@: 50 : 0x03 (  3) //blue colour rgb
@: 51 : 0xfc (252) //yellow colour rgb
@: 80 : 0x00 (  0) //0 constant
@: 81 : 0x01 (  1) //1 constant
@: 82 : 0x02 (  2) //2 constant
@: 83 : 0x03 (  3) //3 constant
@: 84 : 0x04 (  4) //4 constant
@: 85 : 0x05 (  5) //5 constant
@: 86 : 0x06 (  6) //6 constant
@: 87 : 0x07 (  7) //7 constant
@: 88 : 0x08 (  8) //8 constant
@: 89 : 0x09 (  9) //9 constant
@: 90 : 0x0a ( 10) //10 constant
@: 91 : 0xff (255) //255 constant
-------------------------------------------------------------------
------------Processor RAM Contents End-----------------------------
-------------------------------------------------------------------




-------------------------------------------------------------------
------------Processor ROM Overview---------------------------------
-------------------------------------------------------------------
ROM : Program Counter      : 253
ROM : Instruction Count    : 133
ROM : Entry Count          : 255
ROM : Maximum Size         : 256
RAM : Remaining Space      : 1
-------------------------------------------------------------------
------------Processor ROM Overview End-----------------------------
-------------------------------------------------------------------




-------------------------------------------------------------------
------------Processor ROM Contents---------------------------------
-------------------------------------------------------------------
  |PC||Instruction|                       |Comment|
-------------------------------------------------------------------
@:  0 : GOTO                0x4e    ( 78) //write pattern
BEGIN : Write Current Pixel
BEGIN : Get Pixel
@:  2 : LOAD A              0x05    (  5) //a = x_curr
@:  4 : LOAD B              0x17    ( 23) //b = line_x_1
@:  6 : BRANCH A_EQ_B       0x1a    ( 26) //jump if line_x_1
@:  8 : LOAD B              0x18    ( 24) //b = line_x_2
@: 10 : BRANCH A_EQ_B       0x1a    ( 26) //jump if line_x_2
@: 12 : LOAD A              0x09    (  9) //a = x_curr
@: 14 : LOAD B              0x19    ( 25) //b = line_y_1
@: 16 : BRANCH A_EQ_B       0x1a    ( 26) //jump if line_y_1
@: 18 : LOAD B              0x1a    ( 26) //b = line_y_1
@: 20 : BRANCH A_EQ_B       0x1a    ( 26) //jump if line_y_2
@: 22 : LOAD A              0x50    ( 80) //a = 0
@: 24 : GOTO                0x1c    ( 28) //pixel = a, jump to assign
@: 26 : LOAD A              0x51    ( 81) //a = 1
@: 28 : STORE A             0x0d    ( 13) //pixel = (is_horizontal_or_vertical_line) ? 1 : 0
END : Get Pixel
BEGIN : Write to VGA
@: 30 : LOAD A              0x05    (  5) //a = x
@: 32 : STORE A             0xb0    (176) //0xB0 = x
@: 34 : LOAD A              0x09    (  9) //a = y
@: 36 : STORE A             0xb1    (177) //0xB1 = y
@: 38 : LOAD A              0x0d    ( 13) //a = pixel
@: 40 : STORE A             0xb2    (178) //0xB2 =  pixel
END : Write to VGA
@: 42 : RETURN                      (N/A) //pixel written
END : Write Current Pixel
BEGIN : Write Colours
@: 43 : LOAD A              0x1d    ( 29) //a = car_selected
@: 45 : LOAD B              0x50    ( 80) //b = 0
@: 47 : BRANCH A_GREATER_B  0x35    ( 53) //if car > 0, branch away
@: 49 : LOAD A              0x30    ( 48) //a = green
@: 51 : GOTO                0x47    ( 71) //car = 0
@: 53 : LOAD B              0x51    ( 81) //b = 1
@: 55 : BRANCH A_GREATER_B  0x3d    ( 61) //if car > 1, branch away
@: 57 : LOAD A              0x32    ( 50) //a = blue
@: 59 : GOTO                0x47    ( 71) //car = 1
@: 61 : LOAD B              0x52    ( 82) //b = 2
@: 63 : BRANCH A_GREATER_B  0x45    ( 69) //if car > 2, branch away
@: 65 : LOAD A              0x31    ( 49) //a = red
@: 67 : GOTO                0x47    ( 71) //car = 2
@: 69 : LOAD A              0x33    ( 51) //toherwise, a = yellow
@: 71 : LOAD B              0x0f    ( 15) //b = config_bg
@: 73 : STORE B             0xb2    (178) //0xB0 =  b ie.  fg_new -> config_fg
@: 75 : STORE A             0xb1    (177) //0xB1 =  a i.e. bg_new -> config_bg
@: 77 : RETURN                      (N/A) //colours updated
END : Write Colours
@: 78 : FUNCTION_CALL       0x2b    ( 43) //write current pixel value
BEGIN : Write VGA Pattern
@: 80 : FUNCTION_CALL       0x02    (  2) //write current pixel value
BEGIN : Generic Counter for x
@: 82 : LOAD A              0x05    (  5) //a = x_curr
@: 84 : LOAD B              0x06    (  6) //b = x_max
@: 86 : ALU A INCR_A                (N/A) //a = x_curr + 1
@: 87 : ALU A A_MOD_B               (N/A) //a = (x_curr + 1) % x_dim
@: 88 : STORE A             0x05    (  5) //x_curr = x_curr + 1) % x_dim
@: 90 : LOAD B              0x50    ( 80) //b = 0
@: 92 : ALU B A_EQ_B                (N/A) //b = x_reset
@: 93 : STORE B             0x07    (  7) //x_reset = (x_curr + 1) % x_max == 0
END : Generic Counter for x
@: 95 : LOAD A              0x07    (  7) //a = x_reset
@: 97 : LOAD B              0x50    ( 80) //b = 0
@: 99 : BRANCH A_EQ_B       0x72    (114) //if x_reset == 0 i.e. not reset, don't update y
BEGIN : Generic Counter for y
@:101 : LOAD A              0x09    (  9) //a = y_curr
@:103 : LOAD B              0x0a    ( 10) //b = y_max
@:105 : ALU A INCR_A                (N/A) //a = y_curr + 1
@:106 : ALU A A_MOD_B               (N/A) //a = (y_curr + 1) % y_dim
@:107 : STORE A             0x09    (  9) //y_curr = y_curr + 1) % y_dim
@:109 : LOAD B              0x50    ( 80) //b = 0
@:111 : ALU B A_EQ_B                (N/A) //b = y_reset
@:112 : STORE B             0x0b    ( 11) //y_reset = (y_curr + 1) % y_max == 0
END : Generic Counter for y
BEGIN : Is VGA PAttern Done
@:114 : LOAD A              0x0b    ( 11) //a = vga_pattern_done
@:116 : LOAD B              0x51    ( 81) //b ==1
@:118 : BRANCH A_EQ_B       0x7a    (122) //jump over return to base address if a == b
@:120 : GOTO                0x50    ( 80) //start the vga pattern loop over
END : Is VGA PAttern Done
END : Write VGA Pattern
@:122 : GOTO_IDLE                   (N/A) //Wait for interrupts
BEGIN : Handle Timer Interrupt
BEGIN : Get Mouse Action
@:123 : LOAD A              0xa0    (160) //a = mouse_status
@:125 : LOAD B              0x1e    ( 30) //b = mouse_status_prev
@:127 : BRANCH A_EQ_B       0x85    (133) //if same status, jump away
@:129 : STORE A             0x1e    ( 30) //mouse_status_prev = mouse_status
@:131 : STORE A             0x91    (145) //ir[1]  = a
END : Get Mouse Action
BEGIN : Write To IR
@:133 : LOAD A              0xa3    (163) //a = mouse_scroll_value
@:135 : STORE A             0x1d    ( 29) //car_selected = a
@:137 : STORE A             0x92    (146) //ir_car_selected = car_select
@:139 : LOAD A              0x1b    ( 27) //a = region_selected
@:141 : STORE A             0x90    (144) //ir_command = region_select
@:143 : LOAD B              0x1c    ( 28) //b = region_prev
@:145 : BRANCH A_EQ_B       0x97    (151) //do not reset if same region
@:147 : STORE A             0xf2    (242) //reset timer
@:149 : STORE A             0x1c    ( 28) //region_prev = region_curr
BEGIN : Visualize Timer Value
@:151 : LOAD A              0xf0    (240) //a = timer_curr (in seconds)
@:153 : STORE A             0xd0    (208) //seven_seg[0] = a
@:155 : LOAD B              0x50    ( 80) //b = 0
@:157 : BRANCH A_GREATER_B  0xa7    (167) //branch away if timer is > 0
@:159 : LOAD A              0x5b    ( 91) //a = 255
@:161 : STORE A             0xc0    (192) //led[0] = 255
@:163 : STORE A             0xc1    (193) //led[1] = 255
@:165 : GOTO                0xab    (171) //flash leds done
@:167 : STORE B             0xc0    (192) //led[0] = 0
@:169 : STORE B             0xc1    (193) //led[1] = 0
END : Visualize Timer Value
END : Write To IR
@:171 : FUNCTION_CALL       0x2b    ( 43) //rewrite_colours
END : Handle Timer Interrupt
@:173 : GOTO_IDLE                   (N/A) //Wait for interrupts
BEGIN : Handle Mouse Interrupt
BEGIN : Update Mouse Pointer
BEGIN : Clear Previous Mouse Pointer
@:174 : LOAD A              0x15    ( 21) //a = prev_mouse_x
@:176 : STORE A             0x05    (  5) //x_curr = prev_mouse_x
@:178 : LOAD A              0x16    ( 22) //a = prev_mouse_y
@:180 : STORE A             0x09    (  9) //y_curr = prev_mouse_y
@:182 : FUNCTION_CALL       0x02    (  2) //restore old pixel value
END : Clear Previous Mouse Pointer
BEGIN : Draw New Mouse Pointer
@:184 : LOAD A              0xa1    (161) //a = mouse_x
@:186 : STORE A             0x15    ( 21) //prev_mouse_x = mouse_x
@:188 : STORE A             0xb0    (176) //0xB0 = x
@:190 : LOAD A              0xa2    (162) //b = mouse_y
@:192 : STORE A             0x16    ( 22) //prev_mouse_y = mouse_y
@:194 : STORE A             0xb1    (177) //0xB1 = y
@:196 : LOAD A              0x51    ( 81) //pixel = 1
@:198 : STORE A             0xb2    (178) //0xB2 =  pixel
END : Draw New Mouse Pointer
END : Update Mouse Pointer
BEGIN : Determine Screen Region
BEGIN : Determine Region X
@:200 : LOAD A              0xa1    (161) //a = mouse_x
@:202 : LOAD B              0x54    ( 84) //b = 4
@:204 : STORE B             0x1b    ( 27) //region = b
@:206 : LOAD B              0x17    ( 23) //b = line_x_1
@:208 : BRANCH A_LESSER_B   0xde    (222) //do not continue of region found
@:210 : LOAD B              0x58    ( 88) //b = 8
@:212 : STORE B             0x1b    ( 27) //region = b
@:214 : LOAD B              0x18    ( 24) //b = line_x_2
@:216 : BRANCH A_GREATER_B  0xde    (222) //do not continue of region found
@:218 : LOAD B              0x50    ( 80) //b = 0
@:220 : STORE B             0x1b    ( 27) //region = b
END : Determine Region X
BEGIN : Determine Region Y
@:222 : LOAD A              0xa2    (162) //a = mouse_y
@:224 : LOAD B              0x19    ( 25) //b = line_y_1
@:226 : BRANCH A_LESSER_B   0xea    (234) //if mouse_y_curr < line_y_1
@:228 : LOAD B              0x1a    ( 26) //b = line_y_2
@:230 : BRANCH A_GREATER_B  0xf1    (241) //if mouse_y_curr > line_y_2
@:232 : GOTO                0xf8    (248) //region detected
@:234 : LOAD A              0x1b    ( 27) //a = region
@:236 : LOAD B              0x51    ( 81) //b = 1
@:238 : ALU A A_ADD_B               (N/A) //a = region + 1
@:239 : GOTO                0xf6    (246) //region detected
@:241 : LOAD A              0x1b    ( 27) //a = region
@:243 : LOAD B              0x52    ( 82) //b = 2
@:245 : ALU A A_ADD_B               (N/A) //a = region + 2
@:246 : STORE A             0x1b    ( 27) //region = a
END : Determine Region Y
END : Determine Screen Region
BEGIN : Update Mouse Speeds
@:248 : LOAD A              0xe0    (224) //a = switches[7:0]
@:250 : STORE A             0xa4    (164) //mouse_speeds = a
END : Update Mouse Speeds
END : Handle Mouse Interrupt
@:252 : GOTO_IDLE                   (N/A) //Wait for interrupts
@:254 : 01111011                    (123) //Timer interrupt address
@:255 : 10101110                    (174) //Mouse interrup address
-------------------------------------------------------------------
------------Processor ROM Contents End-----------------------------
-------------------------------------------------------------------


BEGINNING ASSEMBLY
ASSEMBLY COMPLETED
