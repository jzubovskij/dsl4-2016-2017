`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
// 
// Create Date: 23.02.2017 16:42:19
// Design Name: Microprocessor
// Module Name: Microprocessor_Top_Tb
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: This module is used to test the behavior of the 
// Microprocessor_Top module
// 
// Dependencies: Microprocessor_Top
// 
// Revision 1.0
// 
//////////////////////////////////////////////////////////////////////////////////

module Microprocessor_Top_Tb (
    );
    
    reg CLK;
    reg RESET;
    wire VGA_HS;
    wire VGA_VS;
    wire [3:0] VGA_RED;
    wire [3:0] VGA_GREEN;
    wire [3:0] VGA_BLUE;
    wire [15:0] LED;
    
    
    Microprocessor_Top microprocessor(
    
    
        .CLK,
        .RESET,
       
        .VGA_HS,
        .VGA_VS, 
        .VGA_RED,
        .VGA_GREEN,
        .VGA_BLUE,
        .LED
    
    );
    
    initial begin
        CLK = 0;
        RESET = 0;
        forever #5 CLK = ~CLK;
    end
    
endmodule

