`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
//
// Design Name: Microprocessor
// Module Name: Processor
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: The Processor executing instructions from the ROM. It reads the
// instruction from ROM[PC] to get the action one wants to perfrom. it also reads
// ROM[PC+1] to get the 2nd byte of the instruction, which can be used or 
// not, based on the instruction. 
//
// Dependencies: ALU
// Revision: 1.0
//////////////////////////////////////////////////////////////////////////////////

module Processor(
	//Standard Signals
    input           CLK,
    input           RESET,
    // INTERRUPT signals
    input   [1:0]   BUS_INTERRUPTS_RAISE,
    output  [1:0]   BUS_INTERRUPTS_ACK,
    //BUS Signals
    output 	[7:0]   BUS_ADDR,
    inout   [7:0]   BUS_DATA,
    output BUS_WE,
    // ROM signals
    output  [7:0]   ROM_ADDRESS,
    input   [7:0]   ROM_DATA

);

    //The main data bus is treated as tristate, so we need a mechanism to handle this.
    //Tristate signals that interface with the main state machine
    wire [7:0]  BusDataIn;
    reg  [7:0]  CurrBusDataOut, NextBusDataOut;
    reg         CurrBusDataOutWE, NextBusDataOutWE;
    
    //Tristate Mechanism
    assign BusDataIn    = BUS_DATA;
    assign BUS_DATA     = CurrBusDataOutWE ? CurrBusDataOut : 8'hZZ;
    assign BUS_WE       = CurrBusDataOutWE;
    
    //Address of the bus
    reg [7:0] NextBusAddr,CurrBusAddr = 2'h00;
    assign    BUS_ADDR = CurrBusAddr;
    
    //The processor has two internal registers to hold data between operations, and a third to hold
    //the current program context when using function calls.
    reg [7:0]   NextRegA,CurrRegA  = 2'h00;
    reg [7:0]   NextRegB, CurrRegB = 2'h00;
    reg         NextRegSelect, CurrRegSelect = 1'b0; 
    reg [7:0]   NextProgContext, CurrProgContext = 2'h00;
    
    //Dedicated Interrupt output lines - one for each interrupt line
    reg [1:0] CurrInterruptAck, NextInterruptAck;
    assign BUS_INTERRUPTS_ACK = CurrInterruptAck;
    
    //Instantiate program memory here
    //There is a program counter which points to the current operation.  The program counter
    //has an offset that is used to reference information that is part of the current operation
    reg     [7:0] NextProgCounter, CurrProgCounter = 2'h00;
    reg     NextProgCounterOffset, CurrProgCounterOffset = 2'b00;
    wire    [7:0] ProgMemoryOut;
    wire    [7:0] ActualAddress;
    assign ActualAddress = CurrProgCounter + CurrProgCounterOffset;
    // ROM signals
    assign ROM_ADDRESS 	    = ActualAddress;
    assign ProgMemoryOut    = ROM_DATA;
    
    //Instantiate the ALU
    //The processor has an integrated ALU that can do several different operations
    wire [7:0] AluOut;
    ALU ALU0(
        //standard signals
        .CLK(CLK),
        .RESET(RESET),
        //I/O
        .IN_A(CurrRegA),
        .IN_B(CurrRegB),
        .ALU_Op_Code(ProgMemoryOut[7:4]),
        .OUT_RESULT(AluOut)
    );

    //The microprocessor is essentially a state machine, with one sequential pipeline
    //of states for each operation.
    //The current li st of operations is:
    // 0: Read from memory to A
    // 1: Read from memory to B
    // 2: Write to memory from A
    // 3: Write to memory from B
    // 4: Do maths with the ALU, and save result in reg A
    // 5: Do maths with the ALU, and save result in reg B
    // 6: if A (== or < or > B) GoTo ADDR
    // 7: Goto ADDR
    // 8: Go to IDLE
    // 9: End thread, goto idle state and wait for interrupt.
    // 10: Function call
    // 11: Return from function call
    // 12: Dereference A
    // 13: Dereference B
    // 15: NOP

    parameter [7:0] //Program thread selection
        IDLE                    = 8'hF0, //Waits here until an interrupt wakes up the processor.
        GET_THREAD_START_ADDR_0 = 8'hF1, //Wait.
        GET_THREAD_START_ADDR_1	= 8'hF2, //Apply the new address to the program counter.
        GET_THREAD_START_ADDR_2 = 8'hF3, //Wait. Goto ChooseOp.
        
        //Operation selection
        //Depending on the value of ProgMemOut, goto one of the instruction start states.
        CHOOSE_OPP              = 8'h00, //Data Flow
        READ_FROM_MEM_TO_A      = 8'h10, //Wait to find what address to read, save reg select.
        READ_FROM_MEM_TO_B 	    = 8'h11, //Wait to find what address to read, save reg select.
        READ_FROM_MEM_0         = 8'h12, //Set BUS_ADDR to designated address.
        READ_FROM_MEM_1         = 8'h13, //wait - Increments program counter by 2. Reset offset.
        READ_FROM_MEM_2         = 8'h14, //Writes memory output to chosen register, end op.
        WRITE_TO_MEM_FROM_A     = 8'h20, //Reads Op+1 to find what address to Write to.
        WRITE_TO_MEM_FROM_B     = 8'h21, //Reads Op+1 to find what address to Write to.
        WRITE_TO_MEM_0          = 8'h22, //wait - Increments program counter by 2. Reset offset.
        
        //Data Manipulation
        DO_MATHS_OPP_SAVE_IN_A 	= 8'h30, //The result of maths op. is available, save it to Reg A.
        DO_MATHS_OPP_SAVE_IN_B 	= 8'h31, //The result of maths op. is available, save it to Reg B.
        DO_MATHS_OPP_0          = 8'h32, //wait for new op address to settle. end op.
       
        //Conditional Branching
        IF_A_COMPARE_B_GOTO     = 8'h40, // Result of logical condition, if 8'h0, PC_next = PC + 2
        IF_GOTO_0               = 8'h41, // If branching, now have ROM[PC+1] so set PC_next = ROM[PC+1] 
        IF_GOTO_END             = 8'h44, // Waiting for ROM[PC_next] to be read. Then Goto ChooseOp. 
         
        //Unconditional Branch
        GOTO                    = 8'h50, // Wait for ROM[PC+1] to be read 
        GOTO_0                  = 8'h51, // Set PC_next = ROM[PC+1]
        GOTO_END                = 8'h54, // Waiting for ROM[PC_next] to be read. Then Goto ChooseOp. 
        
        //Function Call
        FUNCTION_START          = 8'h60, // Saved_Context = PC + 2. ROM[PC+1] available only on next cycle. So Goto GOTO0.
              
        //Return
        RETURN 	                = 8'h70, // Set PC_next = Saved_context
        RETURN_END 	            = 8'h71, // Wait for ROM[PC_next] to be read. Then Goto ChooseOp. 
                
        //Dereferencing
        DE_REFERENCE_A          = 8'h80, // Set RAM_Addr_next = Reg_A. New address propagates into RAM on next cycle. Save reg select.
        DE_REFERENCE_B          = 8'h81, // Set RAM_Addr_next = Reg_B. New address propagates into RAM on next cycle. Save reg select
        DE_REFERENCE_0          = 8'h82, // Wait for RAM[RAM_Addr_next]. Goto READ_FROM_MEM_2
        
        //NOP (No-Operation)
        NOP                     = 8'h90, // set PC_next = PC+1
        NOP_END                 = 8'h91; // wait for it to propagate. Goto ChooseOp. 


    //Sequential part of the State Machine.
    reg [7:0] NextState,CurrState = GET_THREAD_START_ADDR_0;

always@(posedge CLK) begin
	if(RESET) begin
		CurrState             <= IDLE;
		CurrProgCounter       <= 8'h00;
		CurrProgCounterOffset <= 1'b0;
		CurrBusAddr           <= 8'h00; 
		CurrBusDataOut        <= 8'h00;
		CurrBusDataOutWE      <= 1'b0;
		CurrRegA              <= 8'h00;
		CurrRegB              <= 8'h00;
		CurrRegSelect         <= 1'b0;
		CurrProgContext       <= 8'h00;
		CurrInterruptAck      <= 2'b00;
	end
	else begin
		CurrState             <= NextState;
		CurrProgCounter       <= NextProgCounter;
		CurrProgCounterOffset <= NextProgCounterOffset;
		CurrBusAddr           <= NextBusAddr;
		CurrBusDataOut        <= NextBusDataOut;
		CurrBusDataOutWE      <= NextBusDataOutWE;
		CurrRegA              <= NextRegA;
		CurrRegB              <= NextRegB;
		CurrRegSelect         <= NextRegSelect;
		CurrProgContext       <= NextProgContext;
		CurrInterruptAck      <= NextInterruptAck;
	end
end

//Combinatorial section – large!
always@* begin
	//Generic assignment to reduce the complexity of the rest of the S/M
	NextState              = CurrState;
	NextProgCounter        = CurrProgCounter;
	NextProgCounterOffset  = 1'b0;
	NextBusAddr	           = 8'hFF;
	NextBusDataOut 	       = CurrBusDataOut;
	NextBusDataOutWE       = 1'b0;
	NextRegA               = CurrRegA;
	NextRegB               = CurrRegB;
	NextRegSelect          = CurrRegSelect;
	NextProgContext        = CurrProgContext;
	NextInterruptAck       = 2'b00;

	//Case statement to describe each state
	case (CurrState)
		///////////////////////////////////////////////////////////////////////////////////////
		//Thread states.
		IDLE: 
		begin
            if(BUS_INTERRUPTS_RAISE[0]) 
                begin	// Interrupt Request A.
                    NextState         = GET_THREAD_START_ADDR_0;
                    NextProgCounter   = 8'hFF;
                    NextInterruptAck  = 2'b01;
                end
            else 
            if(BUS_INTERRUPTS_RAISE[1]) 
                begin //Interrupt Request B.
                    NextState         = GET_THREAD_START_ADDR_0;
                    NextProgCounter   = 8'hFE;
                    NextInterruptAck  = 2'b10;
                end
            else 
                begin
                    NextState         = IDLE;
                    NextProgCounter   = 8'hFF; //Nothing has happened.
                    NextInterruptAck  = 2'b00;
                end
        end
		//Wait state - for new prog address to arrive.
		GET_THREAD_START_ADDR_0:
			NextState = GET_THREAD_START_ADDR_1;

		//Assign the new program counter value
		GET_THREAD_START_ADDR_1:
			begin
				NextState       = GET_THREAD_START_ADDR_2;
				NextProgCounter = ProgMemoryOut;
			end

		//Wait for the new program counter value to settle
		GET_THREAD_START_ADDR_2:
			NextState = CHOOSE_OPP;

		///////////////////////////////////////////////////////////////////////////////////////
		//CHOOSE_OPP - Another case statement to choose which operation to perform
		CHOOSE_OPP: begin
			case (ProgMemoryOut[3:0])
				4'h0: NextState = READ_FROM_MEM_TO_A;
				4'h1: NextState = READ_FROM_MEM_TO_B;
				4'h2: NextState = WRITE_TO_MEM_FROM_A;
				4'h3: NextState = WRITE_TO_MEM_FROM_B;
				4'h4: NextState = DO_MATHS_OPP_SAVE_IN_A;
				4'h5: NextState = DO_MATHS_OPP_SAVE_IN_B;
				4'h6: NextState = IF_A_COMPARE_B_GOTO;
				4'h7: NextState = GOTO;
				4'h8: NextState = IDLE;
				4'h9: NextState = FUNCTION_START;
				4'hA: NextState = RETURN;
				4'hB: NextState = DE_REFERENCE_A;
				4'hC: NextState = DE_REFERENCE_B;
				4'hF: NextState = NOP;
				default: NextState = CurrState;
			endcase
			NextProgCounterOffset = 1'b1;
		    end

		///////////////////////////////////////////////////////////////////////////////////////
		//READ_FROM_MEM_TO_A : here starts the memory read operational pipeline.
		//Wait state - to give time for the mem address to be read. Reg select is set to 0
		READ_FROM_MEM_TO_A:
			begin
				NextState       = READ_FROM_MEM_0;
				NextRegSelect   = 1'b0;
			end

		//READ_FROM_MEM_TO_B : here starts the memory read operational pipeline.
		//Wait state - to give time for the mem address to be read. Reg select is set to 1
		READ_FROM_MEM_TO_B:
			begin
				NextState     = READ_FROM_MEM_0;
				NextRegSelect = 1'b1;
			end

		//The address will be valid during this state, so set the BUS_ADDR to this value.
		READ_FROM_MEM_0:
			begin
				NextState   = READ_FROM_MEM_1;
				NextBusAddr = ProgMemoryOut;
			end

		//Wait state - to give time for the mem data to be read
		//Increment the program counter here. This must be done 2 clock cycles ahead
		//so that it presents the right data when required.
		READ_FROM_MEM_1:
			begin
				NextState       = READ_FROM_MEM_2;
				NextProgCounter	= CurrProgCounter + 2;
		  end

		//The data will now have arrived from memory.  Write it to the proper register.
		READ_FROM_MEM_2:
			begin
				NextState = CHOOSE_OPP;
				if(!CurrRegSelect)
					NextRegA = BusDataIn;
				else
					NextRegB = BusDataIn;
			end

		///////////////////////////////////////////////////////////////////////////////////////
		//WRITE_TO_MEM_FROM_A : here starts the memory write operational pipeline.
		//Wait state - to find the address of where we are writing
		//Increment the program counter here. This must be done 2 clock cycles ahead
		//so that it presents the right data when required.
		WRITE_TO_MEM_FROM_A:
			begin
				NextState       = WRITE_TO_MEM_0;
				NextRegSelect   = 1'b0;
				NextProgCounter	= CurrProgCounter + 2;
		  end

		//WRITE_TO_MEM_FROM_B : here starts the memory write operational pipeline.
		//Wait state - to find the address of where we are writing
		//Increment the program counter here. This must be done 2 clock cycles ahead
		// so that it presents the right data when required.
		WRITE_TO_MEM_FROM_B:
			begin
				NextState       = WRITE_TO_MEM_0;
				NextRegSelect   = 1'b1;
				NextProgCounter	= CurrProgCounter + 2;
			end

		//The address will be valid during this state, so set the BUS_ADDR to this value,
		//and write the value to the memory location.
		WRITE_TO_MEM_0:
			begin
				NextState   = CHOOSE_OPP;
				NextBusAddr = ProgMemoryOut;
				if(!NextRegSelect)
					NextBusDataOut 	 = CurrRegA;
				else
					NextBusDataOut   = CurrRegB;
				    NextBusDataOutWE = 1'b1;
			end

		///////////////////////////////////////////////////////////////////////////////////////
		//DO_MATHS_OPP_SAVE_IN_A : here starts the DoMaths operational pipeline.
		//Reg A and Reg B must already be set to the desired values.  The MSBs of the
		//Operation type determines the maths operation type.  At this stage the result is
		//ready to be collected from the ALU.
		DO_MATHS_OPP_SAVE_IN_A:
			begin
				NextState       = DO_MATHS_OPP_0;
				NextRegA        = AluOut;
				NextProgCounter	= CurrProgCounter + 1;
			end

		//DO_MATHS_OPP_SAVE_IN_B : here starts the DoMaths operational pipeline
		//when the result will go into reg B.
		DO_MATHS_OPP_SAVE_IN_B:
			begin
				NextState       = DO_MATHS_OPP_0;
				NextRegB        = AluOut;
				NextProgCounter = CurrProgCounter + 1;
			end

		//Wait state for new prog address to settle.
		DO_MATHS_OPP_0:
			NextState = CHOOSE_OPP;


		///////////////////////////////////////////////////////////////////////////////////////
		//IF_A_COMPARE_B_GOTO : here starts the conditional branching operational pipeline.
        //Decision state to branch or not to branch based on ALU output. If branching, becomes
        //a wait state for ROM[PC+1]. ROM[PC+1] is the address we branch to if condition (from ALU) 
        //is evaluated to be true. If not branching, set PC_next = PC + 2. i.e. directly after 
        //this instruction. 
		IF_A_COMPARE_B_GOTO:
			begin
                //if the registers satisfy set logical condition, calculate jump
                if(AluOut == 8'h01)
                        NextState       = IF_GOTO_0;
                //otherwise skip to next instruction        
                else
                    begin
                        NextProgCounter	= CurrProgCounter + 2;
                        NextState       = IF_GOTO_END;
                    end
			end
			
        //IF_GOTO_0 : ROM[PC+1] avaiable, set PC_next = ROM[PC+1]
		IF_GOTO_0:
			begin
				NextState   = IF_GOTO_END;
				NextProgCounter = ProgMemoryOut;
			end

	    //IF_GOTO_END : Wait for new PC to settle
		IF_GOTO_END:
			NextState = CHOOSE_OPP;


		///////////////////////////////////////////////////////////////////////////////////////
        //GOTO : here starts the unconditional branching operational pipeline.
        //Wait stage for ROM[PC+1] - the PC we jump to.
		GOTO:
			NextState = GOTO_0;


		//GOTO_0 : ROM[PC+1] avaiable, set PC_next = ROM[PC+1]
		GOTO_0:
            begin
                NextState       = GOTO_END;
                NextProgCounter = ProgMemoryOut;
            end

        //GOTO_END : Wait for new PC to settle
		GOTO_END:
			NextState = CHOOSE_OPP;


		///////////////////////////////////////////////////////////////////////////////////////
		//FUNCTION_START : here starts the function call operational pipeline. 
		//Record PC+2 to Context register. Wait for ROM[PC+1] - the PC of the first 
		//instruction in the function
		FUNCTION_START:
			begin
				NextProgContext = CurrProgCounter + 2;
				NextState = GOTO_0;
			end


	    ///////////////////////////////////////////////////////////////////////////////////////
	    //FUNCTION_START : here starts the return operational pipeline. 
	    //Restores PC to value stored in Context register.
        RETURN:
			begin
				NextProgCounter = CurrProgContext;
				NextState = RETURN_END;
			end

        //GOTO_END : Wait for new PC to settle
		RETURN_END:
			NextState = CHOOSE_OPP;


		///////////////////////////////////////////////////////////////////////////////////////
		//DEREFERENCE_A : here starts the dereference operation pipeline. Record register 
		//destination. Set RAM_Addr_next = Reg_A. Wait for addres to proparate.
		DE_REFERENCE_A:
			begin

				NextRegSelect   = 1'b0;
				NextBusAddr     = CurrRegA;
				NextState       = DE_REFERENCE_0;
				
			end

		///////////////////////////////////////////////////////////////////////////////////////
		//DEREFERENCE_B : here starts the dereference operation pipeline. Record register 
		//destination. Set RAM_Addr_next = Reg_B. Wait for addres to proparate.
		DE_REFERENCE_B:
			begin
				NextRegSelect   = 1'b1;
                NextBusAddr     = CurrRegB;
                NextState       = DE_REFERENCE_0;
			end

		///////////////////////////////////////////////////////////////////////////////////////
		//DEREFERENCE_) : wait for RAM[Selected_Reg]. After, can proceed as normal 
		//memory read operation into selected register.
		DE_REFERENCE_0:
            begin		    
                NextState = READ_FROM_MEM_2;
                NextProgCounter = CurrProgCounter + 1;
            end
        
        
         //////////////////////////////////////////////////////////////////////////////////////
        //NOP : here starts the return operational pipeline of a NO-OPERATION 
        //Increments PC
        NOP:
            begin
                NextProgCounter = CurrProgCounter + 1;
                NextState = NOP_END;
            end    
        
        //NOP_END : wait for new PC to settle
        NOP_END:
            begin
			     NextState = CHOOSE_OPP;
            end    
                            


	endcase
end

endmodule
