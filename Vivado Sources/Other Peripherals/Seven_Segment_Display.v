`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
// 
// Create Date: 23.02.2017 16:42:19
// Module Name: Seven_Segment_Display
// Project Name: Microprocessor
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.2
// Description: This module is a LUT for decoding for the 7-segment display to
// transform the value entered into it into connections to actually display it
// 
// Dependencies: N / A
// Revision 1.0
// 
//////////////////////////////////////////////////////////////////////////////////


module Seven_Segment_Display(
    input [1:0] SEGMENT_POSITION,
    input [3:0] BINARY,
    input DOT,
    output reg [3:0] SEGMENT_SELECT,
    output reg [7:0] HEXADECIMAL
    );         

    
    //////////////////////////////////////////////
    // SEVEN SEGMENT DISPLAY CHOICE
    //////////////////////////////////////////////
     
     
     //select the segment
     always@* 
     begin
        case(SEGMENT_POSITION)
            0   :       SEGMENT_SELECT = 4'b1110;
            1   :       SEGMENT_SELECT = 4'b1101;
            2   :       SEGMENT_SELECT = 4'b1011;
            3   :       SEGMENT_SELECT = 4'b0111;
            default :   SEGMENT_SELECT = 4'b1111;
        endcase
     end
     
     
    //////////////////////////////////////////////
    // VALUE DECODING
    ////////////////////////////////////////////// 
     
    //cdecode the 4 bits of input into 8 bits of output
    always@* 
    begin
        case (BINARY)
            0    :    HEXADECIMAL [6:0] = 7'b1000000;
            1    :    HEXADECIMAL [6:0] = 7'b1111001;
            2    :    HEXADECIMAL [6:0] = 7'b0100100;
            3    :    HEXADECIMAL [6:0] = 7'b0110000;
            
            4    :    HEXADECIMAL [6:0] = 7'b0011001;
            5    :    HEXADECIMAL [6:0] = 7'b0010010;
            6    :    HEXADECIMAL [6:0] = 7'b0000010;
            7    :    HEXADECIMAL [6:0] = 7'b1111000;
            
            8    :    HEXADECIMAL [6:0] = 7'b0000000;
            9    :    HEXADECIMAL [6:0] = 7'b0010000;
            10   :    HEXADECIMAL [6:0] = 7'b0001000;
            11   :    HEXADECIMAL [6:0] = 7'b0000011;
            
            12   :    HEXADECIMAL [6:0] = 7'b1000110;
            13   :    HEXADECIMAL [6:0] = 7'b0100001;
            14   :    HEXADECIMAL [6:0] = 7'b0000110;
            15   :    HEXADECIMAL [6:0] = 7'b0001110;
            
            default : HEXADECIMAL [6:0] = 7'b1111111;
        endcase   

    end

    //////////////////////////////////////////////
    // DOT SETTING
    //////////////////////////////////////////////  
      
    always@*
    begin
        //set the dot value
        HEXADECIMAL[7] <= ~DOT; 
    end
       
endmodule
