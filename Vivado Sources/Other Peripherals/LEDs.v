`timescale 1ns / 1ps


//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Antonis Efthymiou
//
// Design Name: Microprocessor
// Module Name: LEDs
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description:This module is the LEDs peripheral. It takes 2 address again:
//1) lower 8 LEDs 0-7
//2)Higher 8 LEDs 8-15

// Dependencies: N / A
// Revision: 1.0
//
//////////////////////////////////////////////////////////////////////////////////


module LEDs(
	input RESET, 
	input CLK, 
	input [7:0] BUS_DATA, 
	input [7:0] BUS_ADDR,
	input BUS_WE, 
	output [15:0] LED_OUT
);

	parameter [7:0] 
	BaseAddress = 8'hC0,
	HighAddress = 8'hC1;
	
	reg [7:0] First_8;
	reg [7:0] Last_8;
	
	//Depending on the address written by the processor choose register to write the data.
    //First_8 refers to the LEDs on the right from 0-7 
    //Last_8 refers to the LEDs on the left from 8-15
	always@(posedge CLK or posedge RESET)begin 
		if(RESET)begin
			First_8<= 8'h00;
			Last_8<= 8'h00;
		end 
		else if(BUS_ADDR==BaseAddress && BUS_WE)
			First_8<=BUS_DATA;
		else if(BUS_ADDR==HighAddress && BUS_WE)
		    Last_8<=BUS_DATA;
	end
	
	assign LED_OUT={Last_8,First_8};
	
endmodule