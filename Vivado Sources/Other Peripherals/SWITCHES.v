`timescale 1ns / 1ps



//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Antonis Efthymiou
//
// Design Name: Microprocessor
// Module Name: Switches
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: This module is an aditional peripheral to take inputs from the switches 
// it takes 2 addresses for the lower 8 switches and the higher 8 switches 
// Address:E0 for 7-0 
// Address=E1 for 15-8
//
// Dependencies: N / A
// Revision: 1.0
//////////////////////////////////////////////////////////////////////////////////

module SWITCHES(
	input RESET, 
	input CLK, 
	inout [7:0] BUS_DATA, 
	input [7:0] BUS_ADDR,
	input BUS_WE, 
	input [15:0] SWITCHES
);

	wire [7:0] DATA_IN;
	reg  DATA_OUT_EN;
	reg [7:0] DATA_OUT;
	
	assign DATA_IN=BUS_DATA;
	assign BUS_DATA=DATA_OUT_EN ? DATA_OUT : 8'hZZ;
	
	parameter [7:0] 
	First_8_ADDR=8'hE0,
	Last_8_ADDR=8'hE1;
	
	always@(posedge CLK or posedge RESET)begin 
		if(RESET)begin
			DATA_OUT<=8'h00;
			DATA_OUT_EN<=0;
		end
		else if (BUS_ADDR==First_8_ADDR)begin
			DATA_OUT_EN<=1;
			DATA_OUT<=SWITCHES[7:0];
		end	
		else if (BUS_ADDR==Last_8_ADDR)begin
			DATA_OUT_EN<=1;
			DATA_OUT<=SWITCHES[15:8];
		end
		else begin 
			DATA_OUT_EN<=0;
		end
	end

endmodule 