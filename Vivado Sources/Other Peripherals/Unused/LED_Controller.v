`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
//
// Create Date: 15.03.2017 14:59:09
// Design Name: Microprocessor
// Module Name: LED_Controller
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: This module allows writing to the LED peripheral at htheir address
// of 0xC0. First write is to the higher 8 bits i.e. and the second one is to 
// the lower 8 bits . This write location switching is controlled by a 
// 1-bit counter incremented each time a write occurs.
// 
// Dependencies: N / A
// 
// Revision 1.0
//
//////////////////////////////////////////////////////////////////////////////////


module LED_Controller(
    input         CLK,
    input         RESET,
    inout  [7:0]  BUS_DATA,
    input  [7:0]  BUS_ADDR,
    
    output reg [15:0] LED = 16'h00

);

    /////////////////////////////////////////
    // LOCAL VARIABLES
    ///////////////////////////////////////// 
    

    
    //the address to whihc one needs to write to acess this peripheral
    localparam LED_ADDR = 8'hC0;
    
    //values for the control register to know which bits to write to
    localparam 
    CONTROL_0 = 1'b0, 
    CONTROL_1 = 1'b1;
    
    //the register to hold the current and next LED values
    reg [15:0] LED_next;
    
    //registers to keep track of where the next write should occur
    reg Next_Write_next, Next_Write = 0;
     
    // specifiy we are only reading
    reg local_we = 1'b0;
    assign BUS_DATA  = local_we ? 8'h00 : 8'hZZ;
 
 
    /////////////////////////////////////////
    // DFF VALUE PROPAGATION
    /////////////////////////////////////////   
 
    always@(posedge CLK)
    begin
        if(RESET)
            begin
                Next_Write <= 0;
                LED        <= 0;
            end
        else
            begin
                Next_Write <= Next_Write_next;
                LED        <= LED_next;
            end    
                
    end

    /////////////////////////////////////////
    // CHECK FOR WRITE TO LED ADDRESSES
    ///////////////////////////////////////// 
     
    always@*
    begin
        //default assignment to keep valeus the same
        LED_next = LED;
        Next_Write_next = Next_Write;
        
        case(BUS_ADDR)
            //only read its own address
            LED_ADDR:
                begin     
                    // write to an address ot yet written to     
                    if(Next_Write == CONTROL_0)
                        LED_next[15:8] = BUS_DATA;
                    if(Next_Write == CONTROL_1)
                        LED_next[7:0] = BUS_DATA;
                    //next time write to other address    
                    Next_Write_next = Next_Write + 1;         
                end
        
        endcase
        
        
    end
     
     
     
     
     
     
     
    
    
    
endmodule
