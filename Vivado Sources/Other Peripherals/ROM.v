`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
//
// Design Name: Microprocessor
// Module Name: ROM
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: ROM holding the instructions to be executed by the processor
//
// Dependencies: N / A
// Revision: 1.0
//
//////////////////////////////////////////////////////////////////////////////////

module ROM(
	//standard signals
	input 			CLK,
	//BUS signals
	output reg 	[7:0] 	DATA,
	input 		[7:0] 	ADDR
);

parameter RAMAddrWidth = 8;

//Memory
reg [7:0] ROM [2**RAMAddrWidth-1:0];

//initialize with instructions and interrupt addresses
initial
begin
$readmemb("../../Memory Initialisation/Instruction_ROM.txt", ROM); 
end


//single port ram
always@(posedge CLK)
	DATA <= ROM[ADDR];

endmodule
