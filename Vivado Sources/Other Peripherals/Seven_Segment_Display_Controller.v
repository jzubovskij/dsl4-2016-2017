`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
// 
// Create Date: 23.02.2017 16:42:19
// Module Name: Seven_Segment_Display_Controller
// Project Name: Microprocessor
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.2
// Description: Seven Segment Display controller, including the bus read control,
// allowing writes to 0xD0 and 0xD1 to set the 2 leftmost adn 2 rightmost seven
// segment displays respectively
// 
// Dependencies: Seg7_Display
// 
// Revision 1.0
// 
//////////////////////////////////////////////////////////////////////////////////


module Seven_Segment_Display_Controller ( 
	input RESET, 
	input CLK, 
	inout [7:0] BUS_DATA, 
	input [7:0] BUS_ADDR,
	input BUS_WE, 
	output [3:0] SEG_SELECT,
    output [7:0] DEC_OUT
);


    /////////////////////////////////////////
    // LOCAL VARIABLES
    ///////////////////////////////////////// 

	localparam
	SEVEN_SEGMENT_ADDR0 = 8'hD0,
	SEVEN_SEGMENT_ADDR1 = 8'hD1;
	
	//register keepign values for the seven segmenty displays
	reg [15:0] Seven_Segments, Seven_Segments_next;
	
	//counter to control the operation of the 7 segment display
	reg [16:0] Counter_17_Bit, Counter_17_Bit_next;
	reg [1:0]  Counter_2_Bit,  Counter_2_Bit_next;  
	
	//the value heading into the seven segment display    
    reg [3:0] Seven_Segment_Value;
	
	// specifiy we are only reading
    reg local_we = 1'b0;
    assign BUS_DATA  = local_we ? 8'h00 : 8'hZZ;

 
    /////////////////////////////////////////
    // DFF VALUE PROPAGATION
    /////////////////////////////////////////   
	
	always@(posedge CLK)begin 
		if(RESET)
            begin        
                Counter_17_Bit <= 0;
                Counter_2_Bit  <= 0;
                Seven_Segments <= 0;
            end	
		else
            begin  		
                Counter_17_Bit <= Counter_17_Bit_next;
                Counter_2_Bit  <= Counter_2_Bit_next;  
                Seven_Segments <= Seven_Segments_next;
		    end
	end
	
	
    //////////////////////////////////////////////
    // CLOCK STEP DOWN
    /////////////////////////////////////////////	
	
	always@*
	begin
	
	   //step down the clock 4 and 10000 times
       Counter_2_Bit_next  = Counter_2_Bit;  
	   Counter_17_Bit_next = (Counter_17_Bit + 1) % 10000;
	   
	   if((Counter_17_Bit + 1) == 10000)
            Counter_2_Bit_next  = Counter_2_Bit+ 1; 
	end
	
	
	
    //////////////////////////////////////////////
    // CHECK FOR WRITE TO SEVEN_SEGMENT ADDRESSES
    /////////////////////////////////////////////
     
    always@*
    begin
        //default assignment to keep valeus the same
        Seven_Segments_next = Seven_Segments;
        
        case(BUS_ADDR)
            //if it is 0xD0 i.e. dedicated Seven Segment address #1
            SEVEN_SEGMENT_ADDR0: 
                Seven_Segments_next[7:0] = BUS_DATA;
                
            //if it is 0xD1 i.e. dedicated Seven Segment address #2
            SEVEN_SEGMENT_ADDR1: 
                Seven_Segments_next[15:8] = BUS_DATA;     
        endcase
    end	
    




    //////////////////////////////////////////////
    // SEVEN SEGMENT DISPLAY CHOICE
    /////////////////////////////////////////////
    
    always@*
    begin
        //select which of the 4 displays to write to
        case(Counter_2_Bit)
            0:      Seven_Segment_Value = Seven_Segments[3:0];
            1:      Seven_Segment_Value = Seven_Segments[7:4];
            2:      Seven_Segment_Value = Seven_Segments[11:8];
            3:      Seven_Segment_Value = Seven_Segments[15:12];
            default:Seven_Segment_Value = Seven_Segments[15:12];
        endcase
    end
    
    /////////////////////////////////////////
    // MODULE INSTANTIATIONS
    /////////////////////////////////////////      
 
   
    //Instantiating the decoder
    Seven_Segment_Display seven_segment_display(
                .SEGMENT_POSITION(Counter_2_Bit),
                .BINARY(Seven_Segment_Value),
                .DOT(1'b0),
                .SEGMENT_SELECT(SEG_SELECT),
                .HEXADECIMAL(DEC_OUT)
                );
         


endmodule