`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
// 
// Create Date: 23.02.2017 16:42:19
//
// Module Name: Microprocessor_Top
// Design Name: Microprocessor
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.2
// Description: Acts as the glue between the architectural components and 
// contains the busses through which data is transferred between the Processor
// and other components (peripherals)
// 
// Dependencies: VGA_Interface_Wrapper, Processor, ROM, RAM, Timer, LEDs,
// Seven_Segment_Display_Controller, IRTxWrapper, MouseTransceiver, SWITCHES
// 
// Revision 1.0
//////////////////////////////////////////////////////////////////////////////////


module Microprocessor_Top(


    input         CLK,
    input         RESET,
    input  [15:0] SWITCH,
    
    output        VGA_HS,
    output        VGA_VS, 
    output [3:0]  VGA_RED,
    output [3:0]  VGA_GREEN,
    output [3:0]  VGA_BLUE,
    output [15:0] LED,

    output [7:0]  SEVEN_SEGMENT,
    output [3:0]  SEVEN_SEGMENT_SELECT,
    output        IR_LED,
    inout         PS2_CLK,
    inout         PS2_DATA 

);

    /////////////////////////////////////////
    // LOCAL VARIABLES
    ///////////////////////////////////////// 
    
    //BUS wires
    wire [7:0] 
    BUS_DATA_wire,
    BUS_ADDR_wire,
    ROM_DATA_wire,  
    ROM_ADDRESS_wire;
    
    wire 
    BUS_WE_wire;
    
    //Interrput wires
    wire [1:0] 
    BUS_INTERRUPTS_RAISE_wire,
    BUS_INTERRUPTS_ACK_wire;
    
    // The VGA_Interface together with the VGA Bus Controller
    VGA_Interface_Wrapper vga_interface_wrapper(
    
        .CLK(CLK),
        .RESET(RESET),
                     
        .BUS_ADDR(BUS_ADDR_wire), 
        .BUS_DATA(BUS_DATA_wire),

        .VGA_HS(VGA_HS),
        .VGA_VS(VGA_VS), 
        .VGA_RED(VGA_RED),
        .VGA_GREEN(VGA_GREEN),
        .VGA_BLUE(VGA_BLUE)
        
     );
    
    // The instruction and data processing unit   
    Processor processor(
        .CLK(CLK),
        .RESET(RESET),
        .BUS_DATA(BUS_DATA_wire),
        .ROM_DATA(ROM_DATA_wire),
        .BUS_INTERRUPTS_RAISE(BUS_INTERRUPTS_RAISE_wire),
            
   
        .BUS_ADDR(BUS_ADDR_wire),
        .BUS_WE(BUS_WE_wire),
        .ROM_ADDRESS(ROM_ADDRESS_wire),
        .BUS_INTERRUPTS_ACK(BUS_INTERRUPTS_ACK_wire)
    );
    
    
    // Contains the instructions
    ROM rom( 
        .CLK(CLK),
        .DATA(ROM_DATA_wire),
        .ADDR(ROM_ADDRESS_wire)
    );    
        
    // Contains variables and constants
    RAM ram(
        .CLK(CLK),
        .BUS_DATA(BUS_DATA_wire),
        .BUS_ADDR(BUS_ADDR_wire),
        .BUS_WE(BUS_WE_wire)
    );    
    
   // 100 ms timer, triggers an interrupt every 100 ms 
   Timer timer(
        .CLK(CLK),
        .RESET(RESET),
        .BUS_DATA(BUS_DATA_wire),
        .BUS_ADDR(BUS_ADDR_wire),
        .BUS_WE(BUS_WE_wire),
        .BUS_INTERRUPT_ACK(BUS_INTERRUPTS_ACK_wire[1]),
        .BUS_INTERRUPT_RAISE(BUS_INTERRUPTS_RAISE_wire[1])
    );
    
    //allows us to set the LEDs
    LEDs led_controller(
        .CLK(CLK),
        .RESET(RESET),
        .BUS_DATA(BUS_DATA_wire),
        .BUS_ADDR(BUS_ADDR_wire),
        .BUS_WE(BUS_WE_wire),
        
        .LED_OUT(LED)
    
    );
    
    
    //allows us to set the Seven Segment Display
    Seven_Segment_Display_Controller seven_segment_display ( 
        .RESET(RESET), 
        .CLK(CLK), 
        .BUS_DATA(BUS_DATA_wire), 
        .BUS_ADDR(BUS_ADDR_wire),
        .BUS_WE(BUS_WE_wire), 
        .SEG_SELECT(SEVEN_SEGMENT_SELECT),
        .DEC_OUT(SEVEN_SEGMENT)
    );
    
    //instantiating the IR transmitter
    IRTxWrapper ir_module(
        .RESET(RESET), 
        .CLK(CLK), 
        .BUS_DATA(BUS_DATA_wire), 
        .BUS_ADDR(BUS_ADDR_wire),
        .BUS_WE(BUS_WE_wire), 
        .IR_LED(IR_LED)
    );
    
    //instantiating the mouse interface
    MouseTransceiver Mouse_interface( 
        .RESET(RESET), 
        .CLK(CLK), 
        .BUS_DATA(BUS_DATA_wire), 
        .BUS_ADDR(BUS_ADDR_wire),
        .BUS_WE(BUS_WE_wire), 
        .BUS_INTERRUPT_ACK(BUS_INTERRUPTS_ACK_wire[0]),
        .BUS_INTERRUPT_RAISE(BUS_INTERRUPTS_RAISE_wire[0]), 
	
        .CLK_MOUSE(PS2_CLK), 
        .DATA_MOUSE(PS2_DATA)
 
    );
    
    //intantiating the switches peripheral
    SWITCHES switch_peripheral(
        .RESET(RESET), 
        .CLK(CLK), 
        .BUS_DATA(BUS_DATA_wire), 
        .BUS_ADDR(BUS_ADDR_wire),
        .BUS_WE(BUS_WE_wire), 
        .SWITCHES(SWITCH)
    );
     
        
    
endmodule
