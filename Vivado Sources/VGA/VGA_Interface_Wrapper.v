`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
//
// Design Name: Microprocessor
// Module Name: VGA_Interface_Wrapper
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: Connects the VGA_Interface to  VGA_Interface_Bus_Controller
// which allows the Processor to configure both the Config_Colours and write
// to the Frame Buffer at specified addresses
//
// Dependencies: VGA_Interface. VGA_Interface_Bus_Controller
// Revision: 1.0
//////////////////////////////////////////////////////////////////////////////////


module VGA_Interface_Wrapper(

    input        CLK,
    input        RESET,

    input   [7:0]   BUS_ADDR,
    inout   [7:0]   BUS_DATA,
    
    output       VGA_HS,
    output       VGA_VS,
    output [3:0] VGA_RED,
    output [3:0] VGA_GREEN,
    output [3:0] VGA_BLUE
    
    
    

 );



    ////////////////////////////////////////
    // LOCAL VARIABLES
    ///////////////////////////////////////


    //Frame buffer A Port's wires
    wire [14:0] A_ADDR_wire;
    wire A_DATA_IN_wire, A_WE_wire; 
    
    wire A_DATA_OUT_wire;

    //VGA Generated coulour config wire
    wire [15:0] CONFIG_COLOURS_wire;

    //VGA genrated coulour signal wire
    wire [7:0] VGA_COLOUR_wire;

    ////////////////////////////////////////
    // WIRE ASSIGNMENT
    ///////////////////////////////////////

    //assigning the generated VGA_COLOUR to the VGA pins
    assign VGA_RED   = { VGA_COLOUR_wire[7],  VGA_COLOUR_wire[7:5] };
    assign VGA_GREEN = { VGA_COLOUR_wire[4],  VGA_COLOUR_wire[4:2] };
    assign VGA_BLUE  = { VGA_COLOUR_wire[1],  VGA_COLOUR_wire[1], VGA_COLOUR_wire[1:0] };
    

    ////////////////////////////////////////
    // MODULE INSTANTIATIONS
    ///////////////////////////////////////
    
    // Control the write protocols for the config colours and to
    // the frame buffer memory
    VGA_Interface_Bus_Controller vga_interface_bus_controller(
     
         .CLK(CLK),
         .RESET(RESET),
         .BUS_ADDR(BUS_ADDR),
         .BUS_DATA(BUS_DATA),
     
         .CONFIG_COLOURS(CONFIG_COLOURS_wire),
         .A_ADDR(A_ADDR_wire),
         .A_DATA_IN(A_DATA_IN_wire),
         .A_WE(A_WE_wire)
         
     ); 

    //generate the VGA signals and data
     VGA_Interface vga_interface(
        .CLK(CLK),
        .RESET(RESET),
        .CONFIG_COLOURS(CONFIG_COLOURS_wire),
        .A_ADDR(A_ADDR_wire),
        .A_DATA_IN(A_DATA_IN_wire),
        .A_WE(A_WE_wire),

        .A_DATA_OUT(A_DATA_OUT_wire),
        .VGA_HS(VGA_HS),
        .VGA_VS(VGA_VS),
        .VGA_COLOUR(VGA_COLOUR_wire)
     );
     
 



 endmodule
