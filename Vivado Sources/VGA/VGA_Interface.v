`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
//
// Design Name: VGA Interface
// Module Name: VGA_Interface
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: Links the VGA_Interface to the Frame Buffer. Signal generator
// reads the pixel (1 / 0) based on (foreground / background) and outputs 
// the corresponding colour (out of Config Colours) onto the screen
// 
// Dependencies: Frame_Buffer, VGA_Sig_Gen
// Revision: 1
//////////////////////////////////////////////////////////////////////////////////

module VGA_Interface(

    input CLK,
    input RESET,
    input [15:0] CONFIG_COLOURS,
    input [14:0] A_ADDR,
    input A_DATA_IN,
    input A_WE,
    
    output A_DATA_OUT,
    output VGA_HS,
    output VGA_VS,
    output [7:0] VGA_COLOUR
    
);
 
    
    ///////////////////////////////////////////////
    // LOCAL VARIABLES
    //////////////////////////////////////////////
    

    //Clock wires
    wire DPR_CLK_wire, B_CLK_wire, B_DATA_wire;
    
    //Frame buffer B port to VGa Sig Gen wires
    wire VGA_DATA_wire;
    wire [14:0] VGA_ADDR_wire, B_ADDR_wire;     
    
    ///////////////////////////////////////////////
    // WIRE CONNECTION - INTERNAL 
    //////////////////////////////////////////////
    

    //Inter-module connections
    assign {VGA_DATA_wire, B_ADDR_wire} = {B_DATA_wire, VGA_ADDR_wire};
    
    //Clock wire connections
    assign {A_CLK_wire, B_CLK_wire} = {CLK, CLK};
    
    
    ///////////////////////////////////////////////
    // MODULE INSTANTIATION
    //////////////////////////////////////////////
    
    //Generates the VGA protocol interface signals
    VGA_Sig_Gen vga_sig_gen(
        .CLK(CLK),
        .RESET(RESET),
        .DPR_CLK(DPR_CLK_wire),
        .CONFIG_COLOURS(CONFIG_COLOURS),
        
        .VGA_DATA(VGA_DATA_wire),
        .VGA_ADDR(VGA_ADDR_wire),
        .VGA_HS(VGA_HS),
        .VGA_VS(VGA_VS),
        .VGA_COLOUR(VGA_COLOUR)
        
     );
     
     //Contains data which pixels are forgeround and which ones 
     //are background
     Frame_Buffer frame_buffer(
    
        .A_CLK(A_CLK_wire),
        .A_WE(A_WE), 
        .A_ADDR(A_ADDR), 
        .A_DATA_IN(A_DATA_IN),
        .A_DATA_OUT(A_DATA_OUT),
         
        .B_CLK(B_CLK_wire),
        .B_ADDR(B_ADDR_wire), 
        .B_DATA(B_DATA_wire)
     );
 
endmodule
 