`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
//
// Design Name: VGA Interface
// Module Name: Frame_Buffer
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: Acts as the 2-port RAM memory for the VGA Interface. Contains 
// data on which pixels are foreground and which ones are background (1 and 0
// respectively).
// 
// Dependencies: N/A
// Revision: 1.0
//////////////////////////////////////////////////////////////////////////////////

module Frame_Buffer(

    input A_CLK,
    input [14:0] A_ADDR,
    input A_DATA_IN,    
    input A_WE,           

    input B_CLK,
    input [14:0] B_ADDR, 
    
    output reg A_DATA_OUT,
    output reg B_DATA
    
 );
 
 
    //////////////////////////////////////////
    // LOCAL CONSTANTS
    ////////////////////////////////////////// 


    //number of RAM clots we need
    parameter MEM_WIDTH = 2**15-1;  
    reg [0:0] Mem [MEM_WIDTH:0]; 
    
   

    //////////////////////////////////////////
    // RAMD R/W CONTROL
    ////////////////////////////////////////// 
    
     // Port A - Read/Write e.g. to be used by microprocessor
    always@(posedge A_CLK) begin
        if(A_WE)
            Mem[A_ADDR] <= A_DATA_IN;
        A_DATA_OUT <= Mem[A_ADDR];
    end
     // Port B - Read Only e.g. to be read from the VGA signal generator module for display
    always@(posedge B_CLK)
        B_DATA <= Mem[B_ADDR];

endmodule
