`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
//
// Create Date: 15.03.2017 14:59:09
// Design Name: Microprocessor
// Module Name: VGA_Interface_Bus_Controller
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: The module is meant to manage the writes to the VGA assigned 
// addresses 0xB0, 0xB1, 0xB2. In particular, it records which addresses 
// were written to since last reset (of the records). The it decides if it 
// should use the data to write to Frame Buffer: sequence of writes is 
// 0xB0 => X_ADDR, 0xB1 => Y_ADDR, 0xB2 => Pixel_value ( 1 ir 0). Or it can 
// also be a write to config colours 0xB1 => foreground, 0xB2 => background
// colours. Then it resets the records and waits until next completion of one of
// these protocols.
// 
// Dependencies: N / A
// 
// Revision 1.0
// 
//////////////////////////////////////////////////////////////////////////////////


module VGA_Interface_Bus_Controller(

    input             CLK,
    input             RESET,
    input      [7:0]  BUS_ADDR,
    inout      [7:0]  BUS_DATA,

    output reg [15:0] CONFIG_COLOURS = 16'hFF00,
    output reg [14:0] A_ADDR,
    output reg        A_DATA_IN,
    output reg        A_WE 
    
); 
    
    /////////////////////////////////////////
    // LOCAL VARIABLES
    ///////////////////////////////////////// 
    
    localparam
    
    // write to frame buffer protocol completion (write) pattern
    XYPIXEL_CHANGED = 3'b111,
    // write to config coulourscompletion (write) pattern
    CONFIG_CHANGED = 3'b011,
    // VGA BUSS_ADDR value corresponding to function of adddress   
    VGA_X_ADDRESS      = 8'hB0,
    VGA_Y_ADDRESS      = 8'hB1,
    VGA_PIXEL_ADDRESS  = 8'hB2;
    
     
    // vector describing which which addresses were written to
    // since last protocol completion
    reg [2:0] Data_Change_Vector_next, Data_Change_Vector = 3'b00;
    
     
    
    // registers holding (potential) new config colour
    // foreground and background colours
    reg [7:0] Config_FG_next, Config_FG = 0;
    reg [7:0] Config_BG_next, Config_BG = 0;
     
    // registers holding (potential) new X and Y ADDRESSES
    // for writing to Frame Buffer 
    reg [7:0] X_Addr_next, X_Addr = 8'b00000000;      
    reg [6:0] Y_Addr_next, Y_Addr = 7'b0000000;   
     
    // registers holding (potential) new Pixel value
    // to be written at specified addresses to Frame Buffer  
    reg Pixel_next, Pixel = 1'b0;
     
    
    // registers holding output signals for port A of the frame Buffer
    // and the CONFIG COLOURS            
    reg [15:0] CONFIG_COLOURS_next; 
    reg [14:0] A_ADDR_next;  
    reg A_DATA_IN_next;      
    reg A_WE_next;           
    
     
    // specifiy we are only reading
    reg local_we = 1'b0;
    assign BUS_DATA  = local_we ? 8'h00 : 8'hZZ;
    
    
    /////////////////////////////////////////
    // DFF VALUE PROPAGATION
    /////////////////////////////////////////   
    
    always@(posedge CLK)   
    begin
    
        if(RESET)   
            begin   
               X_Addr       <= 0;   
               Y_Addr       <= 0;    
               Pixel        <= 0;    
               Data_Change_Vector <= 0; 
               Config_FG <= 0;
               Config_BG <= 0;
               CONFIG_COLOURS <= 16'hFF00;   
               
               A_ADDR  <= 0;
               A_DATA_IN  <= 0;
               A_WE  <= 0;
    
            end
        else 
    
            begin
               X_Addr         <= X_Addr_next;   
               Y_Addr         <= Y_Addr_next;   
               Pixel          <= Pixel_next; 
               Data_Change_Vector   <= Data_Change_Vector_next;
               Config_FG <=  Config_FG_next;
               Config_BG <=  Config_BG_next;
               CONFIG_COLOURS <= CONFIG_COLOURS_next;
               
               A_ADDR  <= A_ADDR_next;
               A_DATA_IN  <= A_DATA_IN_next;
               A_WE  <= A_WE_next;
    
            end        
    
    end
    
     
        
    /////////////////////////////////////////
    // CHECK FOR WRITE TO VGA ADDRESSES
    /////////////////////////////////////////   
    
    always@(*)
    begin
    
        // default assignments to keep same value
        // unless write to correspondign address occurs
        X_Addr_next       = X_Addr;
        Y_Addr_next       = Y_Addr;
        Pixel_next        = Pixel; 
        Config_FG_next    = Config_FG;  
        Config_BG_next    = Config_BG;
        Data_Change_Vector_next = Data_Change_Vector;
            
    
        case(BUS_ADDR)
            // if BUS_ADDR = 0xB0
            VGA_X_ADDRESS:  
                begin
                  // if have not already recorded value  
                  if(~Data_Change_Vector[2])
                  begin 
                      // record change
                      Data_Change_Vector_next[2] = 1'b1;
                      // record(potential) X_ADDR
                      X_Addr_next = BUS_DATA;    
                  end
    
                end            
            // if BUS_ADDR = 0xB1
            VGA_Y_ADDRESS:
                begin   
                  // if have not already recorded value  
                  if(~Data_Change_Vector[1])
                  begin 
                      // record (potential) Y_ADDR
                      Y_Addr_next = BUS_DATA[6:0];
                      // record change     
                      Data_Change_Vector_next[1] = 1'b1;
                      //record (potential) foreground colour
                      Config_FG_next  = BUS_DATA;
                  end
    
                end
            // if BUS_ADDR = 0xB2
            VGA_PIXEL_ADDRESS:  
                begin
                  // if have not already recorded value
                  if(~Data_Change_Vector[0])
                  begin 
                      // get LSB to be (potential) Pixel value
                      Pixel_next = BUS_DATA[0];
                      // record change 
                      Data_Change_Vector_next[0] = 1'b1;
                      // record as (potential) background colour
                      Config_BG_next  = BUS_DATA;  
                  end  
                end
    
        endcase
        
        // write to frame buffer protocol completed, have all the data
        // reset on next clock cycle to be able to accept new value
        Data_Change_Vector_next = (Data_Change_Vector == 3'b111) ? 3'b000 : Data_Change_Vector_next;
       
        // write to config colours protocol completed, have all the data
        // reset on next clock cycle to be able to accept new value
        Data_Change_Vector_next = (Data_Change_Vector == 3'b011) ? 3'b000 : Data_Change_Vector_next;
       
    end

     
     
     
     
    /////////////////////////////////////////
    // CHECK FOR WRITE PROTOCOL COMPLETION
    /////////////////////////////////////////

    always@(*)
     
    begin
         
        // default assignments so no write occurs when 
        // data write protocol not completed 
        A_ADDR_next  = A_ADDR;
        A_DATA_IN_next  = A_DATA_IN;
        A_WE_next  = A_WE;
        
        // by default, keep the same config colours value
        CONFIG_COLOURS_next = CONFIG_COLOURS;

        // check if any write protocol was completed
        case(Data_Change_Vector)        
            // protocol for writing to Frame_Buffer Complete
            XYPIXEL_CHANGED:
                begin
                    //write recorded Pixel to recorde address
                    A_ADDR_next [14:8] = Y_Addr;
                    A_ADDR_next [7:0]  = X_Addr;
                    A_DATA_IN_next     = Pixel;
                    A_WE_next          = 1'b1;
                end
    
            // protocol for writing to CONFIG_COLOUR complete
            CONFIG_CHANGED:            
        
                begin
                    //write the recorded background and foreground
                    // colours to corresponding bits
                    CONFIG_COLOURS_next[15:8] = Config_FG;
                    CONFIG_COLOURS_next[7:0] =  Config_BG;
                end       
        endcase
     end   
    
endmodule
