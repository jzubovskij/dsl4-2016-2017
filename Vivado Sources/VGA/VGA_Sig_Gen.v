`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
//
// Design Name: VGA Interface
// Module Name: VGA_Sig_Gen
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: Generates all signals as specified by the VGA Protocol and outputs
// the Foreground or Background colour into pixel indicated by vertical and horizonal 
// counter based on if a 1/0 was read from the RAM (or other memory linked to it)
// 
// Dependencies: N/A
// Revision: 1.0
//////////////////////////////////////////////////////////////////////////////////

module VGA_Sig_Gen(
    input CLK,
    input [15:0] CONFIG_COLOURS,
    input VGA_DATA,
    input RESET,

    output DPR_CLK,
    output [14:0] VGA_ADDR,
    output reg VGA_HS,
    output reg VGA_VS,
    output reg [7:0] VGA_COLOUR
 );
 
    //////////////////////////////////////////
    // LOCAL CONSTANTS
    //////////////////////////////////////////

    // Total Horizontal Sync Pulse Time
    localparam 
    
    HTs = 800,
    // Horizontal Pulse Width Time 
    HTpw = 96,
    // Horizontal Display Time 
    HTDisp = 640,
    // Horizontal Back Porch Time 
    Hbp = 48,
    // Horizontal Front Porch Time 
    Hfp = 16, 
    
    // Total Vertical Sync Pulse Time
    VTs = 521,
    // Vertical Pulse Width Time 
    VTpw = 2, 
    // Vertical Display Time
    VTDisp = 480,
    // Vertical Back Porch Time 
    Vbp = 29,
    // Vertical Front Porch Time 
    Vfp = 10; 


    //////////////////////////////////////////
    // LOCAL VARIABLES
    //////////////////////////////////////////

    //Horizontal and vertical counters
    reg [9:0] HCounter_next, HCounter = 0;
    reg [9:0] VCounter_next, VCounter = 0;
    
    //VGA clock
    reg VGA_CLK_next, VGA_CLK = 1'b0;
    
    //Horizontal and vertical addresses of actual pixel from the memory
    reg [9:0] HAddress;
    reg [8:0] VAddress;
    
    //Vertical and Horizontal Sync
    reg  VGA_HS_next = 0;
    reg  VGA_VS_next = 0;
    
    //actual output colour (based on config and FB/BG)
    reg [7:0] VGA_COLOUR_next;
    
    //a divide by 2 counter to further divide the clock to 25Mhz
    reg Clock_Counter_next, Clock_Counter = 0;

    

    //////////////////////////////////////////
    // WIRE ASSIGNMENT
    //////////////////////////////////////////    
    
    
    assign DPR_CLK = VGA_CLK;
    //every memory address is a 4x4 pixel set
    assign VGA_ADDR = {VAddress[8:2], HAddress[9:2]};
    
    
    ////////////////////////////////////////////////
    // VGA CLOCK CONTROl
    ///////////////////////////////////////////////    
    
    
    always@(posedge CLK) begin
         if(RESET)
             begin
                VGA_CLK <= 0;
                Clock_Counter <= 0;
             end
         else
             begin
                VGA_CLK <= VGA_CLK_next;
                Clock_Counter <= Clock_Counter_next;
             end
     end
     
     
     
    always@*
    begin
        //increment cycle counter
        Clock_Counter_next = Clock_Counter + 1;
    end
    
    always@*
    begin
        // only invert on every clock cycle i.e. a divide by 4 clock
        VGA_CLK_next = (Clock_Counter_next == 1)?  ~VGA_CLK : VGA_CLK;
    end
    
    
    
    ////////////////////////////////////////////////
    // VGA REGISTER ASSIGNMENT
    /////////////////////////////////////////////// 

    always@(posedge VGA_CLK)
    begin
        if(RESET)
            begin
                HCounter   <= 0;
                VCounter   <= 0;
                VGA_HS     <= 0;
                VGA_VS     <= 0;
                VGA_COLOUR <= 0;               
            end
        else
            begin 
                HCounter   <= HCounter_next;
                VCounter   <= VCounter_next;
                VGA_HS     <= VGA_HS_next;
                VGA_VS     <= VGA_VS_next;
                VGA_COLOUR <= VGA_COLOUR_next;
            end
    end
    
    ////////////////////////////////////////////////
    // VGA H and V COUNTER CONTROL
    /////////////////////////////////////////////// 
    

    
    always@*
    begin
        //do not count past maximum needed value
        HCounter_next = (HCounter + 1) % HTs;
        VCounter_next = VCounter;
        //if next row
        if((HCounter + 1) % HTs == 0)
            begin
                //do not count past maximum needed value
                VCounter_next = (VCounter + 1) % VTs;   
            end
    end
    
    
    ////////////////////////////////////////////////
    // VGA SYNC CONTROL
    /////////////////////////////////////////////// 
   
    always@*
    begin
        //do not set sync unless starting new row or frame
        VGA_HS_next =1;
        VGA_VS_next =1;
        
        //if start of new row
        if(HCounter_next <  HTpw + 1)
            begin
                //indicate that with a HSYNC
                VGA_HS_next= 0;       
            end
            
        //if start of a new frame
        if(VCounter_next < VTpw + 1)
            begin
                //indicate that with a VSYNC
                VGA_VS_next = 0;
            end

    end
    
    
    ////////////////////////////////////////////////
    // MEMORY ADDRESS CONTROL
    ///////////////////////////////////////////////
    
    always@(HCounter or VCounter)
    begin
        //make it so (0,0) in visible region is also (0,0) in memory
        HAddress = HCounter - (HTpw + Hbp);
        VAddress = VCounter - (VTpw + Vbp);
    end
    
    
    ////////////////////////////////////////////////
    // VGA COLOUR CONTROL
    ///////////////////////////////////////////////
    
    always@(HCounter or VCounter or VGA_DATA or CONFIG_COLOURS)
    begin
        //unless in visible window, as per protocol output a 0
        VGA_COLOUR_next = 0;
        if((HCounter >  HTpw + Hbp -1) && (HCounter < HTs - Hfp +1))
        begin
            //if in visible region i.e. Display Time both vertically and horizontally
            if((VCounter > VTpw + Vbp -1) && (VCounter < VTs - Vfp +1))
            begin
                //if foreground, use foreground colour
                if(VGA_DATA == 1'b1)
                    VGA_COLOUR_next = CONFIG_COLOURS[15:8];
                //if background, use background colour
                else
                    VGA_COLOUR_next = CONFIG_COLOURS[7:0];
                
            end
        end

    end 
    
    

endmodule