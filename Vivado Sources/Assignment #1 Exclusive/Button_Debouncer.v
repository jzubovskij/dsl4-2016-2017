`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
//
// Design Name: VGA Interface
// Module Name: Button_Debouncer
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: Debounces the signal vlaue of a push button, stabilizing the 
// output value, instead of outputting all oscillations ocurring from the button's
// electromechanical design 
// 
// Dependencies: N/A
// Revision: 1
//////////////////////////////////////////////////////////////////////////////////


module Button_Debouncer(

	input CLK,
    input RESET, 
    input PB,				
	output reg 	PB_State = 1'b0 
	
);

    ///////////////////////////////////////////////
    // LOCAL CONSTANTS
    ///////////////////////////////////////////////   
    
    //how many clock cycles the inout should be stable for   
    localparam  STABILITY_COUNTER_WIDTH = 11 ;		
        
    
    ///////////////////////////////////////////////
    // LOCAL VARIABLES
    ///////////////////////////////////////////////   
       
    //clock cycle counter 
    reg  [STABILITY_COUNTER_WIDTH-1:0]	Counter_next, Counter = 0;
    
    //current and previous input registers
    reg DFF1, DFF2;	
    
    //conrtol wires								
    wire Counter_add, Counter_reset;
    
    //the next button value
    reg PB_State_next;
    
    ///////////////////////////////////////////////
    // WIRE ASSIGNMENT
    ///////////////////////////////////////////////   
       
       
    //is the new input the same as previous, if yes - reset
    assign Counter_reset = (DFF1  ^ DFF2);	
    //add only if not already reached a set value (MSB == 1)	
    assign Counter_add = ~(Counter[STABILITY_COUNTER_WIDTH-1]);			
        
    
    ///////////////////////////////////////////////
    // REGISTER ASSIGNMENT
    ///////////////////////////////////////////////  
        
    always@(posedge CLK)
    begin
        if(RESET)
            begin
                Counter  <= 0; 
                DFF1     <= 0; 
                DFF2     <= 0;
                PB_State <= 0;
            end
        else
            begin
                DFF1     <= PB;
                DFF2     <= DFF1;
                Counter  <= Counter_next;
                PB_State <= PB_State_next;
            end
    end
        
    ///////////////////////////////////////////////
    // BUTTON VALUE CONTROL
    ///////////////////////////////////////////////
    
    always@*
    begin
        //next button value,assign input if it is stable, else keep current value
        PB_State_next = (Counter[STABILITY_COUNTER_WIDTH-1] == 1'b1)? DFF2 : PB_State;    
    end
    
    ///////////////////////////////////////////////
    // COUNTER VALUE CONTROL
    ///////////////////////////////////////////////
    
    always @*
    begin
        case({Counter_reset, Counter_add})
            //do nothing, if value stable and counter at max
            2'b00 :   Counter_next = Counter;
            //if value stable, increment counter
            2'b01 :   Counter_next = Counter + 1;
            //if value not stable, reset counter
            default : Counter_next = {STABILITY_COUNTER_WIDTH{1'b0} };
        endcase 	
    end

endmodule



