`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
//
// Design Name: VGA Interface
// Module Name: Button_Rising_Edge_Detector
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: Detects the positive edge of a button's value i.e. when it is pressed
// 
// Dependencies: Button_Debouncer
// Revision: 1
//////////////////////////////////////////////////////////////////////////////////

module Button_Rising_Edge_Detector(

    input CLK, 
    input RESET,
    input PB,  
    output reg PB_Synched
    
);
    
    ///////////////////////////////////////////////
    // LOCAL VARIABLES
    ///////////////////////////////////////////////   
           
    //input history buffers   
    reg [3:0] Resync, Resync_next;
    reg       PB_Synched_next;
    
    //ouput connection wire
    wire PB_Debounced;
    
       
    
    ///////////////////////////////////////////////
    // REGISTER ASSIGNMENT
    ///////////////////////////////////////////////   
       
       
    always@(posedge CLK)
    begin
        if(RESET) 
           {Resync, PB_Synched} = 2'b00;
        else 
            begin
                Resync     <= Resync_next;  
                PB_Synched <= PB_Synched_next;
            end
    end    
    
    
    ///////////////////////////////////////////////
    // VALUE SYNC CONTROL
    ///////////////////////////////////////////////
    
    
    always@*
    begin
        //add the debounced value to the history buffer
        Resync_next = {Resync[2:0], PB_Debounced};
    end
    
    always@*
    begin
        //it is only a rising edge if the previous value was 0 and the new one is a 1
        PB_Synched_next = ~Resync[3] & Resync[2];
    end
    
    ///////////////////////////////////////////////
    // MODULE INSTANTIATIONS
    ///////////////////////////////////////////////
    
    //debounce the value    
    Button_Debouncer debouncer(
            .CLK(CLK), 
            .RESET(RESET),
            .PB(PB),  
            .PB_State(PB_Debounced)  
        );
        
       
    
endmodule
