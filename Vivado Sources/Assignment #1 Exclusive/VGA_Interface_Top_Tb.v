`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.02.2017 14:28:05
// Design Name: 
// Module Name: VGA_Sig_Gen_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module VGA_Interface_Top_Tb (
    );
    
    reg CLK;
    reg RESET;
    
    wire VGA_HS;
    wire VGA_VS;
    wire [3:0] VGA_RED;
    wire [3:0] VGA_GREEN;
    wire [3:0] VGA_BLUE;
    
    VGA_Interface_Top vga_interface_top(
    .CLK,
    .RESET,
    .VGA_HS,
    .VGA_VS, 
    .VGA_RED,
    .VGA_GREEN,
    .VGA_BLUE
 );

    initial begin
        CLK = 0;
        RESET = 0;
        forever #5 CLK = ~CLK;
    end
    
endmodule