`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
//
// Design Name: VGA Interface
// Module Name: Colour_Config_Gen
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: Creates the Config Colours values based on the user selected 
// configuration. It can switch background with foreground colours every 1 second, make 
// one always be the inverse of the other, accept colour values (without operation 
// applied above) from both switches (from corresponding pins) or generate the values itself
// 
// Dependencies: N/A
// Revision: 1
//////////////////////////////////////////////////////////////////////////////////


module Colour_Config_Gen #(

    parameter CLOCK_FREQ_MHZ         = 100,
    parameter COLOUR_INCREMENT       = 20,
    parameter DEFAULT_CONFIG_COLOURS = 16'h0000
)(

    input        CLK,
    input        RESET,
    input [15:0] SWITCHES,
    input        CONTROL_CONFIG_COLOURS,
    input        CONTROL_CONFIG_OPPOSITE,
    input        CONTROL_CONFIG_REVERSE,
    
    output reg [15:0] CONFIG_COLOURS  = DEFAULT_CONFIG_COLOURS

 );

      
    //////////////////////////////////////////
    // CLOCK CHARACTERISTIC CONSTANTS
    //////////////////////////////////////////
     
    localparam 
    MHz              = 10**6,
    CLOCKS_PER_SECOND = CLOCK_FREQ_MHZ*MHz,
     
      
    //////////////////////////////////////////
    // FUNCTIONALITY STATE CONSTANTS
    //////////////////////////////////////////
     
    SWITCHES_ONLY               = 3'b000,
    SWICTHES_REVERSE            = 3'b001,
    SWICTHES_OPPOSITE           = 3'b010,
    SWITCHES_OPPOSITE_REVERSE   = 3'b011,
     
    GENERATE_ONLY               = 3'b100,
    GENERATE_REVERSE            = 3'b101,
    GENERATE_OPPOSITE           = 3'b110,
    GENERATE_OPPOSITE_REVERSE   = 3'b111;
     
    //////////////////////////////////////////
    // LOCAL VARIABLES
    //////////////////////////////////////////
    
    //clock cycle counter 
    reg [$clog2(CLOCKS_PER_SECOND): 0] Clock_Counter_next, Clock_Counter = 0;   
    
    //config colours 
    reg [15:0] CONFIG_COLOURS_next;
    
    //have the colours been reversed (between FG and GB) 
    reg Colours_Reversed_next, Colours_Reversed = 0;
    
    //functionality control vector
    reg [2:0] CONTROL_VECTOR_next, CONTROL_VECTOR = 3'b000;
    
    //wire to check if a second has passed
    wire Second_Passed;
    
    //wires to increment the config colours in case generate is used
    wire [15:0] Incremented_Config;
    
    //////////////////////////////////////////
    // WIRE ASSIGNMENT
    //////////////////////////////////////////     
     
     
    assign Second_Passed = (Clock_Counter + 1) % CLOCKS_PER_SECOND == 0;
    assign Incremented_Config[15:8] = CONFIG_COLOURS[15:8] + COLOUR_INCREMENT;
    assign Incremented_Config[7:0]  = CONFIG_COLOURS[7:0]  + COLOUR_INCREMENT; 
     
    //////////////////////////////////////////
    // REGISTER ASSIGNMENT
    //////////////////////////////////////////
    
    always@(posedge CLK)
    begin
         if(RESET) begin
             Clock_Counter    <= 0;
             Colours_Reversed <= 0;
             CONFIG_COLOURS   <= DEFAULT_CONFIG_COLOURS;
             CONTROL_VECTOR   <= 3'b000;
             
         end
         else begin
             Colours_Reversed <= Colours_Reversed_next;
             Clock_Counter    <= Clock_Counter_next;
             CONFIG_COLOURS   <= CONFIG_COLOURS_next; 
             CONTROL_VECTOR   <= CONTROL_VECTOR_next;
             
         end
    end

     
     
    //////////////////////////////////////////
    // FUNCTIONALITY CONTROL
    //////////////////////////////////////////
    
    
    always@*
    begin
        //if corresponding button was pressed, toggle related functionality
        CONTROL_VECTOR_next[0] = (CONTROL_CONFIG_REVERSE)? ~CONTROL_VECTOR[0] : CONTROL_VECTOR[0];
    end
    
    always@*
    begin
        //if corresponding button was pressed, toggle related functionality
        CONTROL_VECTOR_next[1] = (CONTROL_CONFIG_OPPOSITE)? ~CONTROL_VECTOR[1] : CONTROL_VECTOR[1];
    end
    
    always@*
    begin
        //if corresponding button was pressed, toggle related functionality
        CONTROL_VECTOR_next[2] = (CONTROL_CONFIG_COLOURS)? ~CONTROL_VECTOR[2] : CONTROL_VECTOR[2];
    end
    
    
              
    //////////////////////////////////////////
    // CLOCK COUNTER CONTROL
    //////////////////////////////////////////
     
    always@*
    begin
        //increment counter, reset when it reached number of clock cycles corresponding to 1 second
        Clock_Counter_next = (Clock_Counter + 1) % CLOCKS_PER_SECOND;            
    end
     
    //////////////////////////////////////////
    // COLOUR REVERSE CONTROL
    //////////////////////////////////////////
     
    always@*
    begin
        
        Colours_Reversed_next = Colours_Reversed;
        //reverse colours (switch BG and FG ones) every 1 second (only actually applies if functionality enabled)
        if(Second_Passed)           
             Colours_Reversed_next = ~Colours_Reversed;
    end
     
     
    //////////////////////////////////////////
    // CONFIG COLOUR CONTROL
    //////////////////////////////////////////
    
    always@*
    begin
    
    
        //statement in case no change is due
        CONFIG_COLOURS_next = CONFIG_COLOURS;
        
        //switch the source of config colours acording to control state
        case (CONTROL_VECTOR) 
               SWITCHES_ONLY: 
                   begin
                        CONFIG_COLOURS_next = SWITCHES;
                   end      
               SWICTHES_REVERSE: 
                   begin
                        CONFIG_COLOURS_next[15:8] = (Colours_Reversed)? SWITCHES[15:8] : SWITCHES[7:0];
                        CONFIG_COLOURS_next[7:0]  = (Colours_Reversed)? SWITCHES[7:0]  : SWITCHES[15:8];
                   end
               SWICTHES_OPPOSITE: 
                   begin
                        CONFIG_COLOURS_next[15:8] =  SWITCHES[15:8];
                        CONFIG_COLOURS_next[7:0]  = ~SWITCHES[15:8];
                   end   
               SWITCHES_OPPOSITE_REVERSE: 
                   begin
                        CONFIG_COLOURS_next[15:8] = (Colours_Reversed)?  SWITCHES[15:8] : ~SWITCHES[15:8];
                        CONFIG_COLOURS_next[7:0]  = (Colours_Reversed)? ~SWITCHES[15:8] :  SWITCHES[15:8];
                   end                                                                                           
               GENERATE_ONLY: 
                   begin
                        //if time to change i.e. a second has passed
                        if(Second_Passed) 
                            begin
                                CONFIG_COLOURS_next[15:8] = Incremented_Config[15:8];
                                CONFIG_COLOURS_next[7:0]  = Incremented_Config[7:0];
                            end                        
                   end             
               GENERATE_REVERSE: 
                   begin
                        //if time to change i.e. a second has passed
                        if(Second_Passed) 
                            begin
                                CONFIG_COLOURS_next[15:8] = (Colours_Reversed)? Incremented_Config[15:8] : Incremented_Config[7:0];
                                CONFIG_COLOURS_next[7:0] =  (Colours_Reversed)? Incremented_Config[7:0]  : Incremented_Config[15:8];
                            end
                            
                   end
               GENERATE_OPPOSITE: 
                   begin
                        //if time to change i.e. a second has passed
                        if(Second_Passed) 
                            begin
                                CONFIG_COLOURS_next[15:8] =  Incremented_Config[15:8];
                                CONFIG_COLOURS_next[7:0]  = ~Incremented_Config[15:8];
                            end
                   end       
               GENERATE_OPPOSITE_REVERSE: 
                   begin
                        //if time to change i.e. a second has passed
                        if(Second_Passed) 
                            begin
                                CONFIG_COLOURS_next[15:8] = (Colours_Reversed)?  Incremented_Config[15:8] : ~Incremented_Config[15:8];
                                CONFIG_COLOURS_next[7:0]  = (Colours_Reversed)? ~Incremented_Config[15:8] :  Incremented_Config[15:8];
                            end
                                               
                   end                                                     
         endcase                 
    end
 
 
 endmodule
 