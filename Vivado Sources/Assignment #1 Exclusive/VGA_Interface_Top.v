`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
//
// Design Name: VGA Interface
// Module Name: VGA_Interface_Top
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: Establishes the connection of the 3 functional modules: the Button Detector, 
// VGA Interface and the Colour Config Generator among each other and to the corresponding 
// device pins (VGA Connector, Buttons and Switches etc.)
// 
// Dependencies: Button_Detector, Colour_Config_Gen, VGA_Interface
// Revision: 1
//////////////////////////////////////////////////////////////////////////////////


module VGA_Interface_Top(

    input        CLK,
    input        RESET,
    input [15:0] SWITCH,
    input        BTN_UP,
    input        BTN_LEFT,
    input        BTN_RIGHT,
    
    output       LED,
    output       VGA_HS,
    output       VGA_VS, 
    output [3:0] VGA_RED,
    output [3:0] VGA_GREEN,
    output [3:0] VGA_BLUE
    
 );
 

    ////////////////////////////////////////
    // LOCAL VARIABLES
    ///////////////////////////////////////
    
    
    //Frame buffer A Port's wires
    wire [14:0] A_ADDR_wire;
    wire A_DATA_IN_wire, A_WE_wire, A_DATA_OUT_wire;
    
    //VGA Generated coulour config wire
    wire [15:0] CONFIG_COLOURS_wire;    
    
    //VGA genrated coulour signal wire
    wire [7:0] VGA_COLOUR_wire;
    
    //Button pin synched wires
    wire BTN_UP_synched_wire, BTN_LEFT_synched_wire, BTN_RIGHT_synched_wire;
    
    //VGA Connector colour pin wires
    wire [3:0] VGA_RED_wire, VGA_GREEN_wire, VGA_BLUE_wire;

    ////////////////////////////////////////
    // WIRE ASSIGNMENT
    ///////////////////////////////////////
    
    assign VGA_RED   = { VGA_COLOUR_wire[7],  VGA_COLOUR_wire[7:5] };
    assign VGA_GREEN = { VGA_COLOUR_wire[4],  VGA_COLOUR_wire[4:2] };
    assign VGA_BLUE  = { VGA_COLOUR_wire[1],  VGA_COLOUR_wire[1], VGA_COLOUR_wire[1:0] };  
    
    //Placebo assignments to reduce warning count
    assign A_ADDR_wire = 1;
    assign A_DATA_IN_wire = 1;
    assign A_WE_wire = 0;
    assign LED = A_DATA_OUT_wire;
    
    
    ////////////////////////////////////////
    // MODULE INSTANTIATIONS
    ///////////////////////////////////////
    
    
    //detect rising edges of buttons
    Button_Detector#(    
        .BUTTON_COUNT(3)
    ) button_detector (
    
        .CLK(CLK), 
        .RESET(RESET),
        .BUTTONS({BTN_UP, BTN_LEFT, BTN_RIGHT}),  
        .BUTTONS_Synched({BTN_UP_Synched_wire, BTN_LEFT_Synched_wire, BTN_RIGHT_Synched_wire})
       
    );
    
     //generate config colours
     Colour_Config_Gen #() 
     colour_config_gen (
        .CLK(CLK),
        .RESET(RESET),
        .SWITCHES(SWITCH),
        .CONTROL_CONFIG_COLOURS(BTN_UP_Synched_wire),
        .CONTROL_CONFIG_OPPOSITE(BTN_LEFT_Synched_wire),
        .CONTROL_CONFIG_REVERSE(BTN_RIGHT_Synched_wire),
    
        .CONFIG_COLOURS(CONFIG_COLOURS_wire)
    
     );
     
    
    //generate the VGA signals and data
     VGA_Interface vga_interface(
        .CLK(CLK),
        .RESET(RESET),
        .CONFIG_COLOURS(CONFIG_COLOURS_wire),
        .A_ADDR(A_ADDR_wire),
        .A_DATA_IN(A_DATA_IN_wire),
        .A_WE(A_WE_wire),
        
        .A_DATA_OUT(A_DATA_OUT_wire),
        .VGA_HS(VGA_HS),
        .VGA_VS(VGA_VS),
        .VGA_COLOUR(VGA_COLOUR_wire)
     );
     

 endmodule
 