`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Jevgenij Zuboskij s1346981
//
// Design Name: VGA Interface
// Module Name: Button_Detector
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: Links each (device pin) button signal with an individual positive edge detector
// for use in other modules in "on button pressed" logic sequences
// 
// Dependencies: Button_Rising_Edge_Detector
// Revision: 1
//////////////////////////////////////////////////////////////////////////////////

module Button_Detector#(
    
    //specifies how many buttons need debouncing and edge detection
    parameter BUTTON_COUNT = 3
    
)(

    input                     CLK, 
    input                     RESET,
    input  [BUTTON_COUNT-1:0] BUTTONS,  
    output [BUTTON_COUNT-1:0] BUTTONS_Synched
 
);

    genvar button;
    generate
        //make an edge detector for each of them
        for (button=0; button<BUTTON_COUNT; button= button + 1) begin : instantiate_rising_edge_detector 
            //detect rising edge of the button
            Button_Rising_Edge_Detector edge_detector_up(
              
                .CLK(CLK),
                .RESET(RESET),
                .PB(BUTTONS[button]),  
                .PB_Synched(BUTTONS_Synched[button])
                  
            );   
        end 
    endgenerate

endmodule
