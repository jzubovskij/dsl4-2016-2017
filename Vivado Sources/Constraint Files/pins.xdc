###########################################
# 100Mhz CLOCK
###########################################

set_property PACKAGE_PIN W5 [get_ports CLK]
    set_property IOSTANDARD LVCMOS33 [get_ports CLK]


###########################################
# VGA BLUE
###########################################

set_property PACKAGE_PIN N18 [get_ports VGA_BLUE[0]]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_BLUE[0]]
set_property PACKAGE_PIN L18 [get_ports VGA_BLUE[1]]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_BLUE[1]]
set_property PACKAGE_PIN K18 [get_ports VGA_BLUE[2]]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_BLUE[2]]
set_property PACKAGE_PIN J18 [get_ports VGA_BLUE[3]]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_BLUE[3]]

###########################################
# VGA RED
###########################################

set_property PACKAGE_PIN G19 [get_ports VGA_RED[0]]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_RED[0]]
set_property PACKAGE_PIN H19 [get_ports VGA_RED[1]]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_RED[1]]
set_property PACKAGE_PIN J19 [get_ports VGA_RED[2]]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_RED[2]]
set_property PACKAGE_PIN N19 [get_ports VGA_RED[3]]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_RED[3]]


###########################################
# VGA GREEN
###########################################

set_property PACKAGE_PIN J17 [get_ports VGA_GREEN[0]]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_GREEN[0]]
set_property PACKAGE_PIN H17 [get_ports VGA_GREEN[1]]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_GREEN[1]]
set_property PACKAGE_PIN G17 [get_ports VGA_GREEN[2]]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_GREEN[2]]
set_property PACKAGE_PIN D17 [get_ports VGA_GREEN[3]]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_GREEN[3]]
    
###########################################
# VGA SYNC
###########################################

set_property PACKAGE_PIN P19 [get_ports VGA_HS]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_HS]
set_property PACKAGE_PIN R19 [get_ports VGA_VS]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_VS]
    
###########################################
# BUTTONS
###########################################

#RESET button   
set_property PACKAGE_PIN U18 [get_ports RESET]
    set_property IOSTANDARD LVCMOS33 [get_ports RESET] 
    
#MODE SWITCHING BUTTONS
#set_property PACKAGE_PIN T18 [get_ports BTN_UP]                        
#    set_property IOSTANDARD LVCMOS33 [get_ports BTN_UP]
#set_property PACKAGE_PIN W19 [get_ports BTN_LEFT]                        
#    set_property IOSTANDARD LVCMOS33 [get_ports BTN_LEFT]
#set_property PACKAGE_PIN T17 [get_ports BTN_RIGHT]                        
#    set_property IOSTANDARD LVCMOS33 [get_ports BTN_RIGHT]
##set_property PACKAGE_PIN U17 [get_ports BTN_DOWN]                        
#    set_property IOSTANDARD LVCMOS33 [get_ports BTN_DOWN]    
    
    
      
###########################################
# SWITCHES
###########################################
set_property PACKAGE_PIN V17 [get_ports {SWITCH[0]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[0]}]
set_property PACKAGE_PIN V16 [get_ports {SWITCH[1]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[1]}]
set_property PACKAGE_PIN W16 [get_ports {SWITCH[2]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[2]}]
set_property PACKAGE_PIN W17 [get_ports {SWITCH[3]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[3]}]
set_property PACKAGE_PIN W15 [get_ports {SWITCH[4]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[4]}]
set_property PACKAGE_PIN V15 [get_ports {SWITCH[5]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[5]}]
set_property PACKAGE_PIN W14 [get_ports {SWITCH[6]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[6]}]
set_property PACKAGE_PIN W13 [get_ports {SWITCH[7]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[7]}]
set_property PACKAGE_PIN V2 [get_ports {SWITCH[8]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[8]}]
set_property PACKAGE_PIN T3 [get_ports {SWITCH[9]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[9]}]
set_property PACKAGE_PIN T2 [get_ports {SWITCH[10]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[10]}]
set_property PACKAGE_PIN R3 [get_ports {SWITCH[11]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[11]}]
set_property PACKAGE_PIN W2 [get_ports {SWITCH[12]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[12]}]
set_property PACKAGE_PIN U1 [get_ports {SWITCH[13]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[13]}]
set_property PACKAGE_PIN T1 [get_ports {SWITCH[14]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[14]}]
set_property PACKAGE_PIN R2 [get_ports {SWITCH[15]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SWITCH[15]}] 
    
###########################################
# LEDS
###########################################  
set_property PACKAGE_PIN U16 [get_ports {LED[0]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[0]}]
set_property PACKAGE_PIN E19 [get_ports {LED[1]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[1]}]
set_property PACKAGE_PIN U19 [get_ports {LED[2]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[2]}]
set_property PACKAGE_PIN V19 [get_ports {LED[3]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[3]}]
set_property PACKAGE_PIN W18 [get_ports {LED[4]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[4]}]
set_property PACKAGE_PIN U15 [get_ports {LED[5]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[5]}]
set_property PACKAGE_PIN U14 [get_ports {LED[6]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[6]}]
set_property PACKAGE_PIN V14 [get_ports {LED[7]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[7]}]
set_property PACKAGE_PIN V13 [get_ports {LED[8]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[8]}]
set_property PACKAGE_PIN V3 [get_ports {LED[9]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[9]}]
set_property PACKAGE_PIN W3 [get_ports {LED[10]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[10]}]
set_property PACKAGE_PIN U3 [get_ports {LED[11]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[11]}]
set_property PACKAGE_PIN P3 [get_ports {LED[12]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[12]}]
set_property PACKAGE_PIN N3 [get_ports {LED[13]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[13]}]
set_property PACKAGE_PIN P1 [get_ports {LED[14]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[14]}]
set_property PACKAGE_PIN L1 [get_ports {LED[15]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {LED[15]}]
	
	
###########################################
# 7 SEGMENT DISPLAY
########################################### 	
set_property PACKAGE_PIN W7 [get_ports {SEVEN_SEGMENT[0]}]					
    set_property IOSTANDARD LVCMOS33 [get_ports {SEVEN_SEGMENT[0]}]
set_property PACKAGE_PIN W6 [get_ports {SEVEN_SEGMENT[1]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SEVEN_SEGMENT[1]}]
set_property PACKAGE_PIN U8 [get_ports {SEVEN_SEGMENT[2]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SEVEN_SEGMENT[2]}]
set_property PACKAGE_PIN V8 [get_ports {SEVEN_SEGMENT[3]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SEVEN_SEGMENT[3]}]
set_property PACKAGE_PIN U5 [get_ports {SEVEN_SEGMENT[4]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SEVEN_SEGMENT[4]}]
set_property PACKAGE_PIN V5 [get_ports {SEVEN_SEGMENT[5]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SEVEN_SEGMENT[5]}]
set_property PACKAGE_PIN U7 [get_ports {SEVEN_SEGMENT[6]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SEVEN_SEGMENT[6]}]
set_property PACKAGE_PIN V7 [get_ports SEVEN_SEGMENT[7]]                            
    set_property IOSTANDARD LVCMOS33 [get_ports SEVEN_SEGMENT[7]]

set_property PACKAGE_PIN U2 [get_ports {SEVEN_SEGMENT_SELECT[0]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SEVEN_SEGMENT_SELECT[0]}]
set_property PACKAGE_PIN U4 [get_ports {SEVEN_SEGMENT_SELECT[1]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SEVEN_SEGMENT_SELECT[1]}]
set_property PACKAGE_PIN V4 [get_ports {SEVEN_SEGMENT_SELECT[2]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SEVEN_SEGMENT_SELECT[2]}]
set_property PACKAGE_PIN W4 [get_ports {SEVEN_SEGMENT_SELECT[3]}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {SEVEN_SEGMENT_SELECT[3]}]
    
###########################################
# IR 
###########################################   

set_property PACKAGE_PIN G2 [get_ports {IR_LED}]                    
    set_property IOSTANDARD LVCMOS33 [get_ports {IR_LED}]  
    
    
###########################################
# MOUSE INTERFACE
########################################### 
 
set_property PACKAGE_PIN C17 [get_ports PS2_CLK]
    set_property IOSTANDARD LVCMOS33 [get_ports PS2_CLK]
    set_property PULLUP true [get_ports PS2_CLK]
        
set_property PACKAGE_PIN B17 [get_ports PS2_DATA] 
    set_property IOSTANDARD LVCMOS33 [get_ports PS2_DATA]
    set_property PULLUP true [get_ports PS2_DATA]
            
    
    