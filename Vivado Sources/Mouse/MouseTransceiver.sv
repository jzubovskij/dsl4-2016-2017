`timescale 1ns / 1ps




//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Antonis Efthymiou
//
// Design Name: Microprocessor
// Module Name: MouseTransceiver
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description:This is the top module that interacts with the mouse.
// It takes the raw data for the MouseX, MouseY, MouseStatus,Scroll and analyses them.
// It controls the the PS Data line and PS CLk line using signals from transmitter and Receiver 
// It has 5 addresses to comunnnicate with the Processor
// 1)A0=Status
// 2)A1=MouseX
// 3)A2=MouseY
// 4)A3=Scroll 
// 5)A4=Speed cotrol 

// Dependencies: MouseMasterSM, MouseTransmitter, MouseReceiver
// Revision: 1.0
//
//////////////////////////////////////////////////////////////////////////////////


module MouseTransceiver( 
	//Standard Inputs 
	input RESET, 
	input CLK, 
	//IO - Mouse side 
	inout CLK_MOUSE, 
	inout DATA_MOUSE,
	inout [7:0] BUS_DATA, 
	input [7:0] BUS_ADDR,
	input BUS_WE,
	output BUS_INTERRUPT_RAISE, 
	input BUS_INTERRUPT_ACK 
	// Mouse data information  
	); 
	
	reg [7:0] MouseX;
	reg [7:0] MouseY;
	reg [3:0] MouseStatus;
	// X, Y Limits of Mouse Position e.g. VGA Screen with 160 x 120 resolution 
	parameter [7:0] MouseLimitX = 160; 
	parameter [7:0] MouseLimitY = 120; 
	///////////////////////////////////////////////////////////////////// 
	parameter [7:0] MouseStatusADDR = 8'hA0;
	parameter [7:0] MouseXADDR=8'hA1;
	parameter [7:0] MouseYADDR=8'hA2;
	parameter [7:0] MouseScrollADDR=8'hA3;
	parameter [7:0] Speed_Control=8'hA4;
	//TriState Signals 
	//Clk 
	reg ClkMouseIn; 
	wire ClkMouseOutEnTrans; BEGIN : Write to VGA
    @: 30 : LOAD A              0x05    (  5) //a = x
    @: 32 : STORE A             0xb0    (176) //0xB0 = x
    @: 34 : LOAD A              0x09    (  9) //a = y
    @: 36 : STORE A             0xb1    (177) //0xB1 = y
    @: 38 : LOAD A              0x0d    ( 13) //a = pixel
    @: 40 : STORE A             0xb2    (178) //0xB2 =  pixel
    END : Write to VGABEGIN : Write to VGA
    @: 30 : LOAD A              0x05    (  5) //a = x
    @: 32 : STORE A             0xb0    (176) //0xB0 = x
    @: 34 : LOAD A              0x09    (  9) //a = y
    @: 36 : STORE A             0xb1    (177) //0xB1 = y
    @: 38 : LOAD A              0x0d    ( 13) //a = pixel
    @: 40 : STORE A             0xb2    (178) //0xB2 =  pixel
    END : Write to VGA


	//Data 
	wire DataMouseIn; 
	wire DataMouseOutTrans; 
	wire DataMouseOutEnTrans; 
	//Clk Output - can be driven by host or device 
	assign CLK_MOUSE = ClkMouseOutEnTrans ? 1'b0 : 1'bz; 
	//Clk Input 
	assign DataMouseIn = DATA_MOUSE; 
	//Clk Output - can be driven by host or device 
	assign DATA_MOUSE = DataMouseOutEnTrans ? DataMouseOutTrans : 1'bz; 
	///////////////////////////////////////////////////////////////////// 
	//This section filters the incoming Mouse clock to make sure that 
	//it is stable before data is latched by either transmitter 
	//or receiver modules 
	reg [7:0] MouseClkFilter; 
	always@(posedge CLK or posedge RESET) begin 
		if(RESET) begin
			ClkMouseIn <= 1'b0; 
            ////Ensure the register initialises 
			MouseClkFilter<=8'h00;
	    end
		else begin 
			//A simple shift register 
			MouseClkFilter[7:1] <= MouseClkFilter[6:0]; 
			MouseClkFilter[0] <= CLK_MOUSE; 
			//falling edge 
			if(ClkMouseIn & (MouseClkFilter == 8'h00)) 
				ClkMouseIn <= 1'b0; 
			//rising edge 
			else if(~ClkMouseIn & (MouseClkFilter == 8'hFF)) 
		ClkMouseIn <= 1'b1; 
		end 
	end 
	/////////////////////////////////////////////////////// 
	//Instantiate the Transmitter module 
	wire SendByteToMouse; 
	wire ByteSentToMouse; 
	wire [7:0] ByteToSendToMouse; 
	MouseTransmitter T( 
						//Standard Inputs 
						.RESET (RESET), 
						.CLK(CLK), 
						//Mouse IO - CLK 
						.CLK_MOUSE_IN(ClkMouseIn), 
						.CLK_MOUSE_OUT_EN(ClkMouseOutEnTrans), 
						//Mouse IO - DATA 
						////.DATA_MOUSE_IN(DataMouseIn), unnecessary
						.DATA_MOUSE_OUT(DataMouseOutTrans), 
						.DATA_MOUSE_OUT_EN (DataMouseOutEnTrans), 
						//Control 
						.SEND_BYTE(SendByteToMouse), 
						.BYTE_TO_SEND(ByteToSendToMouse), 
						.BYTE_SENT(ByteSentToMouse) 
	); 
	
	/////////////////////////////////////////////////////// 
	//Instantiate the Receiver module 
	wire ReadEnable; 
	wire [7:0] ByteRead; 
	wire [1:0] ByteErrorCode; 
	wire ByteReady; 
	MouseReceiver R( 
					//Standard Inputs 
					.RESET(RESET), 
					.CLK(CLK), 
					//Mouse IO - CLK 
					.CLK_MOUSE_IN(ClkMouseIn), 
					//Mouse IO - DATA 
					.DATA_MOUSE_IN(DataMouseIn), 
					//Control 
					.READ_ENABLE (ReadEnable), 
					.BYTE_READ(ByteRead), 
					.BYTE_ERROR_CODE(ByteErrorCode), 
					.BYTE_READY(ByteReady) 
	); 
	/////////////////////////////////////////////////////// 
	//Instantiate the Master State Machine module 
	wire [7:0] MouseStatusRaw; 
	wire [7:0] MouseDxRaw; 
	wire [7:0] MouseDyRaw;
	wire signed [7:0] Mouse_scroll_raw;
    wire signed [1:0] MouseNewScroll;
	reg  [1:0] MOUSE_SCROLL; 
	wire SendInterrupt; 
	MouseMasterSM MSM( 
					//Standard Inputs 
					.RESET(RESET), 
					.CLK(CLK), 
					//Transmitter Interface 
					.SEND_BYTE(SendByteToMouse), 
					.BYTE_TO_SEND(ByteToSendToMouse), 

					.BYTE_SENT(ByteSentToMouse), 
					//Receiver Interface 
					.READ_ENABLE (ReadEnable), 
					.BYTE_READ(ByteRead), 
					.BYTE_ERROR_CODE(ByteErrorCode), 
					.BYTE_READY(ByteReady), 
					//Data Registers 
					.MOUSE_STATUS(MouseStatusRaw), 
					.MOUSE_DX(MouseDxRaw), 
					.MOUSE_DY(MouseDyRaw),
					.Mouse_scroll(Mouse_scroll_raw), 
					.SEND_INTERRUPT(SendInterrupt)
					); 

	
	//Pre-processing - handling of overflow and signs. 
	//More importantly, this keeps tabs on the actual X/Y 
	//location of the mouse. 
	wire signed [8:0] MouseDx; 
	wire signed [8:0] MouseDy; 
	wire signed [8:0] MouseNewX; 
	wire signed [8:0] MouseNewY;
	wire signed [1:0] Mouse_scroll_filtered;
	reg [4:0] SpeedX; 
	reg [4:0] SpeedY;
	//DX and DY are modified to take account of overflow and direction 
	assign MouseDx = (MouseStatusRaw[6]) ? (MouseStatusRaw[4] ? {MouseStatusRaw[4],8'h00} : {MouseStatusRaw[4],8'hFF} ) : (MouseStatusRaw[4]?{MouseStatusRaw[4],MouseDxRaw[7:0]} :{MouseStatusRaw[4],MouseDxRaw[7:0]}); 
	// assign the proper expression to MouseDy 
	assign MouseDy = (MouseStatusRaw[7]) ? (MouseStatusRaw[5] ? {MouseStatusRaw[5],8'h00} : {MouseStatusRaw[5],8'hFF} ) :(MouseStatusRaw[5] ? {MouseStatusRaw[5],MouseDyRaw[7:0]}: {MouseStatusRaw[5],MouseDyRaw[7:0]}); 
	// calculate new mouse position 
	assign Mouse_scroll_filtered=Mouse_scroll_raw[1:0];
	assign MouseNewX = {1'b0,MouseX} + MouseDx; 
	assign MouseNewY = {1'b0,MouseY} - MouseDy; 
	assign MouseNewScroll=MOUSE_SCROLL + Mouse_scroll_filtered;
	

	always@(posedge CLK or posedge RESET) begin 
		if(RESET) begin 
			MouseStatus <= 0; 
			MouseX <= MouseLimitX/2; 
			MouseY <= MouseLimitY/2;
			MOUSE_SCROLL<=0; 
		end 
		else if (SendInterrupt) begin 
			//Status is stripped of all unnecessary info 
			MouseStatus <= MouseStatusRaw[3:0];
			     MOUSE_SCROLL<=MouseNewScroll;
			//X is modified based on DX with limits on max and min 
			if(MouseNewX < 0) 
				MouseX <= 0; 
			else if(MouseNewX > (MouseLimitX-1)) 
				MouseX <= MouseLimitX-1; 
			else begin  
			     if(MouseNewX>MouseX)
			         MouseX <= ((MouseNewX[7:0]-MouseX)/SpeedX) + MouseX ;
			      else 
			         MouseX <=MouseX-((MouseX-MouseNewX[7:0])/SpeedX) ;
			end
			//Y is modified based on DY with limits on max and min 
			if(MouseNewY < 0) 
				MouseY <= 0; 
			else if(MouseNewY > (MouseLimitY-1)) 
				MouseY <= MouseLimitY-1; 
			else begin  
			     if(MouseNewY[7:0]>MouseY)
			         MouseY <= ((MouseNewY[7:0]-MouseY)/SpeedY) + MouseY;
			     else 
			      MouseY <= MouseY-((MouseY-MouseNewY[7:0])/SpeedY) ;
			end			
		end 
	end 
	
	//to handle the data out tristate
	assign BUS_INTERRUPT_RAISE=SendInterrupt;
	wire [7:0] DATA_IN;
	reg  DATA_OUT_EN;
	reg [7:0] DATA_OUT;
	assign DATA_IN=BUS_DATA;
	assign BUS_DATA= DATA_OUT_EN ? DATA_OUT : 8'hZZ;
	
	//Depending on the address from the processor write the correct data out
	always@(posedge CLK or posedge RESET)begin 
		if(RESET)begin 
			DATA_OUT_EN<=0;
			DATA_OUT<=8'h00;
			SpeedX<=2'b01;
			SpeedY<=2'b01;
		end
		else if(BUS_ADDR==MouseStatusADDR)begin
			DATA_OUT_EN<=1;
			DATA_OUT<={5'd0,MouseStatus[2:0]};
		end
		else if(BUS_ADDR==MouseXADDR )begin
			DATA_OUT_EN<=1;
			DATA_OUT<=MouseX;
		end	
		else if(BUS_ADDR==MouseYADDR)begin
			DATA_OUT_EN<=1;
			DATA_OUT<=MouseY;
		end	
		else if(BUS_ADDR==MouseScrollADDR)begin 
		    DATA_OUT_EN<=1;
            DATA_OUT<=MOUSE_SCROLL[1:0];
		end
		else if(BUS_ADDR==Speed_Control && BUS_WE)begin    
		    SpeedX<=DATA_IN[3:0]+1'b1;
		    SpeedY<=DATA_IN[7:4]+1'b1;
		end
		else begin 
			DATA_OUT_EN<=0;
		end
	end
endmodule 