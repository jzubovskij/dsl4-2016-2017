`timescale 1ns / 1ps


//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Antonis Efthymiou
//
// Design Name: Microprocessor
// Module Name: MouseTransceiver
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: This module is a state machine that controls when to transmit to the mouse and when to receive from the mouse.
// It tells the transmitter the byte to send, and when to send it. it then receives from the receiver the signal that the byte was send
// It reads from the receiver the byte that was send and when is the byte ready.
// This is used to initialise the mouse and then continue reading the values from the mouse.
// The MouseRawX, MouseRawY, MouseRawStatus and MouseScroll is send to the transceiver at the top module 
//
// Dependencies: N / A
// Revision: 1.0
//
//////////////////////////////////////////////////////////////////////////////////

module MouseMasterSM( 
	input CLK, 
	input RESET, 
	//Transmitter Control 
	output SEND_BYTE, 
	output [7:0] BYTE_TO_SEND, 
	input BYTE_SENT, 
	//Receiver Control 
	output READ_ENABLE, 
	input [7:0] BYTE_READ, 
	input [1:0] BYTE_ERROR_CODE, 
	input BYTE_READY, 
	//Data Registers 
	output [7:0] MOUSE_DX, 
	output [7:0] MOUSE_DY, 
	output [7:0] MOUSE_STATUS,
	output [7:0] Mouse_scroll, 
	output SEND_INTERRUPT 
	); 
	////////////////////////////////////////////////////////////// 
	// Main state machine - There is a setup sequence 
	// 
	// 1) Send FF -- Reset command, 
	// 2) Read FA -- Mouse Acknowledge, 
	// 2) Read AA -- Self-Test Pass 
	// 3) Read 00 -- Mouse ID 
	// 4) Send F4 -- Start transmitting command, 
	// 5) Read FA -- Mouse Acknowledge, 
	// 
	// If at any time this chain is broken, the SM will restart from 
	// the beginning. Once it has finished the set-up sequence, the read enable flag 
	// is raised. 
	// The host is then ready to read mouse information 3 bytes at a time: 
	// S1) Wait for first read, When it arrives, save it to Status. Goto S2. 
	// S2) Wait for second read, When it arrives, save it to DX. Goto S3. 
	// S3) Wait for third read, When it arrives, save it to DY. Goto S1. 
	// Send interrupt. 
	//State Control 
	reg [5:0] Curr_State, Next_State; 
	reg [23:0] Curr_Counter, Next_Counter; 
	//Transmitter Control 
	reg Curr_SendByte, Next_SendByte; 
	reg [7:0] Curr_ByteToSend, Next_ByteToSend; 
	//Receiver Control 
	reg Curr_ReadEnable, Next_ReadEnable; 
	//Data Registers 
	reg [7:0] Curr_Status, Next_Status; 
	reg [7:0] Curr_Dx, Next_Dx; 
	reg [7:0] Curr_Dy, Next_Dy; 
	reg [7:0] Curr_scroll, Next_scroll; 
	reg Curr_SendInterrupt, Next_SendInterrupt; 
	//Sequential 
	always@(posedge CLK or posedge RESET) begin 
		if(RESET) begin 
			Curr_State <= 6'd0; 
			Curr_Counter <= 0; 
			Curr_SendByte <= 1'b0; 
			Curr_ByteToSend <= 8'h00; 
			Curr_ReadEnable <= 1'b0; 
			Curr_Status <= 8'h00; 
			Curr_Dx <= 8'h00; 
			Curr_Dy <= 8'h00; 
			Curr_scroll <= 8'h00; 
			Curr_SendInterrupt <= 1'b0; 
		end 
		else begin 
			Curr_State <= Next_State; 
			Curr_Counter <= Next_Counter; 
			Curr_SendByte <= Next_SendByte; 
			Curr_ByteToSend <= Next_ByteToSend; 
			Curr_ReadEnable <= Next_ReadEnable; 
			Curr_Status <= Next_Status; 
			Curr_Dx <= Next_Dx; 
			Curr_Dy <= Next_Dy; 
			Curr_scroll <= Next_scroll; 
			Curr_SendInterrupt <= Next_SendInterrupt; 
		end 
	end 
	//Combinatorial 
	always@* begin 
		Next_State = Curr_State; 
		Next_Counter = Curr_Counter; 
		Next_SendByte = 1'b0; 
		Next_ByteToSend = Curr_ByteToSend; 
		Next_ReadEnable = 1'b0; 
		Next_Status = Curr_Status; 
		Next_Dx = Curr_Dx; 
		Next_Dy = Curr_Dy; 
		Next_scroll = Curr_scroll; 
		Next_SendInterrupt = 1'b0; 
		case(Curr_State) 
			//Initialise State - Wait here for 10ms before trying to initialise the mouse. 
			6'd0: begin 
			////1/100th of sec at 100Mhz
				if(Curr_Counter == 1000000)begin
					Next_State = 6'd1; 
					Next_Counter = 0; 
				end 
				else 
					Next_Counter = Curr_Counter + 1'b1; 
				end 
			//Start initialisation by sending FF 
			6'd1: begin 
				Next_State = 6'd2; 
				Next_SendByte = 1'b1; 
				Next_ByteToSend = 8'hFF; 
			end 
			//Wait for confirmation of the byte being sent 
			6'd2: begin 
				if(BYTE_SENT) 
				Next_State = 6'd3; 
			end 
			//Wait for confirmation of a byte being received 
			//If the byte is FA goto next state, else re-initialise. 
			6'd3: begin 
			if(BYTE_READY) begin 
				if((BYTE_READ == 8'hFA) & (BYTE_ERROR_CODE == 2'b00)) 
					Next_State = 6'd4; 
				else 
					Next_State = 5'd0; 
				end 
				Next_ReadEnable = 1'b1; 
			end 
			//Wait for self-test pass confirmation 
			//If the byte received is AA goto next state, else re-initialise 
			6'd4: begin 
				if(BYTE_READY) begin 
					if((BYTE_READ == 8'hAA) & (BYTE_ERROR_CODE == 2'b00)) 
					Next_State = 5'd5; 
					else 
					Next_State = 5'd0; 
				end 
				Next_ReadEnable = 1'b1; 
			end 
			//Wait for confirmation of a byte being received 
			//If the byte is 00 goto next state (MOUSE ID) else re-initialise 
			6'd5: begin 
				if(BYTE_READY) begin 
					if((BYTE_READ == 8'h00) & (BYTE_ERROR_CODE == 2'b00)) 
						Next_State = 5'd6; 
					else 
						Next_State = 6'd0; 
				end 
				Next_ReadEnable = 1'b1; 
			end 
			6'd6: begin 
				Next_State = 6'd7; 
				Next_SendByte = 1'b1; 
				Next_ByteToSend = 8'hF3; 
			end 
			//Wait for confirmation of the byte being sent 
			6'd7: if(BYTE_SENT) Next_State = 6'd8; 
			//Wait for confirmation of a byte being received 
			////If the byte is FA and no error occured goto next state, else re-initialise 
			6'd8: begin 
				if(BYTE_READY) begin 
					if(BYTE_READ == 8'hFA & (BYTE_ERROR_CODE == 2'b00)) 
						Next_State = 6'd9; 
					else 
						Next_State = 6'd0; 
				end 
				Next_ReadEnable = 1'b1; 
			end 
			6'd9: begin 
				Next_State = 6'd10; 
				Next_SendByte = 1'b1; 
				Next_ByteToSend = 8'hC8; 
			end 
			
			//Wait for confirmation of the byte being sent 
			6'd10: if(BYTE_SENT) Next_State = 6'd11; 
			//Wait for confirmation of a byte being received 
			////If the byte is FA and no error occured goto next state, else re-initialise 
			6'd11: begin 
				if(BYTE_READY) begin 
					if(BYTE_READ == 8'hFA & (BYTE_ERROR_CODE == 2'b00)) 
						Next_State = 5'd12; 
					else 
						Next_State = 5'd0; 
				end 
				Next_ReadEnable = 1'b1; 
			end
			6'd12: begin 
				Next_State = 6'd13; 
				Next_SendByte = 1'b1; 
				Next_ByteToSend = 8'hF3; 
			end 
			//Wait for confirmation of the byte being sent 
			6'd13: if(BYTE_SENT) Next_State = 6'd14; 
			//Wait for confirmation of a byte being received 
			////If the byte is FA and no error occured goto next state, else re-initialise 
			6'd14: begin 
				if(BYTE_READY) begin 
					if(BYTE_READ == 8'hFA)// & (BYTE_ERROR_CODE == 2'b00)) 
						Next_State = 6'd15; 
					else 
						Next_State = 6'd0; 
				end 
				Next_ReadEnable = 1'b1; 
			end 
			6'd15: begin 
				Next_State = 6'd16; 
				Next_SendByte = 1'b1; 
				Next_ByteToSend = 8'h64; 
			end 
			
			//Wait for confirmation of the byte being sent 
			6'd16: if(BYTE_SENT) Next_State = 6'd17; 
			//Wait for confirmation of a byte being received 
			////If the byte is FA and no error occured goto next state, else re-initialise 
			6'd17: begin 
				if(BYTE_READY) begin 
					if(BYTE_READ == 8'hFA & (BYTE_ERROR_CODE == 2'b00)) 
						Next_State = 6'd18; 
					else 
						Next_State = 6'd0; 
				end 
				Next_ReadEnable = 1'b1; 
			end
			6'd18: begin 
				Next_State = 6'd19; 
				Next_SendByte = 1'b1; 
				Next_ByteToSend = 8'hF3; 
			end 
			//Wait for confirmation of the byte being sent 
			6'd19: if(BYTE_SENT) Next_State = 6'd20; 
			//Wait for confirmation of a byte being received 
			////If the byte is FA and no error occured goto next state, else re-initialise 
			6'd20: begin 
				if(BYTE_READY) begin 
					if(BYTE_READ == 8'hFA & (BYTE_ERROR_CODE == 2'b00)) 
						Next_State = 6'd21; 
					else 
						Next_State = 6'd0; 
				end 
				Next_ReadEnable = 1'b1; 
			end 
			6'd21: begin 
				Next_State = 6'd22; 
				Next_SendByte = 1'b1; 
				Next_ByteToSend = 8'h50; 
			end 
			
			//Wait for confirmation of the byte being sent 
			6'd22: if(BYTE_SENT) Next_State = 6'd23; 
			//Wait for confirmation of a byte being received 
			////If the byte is FA and no error occured goto next state, else re-initialise 
			6'd23: begin 
				if(BYTE_READY) begin 
					if(BYTE_READ == 8'hFA & (BYTE_ERROR_CODE == 2'b00)) 
						Next_State = 6'd24; 
					else 
						Next_State = 6'd0; 
				end 
				Next_ReadEnable = 1'b1; 
			end 
			//Send F4 - to start mouse transmit 
			6'd24: begin 
				Next_State = 6'd25; 
				Next_SendByte = 1'b1; 
				Next_ByteToSend = 8'hF2; 
			end 
			//Wait for confirmation of the byte being sent 
			6'd25: if(BYTE_SENT) Next_State = 6'd26; 
			//Wait for confirmation of a byte being received 
			////If the byte is FA and no error occured goto next state, else re-initialise 
			6'd26: begin 
				if(BYTE_READY) begin 
					if(BYTE_READ == 8'hFA & (BYTE_ERROR_CODE == 2'b00)) 
						Next_State = 6'd27; 
					else 
						Next_State = 6'd0; 
				end 
				Next_ReadEnable = 1'b1; 
			end 
			//Wait for the confirmation of a byte being received 
			//If the byte is 03 and no error occured goto next state, else re-initialise
			6'd27: begin 
				if(BYTE_READY) begin 
					if(BYTE_READ == 8'h03 & (BYTE_ERROR_CODE == 2'b00)) 
						Next_State = 6'd28; 
					else 
						Next_State = 6'd0; 
				end 
				Next_ReadEnable = 1'b1; 
			end 
			6'd28: begin 
                Next_State = 6'd29; 
                Next_SendByte = 1'b1; 
                Next_ByteToSend = 8'hF4; 
            end 
            //Wait for confirmation of the byte being sent 
            6'd29: if(BYTE_SENT) Next_State = 6'd30; 
            //Wait for confirmation of a byte being received 
            ////If the byte is FA and no error occured goto next state, else re-initialise 
            6'd30: begin 
                if(BYTE_READY) begin 
                    if(BYTE_READ == 8'hFA & (BYTE_ERROR_CODE == 2'b00)) 
                        Next_State = 6'd31; 
                    else 
                        Next_State = 6'd0; 
                end 
                Next_ReadEnable = 1'b1; 
            end 
			/////////////////////////////////////////////////////////// 
			//At this point the SM has initialised the mouse. 
			//Now we are constantly reading. If at any time 
			//there is an error, we will re-initialise 
			//the mouse - just in case. /////////////////////////////////////////////////////////// 
			//Wait for the confirmation of a byte being received. 
			//This byte will be the first of three, the status byte. 
			//If a byte arrives, but is corrupted, then we re-initialise 
////First wait for the Byte to be read and get the confirmation that is ready 
////Then check if the byte arrives with no corruption of data else re-initialise
			6'd31: begin 
				if(BYTE_READY)begin
                   if((BYTE_ERROR_CODE == 2'b00)) begin 
					   Next_State = 6'd32; 
					   Next_Status = BYTE_READ;
				   end 
			       else 
                       Next_State = 6'd0;
				end 
				Next_Counter = 0; 
				Next_ReadEnable = 1'b1; 
			end 
			//Wait for confirmation of a byte being received 
			//This byte will be the second of three, the Dx byte. 
			6'd32: begin 
				if(BYTE_READY)begin
                    if((BYTE_ERROR_CODE == 2'b00)) begin  
					   Next_State = 6'd33; 
					   Next_Dx = BYTE_READ; 
					end
			        else 
                       Next_State = 6'd0;
                end 
				Next_Counter = 0; 
				Next_ReadEnable = 1'b1; 				
			end 
			//Wait for confirmation of a byte being received 
			//This byte will be the third of three, the Dy byte. 
			6'd33: begin 
				if(BYTE_READY)begin
				    if((BYTE_ERROR_CODE == 2'b00)) begin 
					   Next_State = 6'd34; 
					   Next_Dy = BYTE_READ;
				    end 
			        else 
                       Next_State = 5'd0;
                end 
				Next_Counter = 0; 
				Next_ReadEnable = 1'b1; 				
			end
			//Wait for the confirmation of the byte being received
			//This Byte will be the 4th and will be the scroll  
			6'd34: begin 
                            if(BYTE_READY)begin
                                if((BYTE_ERROR_CODE == 2'b00)) begin 
                                   Next_State = 6'd35; 
                                   Next_scroll = BYTE_READ;
                                end 
                                else 
                                   Next_State = 5'd0;
                            end 
                            Next_Counter = 0; 
                            Next_ReadEnable = 1'b1;                 
                        end 
			//Send Interrupt State 
			6'd35: begin 
				Next_State = 6'd31; 
				Next_SendInterrupt = 1'b1; 
			end 
			//Default State 
			default: begin 
				Next_State = 6'd0; 
				Next_Counter = 0; 
				Next_SendByte = 1'b0; 
				Next_ByteToSend = 8'hFF; 
				Next_ReadEnable = 1'b0; 
				Next_Status = 8'h00; 
				Next_Dx = 8'h00; 
				Next_Dy = 8'h00; 
				Next_SendInterrupt = 1'b0; 
			end 
		endcase 
	end 
	/////////////////////////////////////////////////// 
	//Tie the SM signals to the IO 
	//Transmitter 
	assign SEND_BYTE = Curr_SendByte;
	assign BYTE_TO_SEND = Curr_ByteToSend; 
	//Receiver 
	assign READ_ENABLE = Curr_ReadEnable; 
	//Output Mouse Data 
	assign MOUSE_DX = Curr_Dx; 
	assign MOUSE_DY = Curr_Dy; 
	assign MOUSE_STATUS = Curr_Status; 
	assign Mouse_scroll = Curr_scroll; 
	assign SEND_INTERRUPT = Curr_SendInterrupt;
endmodule 



