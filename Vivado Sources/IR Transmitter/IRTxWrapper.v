`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Gordon Trinder
// 
// Create Date: 27.02.2017 14:55:48
// Design Name: 
// Module Name: IRTxWrapper
// Project Name: Microprocessor
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.3
// Description: Retrieves data from bus addresses. Uses these values to determine the speed, colour
// and direction of the car. These values are sent to multiplexer mux.v to send the correct output to
// IR_LED. State machines send the correct direction packet for each colour.
// Dependencies: IRTransmitterSM, mux, gen_counter, mux2
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module IRTxWrapper(
    input CLK,
    input RESET,
    input [7:0] BUS_DATA, 
    input [7:0] BUS_ADDR,
    input BUS_WE, 
    output IR_LED
    );
    
    wire ten_Trigger_out;
    wire twenty_Trigger_out;
    wire [23:0] COUNT10;
    wire [22:0] COUNT20;
    
    reg [7:0] COMMAND;          // Holds the direction code to instruct car which way to move
    reg [2:0] SELECTA;          // High bit controls speed of car, lower 2 bits control the car colour
    
    parameter [7:0] DirectionAddress    = 8'h90;     // Address in data memory is 0x90
    parameter [7:0] SpeedAddress        = 8'h91;     // Address in data memory is 0x91
    parameter [7:0] CarAddress          = 8'h92;     // Address in data memory is 0x92
    
    always@(posedge CLK) begin
        if(RESET) begin            
            COMMAND <= 8'h00;
            SELECTA <= 3'b000;
        end
        else if (BUS_ADDR == DirectionAddress && BUS_WE)    // When 0x90 is being written to
            COMMAND <= BUS_DATA;                            // the bus_data will be sent to COMMAND
        else if (BUS_ADDR == SpeedAddress && BUS_WE)   // When 0x91 is being written to, the last  
            begin         
                if (BUS_DATA[0])
                    SELECTA[2] <= ~SELECTA[2];
            end    
        else if (BUS_ADDR == CarAddress && BUS_WE)          // When 0x92 is being written to, the last  
            SELECTA[1:0] <= BUS_DATA[1:0];                  // 2 bits designate the car colour
    end
   
     gen_counter #(.COUNTER_WIDTH(24),            // Ten Hz counter to trigger mux2 module to alternate
                   .COUNTER_MAX(9999999)          // between zero dircetion packet and standard direction   
                   )                              // packet to enable slow speed feature
                   ten_hz_counter (
                   .CLK(CLK),
                   .RESET(RESET),
                   .ENABLE_IN(1'b1),
                   .TRIG_OUT(ten_Trigger_out),
                   .COUNT(COUNT10)
                   );     
                   
    gen_counter #(.COUNTER_WIDTH(23),            // 20Hz counter to send packets at 20Hz to enable slow speed
                  .COUNTER_MAX(4999999)
                  )
                  twenty_hz_counter (
                  .CLK(CLK),
                  .RESET(RESET),
                  .ENABLE_IN(1'b1),
                  .TRIG_OUT(twenty_Trigger_out),
                  .COUNT(COUNT20)
                  );     
   
   
// There are four colour codes for the RC cars, each car has its own set of parameters
// due to the different pulse frequencies and burst lengths for each colour code.
// A multiplexer chooses which output signal to transmit       
//--------------------   GREEN FAST   -----------------------------
   
IRTransmitterSM #(.StartBurstSize(88),     // Now using peripheral code released after first asessment
                .CarSelectBurstSize(44),   // Altered by adding CarrierCountMax parameter so extra counters 
                .GapSize(40),              // wouldn't be needed
                .AssertBurstSize(44),
                .DeAssertBurstSize(22),
                .CarrierCountMax(1333)
                )
                green_fast (
                .CLK(CLK),
                .RESET(RESET),
                .COMMAND(COMMAND),
                .SEND_PACKET(twenty_Trigger_out),    // Packets sent at 20Hz as opposed to 10Hz so that dual
                .IR_LED(green_fast_IR_LED)           // speed feature can be implemented
                );
   
//--------------------   GREEN ZERO   -----------------------------
                   
IRTransmitterSM #(.StartBurstSize(88),               // Dual speed works by alternating between a standard direction packet
                .CarSelectBurstSize(44),             // And a zero direction packet at 20Hz. Therefore a IRTransmitterSM is  
                .GapSize(40),                        // required to send zero direction packets for each colour
                .AssertBurstSize(44),
                .DeAssertBurstSize(22),
                .CarrierCountMax(1333)
                )
                green_zero (
                .CLK(CLK),
                .RESET(RESET),
                .COMMAND(4'b0000),                   // Zero direction command code
                .SEND_PACKET(twenty_Trigger_out),
                .IR_LED(green_zero_IR_LED)
                );
                
//--------------------   BLUE    -----------------------------

IRTransmitterSM #(.StartBurstSize(191),
              .CarSelectBurstSize(47),
              .GapSize(25),
              .AssertBurstSize(47),
              .DeAssertBurstSize(22),
              .CarrierCountMax(1389)
              )
              blue_fast (
              .CLK(CLK),
              .RESET(RESET),
              .COMMAND(COMMAND),
              .SEND_PACKET(twenty_Trigger_out),
              .IR_LED(blue_fast_IR_LED)
              );

//--------------------   BLUE ZERO    -----------------------------

IRTransmitterSM #(.StartBurstSize(191),
              .CarSelectBurstSize(47),
              .GapSize(25),
              .AssertBurstSize(47),
              .DeAssertBurstSize(22),
              .CarrierCountMax(1389)
              )
              blue_zero (
              .CLK(CLK),
              .RESET(RESET),
              .COMMAND(4'b0000),
              .SEND_PACKET(twenty_Trigger_out),
              .IR_LED(blue_zero_IR_LED)
              );

//--------------------   RED    -----------------------------

IRTransmitterSM #(.StartBurstSize(192),
            .CarSelectBurstSize(24),
            .GapSize(24),
            .AssertBurstSize(48),
            .DeAssertBurstSize(24),
            .CarrierCountMax(1389)
            )
            red_fast (
            .CLK(CLK),
            .RESET(RESET),
            .COMMAND(COMMAND),
            .SEND_PACKET(twenty_Trigger_out),
            .IR_LED(red_fast_IR_LED)
            );            
                                
//--------------------   RED ZERO   -----------------------------
           
IRTransmitterSM #(.StartBurstSize(192),
            .CarSelectBurstSize(24),
            .GapSize(24),
            .AssertBurstSize(48),
            .DeAssertBurstSize(24),
            .CarrierCountMax(1389)
            )
            red_zero (
            .CLK(CLK),
            .RESET(RESET),
            .COMMAND(4'b0000),
            .SEND_PACKET(twenty_Trigger_out),
            .IR_LED(red_zero_IR_LED)
            );            
                            
//--------------------   YELLOW    -----------------------------

IRTransmitterSM #(.StartBurstSize(88),
            .CarSelectBurstSize(22),
            .GapSize(40),
            .AssertBurstSize(44),
            .DeAssertBurstSize(22),
            .CarrierCountMax(1389)
            )
            yellow (
            .CLK(CLK),
            .RESET(RESET),
            .COMMAND(COMMAND),
            .SEND_PACKET(twenty_Trigger_out),
            .IR_LED(yellow_fast_IR_LED)
            );     
               
//--------------------   YELLOW  ZERO  -----------------------------
                       
IRTransmitterSM #(.StartBurstSize(88),
            .CarSelectBurstSize(22),
            .GapSize(40),
            .AssertBurstSize(44),
            .DeAssertBurstSize(22),
            .CarrierCountMax(1389)
            )
            yellow_zero (
            .CLK(CLK),
            .RESET(RESET),
            .COMMAND(4'b0000),
            .SEND_PACKET(twenty_Trigger_out),
            .IR_LED(yellow_zero_IR_LED)
            );   

// mux2 alternates between a zero direction packet and a standard direction packet every 100 ms. The packets are sent at 20Hz.
// There is one mux2 instantiation for each colour.

mux2 green_slow (
           .CLK(CLK),
           .RESET(RESET),
           .trigger(ten_Trigger_out),
           .in0(green_fast_IR_LED),
           .in1(green_zero_IR_LED),
           .out(green_slow_IR_LED)
           );
           
mux2 blue_slow (
           .CLK(CLK),
           .RESET(RESET),
           .trigger(ten_Trigger_out),
           .in0(blue_fast_IR_LED),
           .in1(blue_zero_IR_LED),
           .out(blue_slow_IR_LED)
           );
           
mux2 red_slow (
           .CLK(CLK),
           .RESET(RESET),
           .trigger(ten_Trigger_out),
           .in0(red_fast_IR_LED),
           .in1(red_zero_IR_LED),
           .out(red_slow_IR_LED)
           );
           
mux2 yellow_slow (
           .CLK(CLK),
           .RESET(RESET),
           .trigger(ten_Trigger_out),
           .in0(yellow_fast_IR_LED),
           .in1(yellow_zero_IR_LED),
           .out(yellow_slow_IR_LED)
           );

// The multiplexer is controlled using the SELECTA register. The value of SELECTA is
// based on data written to SpeedAddress (0x91) - corresponds to SELECTA[2] 
// and CarAddress (0x92) - corresponds to SELECTA[1:0].
// SELECTA controls the multiplexer in a binary configuration.
// 000 - green_fast
// 001 - blue_fast
// 010 - red_fast
// 011 - yellow_fast
// 100 - green_slow
// 101 - blue_slow
// 110 - red_slow
// 111 - yellow_slow
                       
mux car_and_speed_select (
           .control(SELECTA),
           .in0(green_fast_IR_LED),
           .in1(blue_fast_IR_LED),
           .in2(red_fast_IR_LED),
           .in3(yellow_fast_IR_LED),
           .in4(green_slow_IR_LED),
           .in5(blue_slow_IR_LED),
           .in6(red_slow_IR_LED),
           .in7(yellow_slow_IR_LED),
           .out(IR_LED)
           );
           
            
endmodule