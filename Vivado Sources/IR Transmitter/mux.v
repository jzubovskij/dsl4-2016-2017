`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Gordon Trinder
// 
// Create Date: 28.02.2017 06:41:15 
// Module Name: mux
// Design Name: Microprocessor
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.2
// Description: Simple 8 way multiplexer. Used for speed and car select.
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux(
        input [2:0] control,   // Simple 8 way multiplexer
        input in0,
        input in1,
        input in2,
        input in3,
        input in4,
        input in5,
        input in6,
        input in7,
        output reg out
        );


        // 3 bit control input dictates which input is sent to module output
        always@(control or in0 or in1 or in2 or in3 or in4 or in5 or in6 or in7)begin
            case(control)
                3'b000:out<=in0;
                3'b001:out<=in1;
                3'b010:out<=in2;
                3'b011:out<=in3;
                3'b100:out<=in4;
                3'b101:out<=in5;
                3'b110:out<=in6;
                3'b111:out<=in7;
                default:out<=5'b00000;
            endcase
        end
	
endmodule
