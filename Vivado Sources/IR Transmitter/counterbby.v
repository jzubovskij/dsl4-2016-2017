`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.02.2017 02:45:42
// Design Name: 
// Module Name: gen_counter
// Project Name: Microprocessor
// Target Devices: 
// Tool Versions: 
// Description: Generic counter module, increments a count register  
// every clock cycle enable_in is high
// Dependencies: None 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module gen_counter(

    CLK , // Clock input of the design
    RESET, // active high, synchronous Reset input
    ENABLE_IN , // Active high enable signal for counter
    COUNT ,// 4 bit vector output of the counter
    TRIG_OUT
    );

    input CLK ;
    input RESET ;
    input ENABLE_IN ;
    
    // These parameters determine the max value, and hence the trigger rate    
    parameter COUNTER_MAX = 9999999;
    parameter COUNTER_WIDTH = 24;
    output [COUNTER_WIDTH-1:0] COUNT ;
    output TRIG_OUT;

    wire CLK ;
    wire RESET ;
    wire ENABLE_IN ;
    reg [COUNTER_WIDTH-1:0] COUNT ;
    reg TRIG_OUT;
    
    // Since this counter is a positive edge trigged one,
    // We trigger the below block with respect to positive
    // edge of the clock.
    always @ (posedge CLK) begin
      // At every rising edge of clock  check if reset is active
      // If active, we load the counter output with zero
      if (RESET == 1'b1) begin
        COUNT <= #1 0;
      end
      
      else if (ENABLE_IN == 1'b1) begin
        if (COUNT == COUNTER_MAX)   // Counter resets once max value reached
            COUNT <= #1 0;
        else
            COUNT <= #1 COUNT + 1; // If enable is active, then we increment the counter
      end
    end
    
        always@(posedge CLK) begin
        if(RESET == 1'b1)
            TRIG_OUT <= 0;
        else begin
            if ((ENABLE_IN == 1'b1) && (COUNT == COUNTER_MAX)) // Reaching max value also
                TRIG_OUT <= 1'b1;                       // sets off the trigger, which can
            else                                        // be used for timing other parts of           
                TRIG_OUT <= 1'b0;                       // the digital system
        end
    end     
    
    endmodule // End of Module counter    

