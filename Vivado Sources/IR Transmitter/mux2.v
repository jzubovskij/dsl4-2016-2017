`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Gordon Trinder
// 
// Create Date: 28.03.2017 11:43:02
// Design Name: 
// Module Name: mux2
// Design Name: Microprocessor
// Target Devices: Basys 3 board (device ID: xc7a35tcpg236-1)
// Tool Versions: Vivado 2015.2
// Description: 2 way multiplexer that alternates output between two inputs when triggered.
// Used for dual speed feature, to alternate between zero direction code and assigned direction code.
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux2(
	input CLK,
input RESET,
input trigger,
input in0,
input in1,
output reg out
);  

reg alternator;

    always@(posedge CLK)begin            // When trigger reaches module, alternator flag goes lo->hi or hi->lo
        if (RESET)
            alternator <= 0; 
        if (trigger)
            alternator <= ~alternator;
    end
    
    always@(posedge CLK) begin    // If alternator flag is high, set input 1 as output
        if (alternator)
            out <= in0;
        else                        // Else set input 2 as output
            out <= in1;
    end
    
    
endmodule
