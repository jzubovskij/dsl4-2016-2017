#######################################
####### DSL4 2017 REPOSITORY
#######################################

#######################################

The contents of each of the folders are as follows

1) Vivado Sources - the constraints, simulation and design files for Vivado
2) Python Scripts - contain the scripts for generating the ROM and RAM contents
3) Documentation - contains the laboratory handout
4) Memory Initialisation - contains the files with which the ROM and RAM are initialised
5) Images - any images used in the report

IMPORTANT NOTE: the Vivado Sources/Assignment #1 Exclusive folder contains 
sources no longer used for Assignment #2. They are submitted as they are part of 
my working directory (which is a Bitbucket Repository). The Old Scripts is the scripts
used solely for the VGA part of Assignment #2 or outdated versions of the compiler.

#######################################

To run the compiler:

1) Navigate to Python Scripts
2) Add permission to run "compile" i.e. $chmod u+x compile
3) run compile $./compile

IMPORTANT NOTE: You need to have Python 2.7 installed. Instead of running compile you could run 

1) $python main.py 0 
2) $python assembler.py 0
